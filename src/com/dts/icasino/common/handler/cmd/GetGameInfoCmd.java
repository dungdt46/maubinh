package com.dts.icasino.common.handler.cmd;

public class GetGameInfoCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new GetGameInfoCmd();
	}

	@Override
	public void processEvent() {
	}

	@Override
	public void processRequest() {
		extension.getGame().sendGameChangeNotify(user);
	}

}
