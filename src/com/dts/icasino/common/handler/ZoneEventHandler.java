package com.dts.icasino.common.handler;

import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class ZoneEventHandler {
	private GameExtension m_parent;

    public ZoneEventHandler(GameExtension parent) {
        m_parent = parent;
    }

    public void handlerCommandFromZone(String cmdName, Object params) {
        m_parent.trace("ZoneEventHandler() - CommandName: " + cmdName + " - Params: " + params);

        if (cmdName == ServerMsgID.CMD_ASK_ACC_PLAYING) {
            //String accID = params.getUtfString(MessageType.EXT_FIELD_USERNAME);
            //ExtensionLoggingMan.getInstance().log("ZoneEventHandler() - CommandName: " + cmdName + " request accID: "+ accID);
        } else {
        	m_parent.trace("ZoneEventHandler() - CommandName: " + cmdName + " NOT FOUND!!!");
        }
        //getParentZone().getExtension().handleInternalMessage("testFunction", a);
    }
    /*
     Bao cho thang zone khi bat dau choi
     */
    public void ntfUserBeginPlayingGameNtf(User user, boolean isPlaying) {
    	m_parent.trace("ntfUserBeginPlayingGameNtf() to Zone, user="
                + user.getName() + ",id=" + user.getId() + " isPlaying: " + isPlaying);

        String str = isPlaying ? "y" : "n";
        int roomID = m_parent.getParentRoom().getId();
        int zoneID = m_parent.getParentZone().getId();

        ISFSObject sfso = new SFSObject();
        sfso.putUtfString(ServerFieldID.EXT_FIELD_USERNAME, user.getName());
        sfso.putUtfString(ServerFieldID.EXT_FIELD_ROOM_ID, String.valueOf(roomID));
        sfso.putUtfString(ServerFieldID.EXT_FIELD_GAME_ID, String.valueOf(m_parent.getGameid()));
        sfso.putUtfString(ServerFieldID.EXT_FIELD_ZONE_ID, String.valueOf(zoneID));
        sfso.putUtfString(ServerFieldID.EXT_FIELD_PLAYING_STATUS, str);
        m_parent.getParentZone().getExtension().handleInternalMessage(ServerMsgID.EXT_EVENT_USER_PLAYING_NOTIF, sfso);
    }
    /*
     Tra loi nguoi choi co dang choi hay ko
     */

    public void replyCommandAskAccountPlaying(User user, String reply) {
    	m_parent.trace("replyCommandAskAccountPlaying(), AccountId="
                + user.getName());
        int roomID = m_parent.getParentRoom().getId();
        int zoneID = m_parent.getParentZone().getId();

        ISFSObject sfso = new SFSObject();
        sfso.putUtfString(ServerFieldID.EXT_FIELD_USERNAME, user.getName());
        sfso.putUtfString(ServerFieldID.EXT_FIELD_ROOM_ID, String.valueOf(roomID));
        sfso.putUtfString(ServerFieldID.EXT_FIELD_GAME_ID, String.valueOf(m_parent.getGameid()));
        sfso.putUtfString(ServerFieldID.EXT_FIELD_ZONE_ID, String.valueOf(zoneID));
        sfso.putUtfString(ServerFieldID.EXT_FIELD_PLAYING_STATUS, reply);
        m_parent.getParentZone().getExtension().handleInternalMessage(ServerMsgID.EXT_EVENT_USER_PLAYING_NOTIF, sfso);
    }
}
