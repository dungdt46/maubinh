package com.dts.icasino.common.entity;

import java.util.ArrayList;

import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.SFSObject;

/*
 * Cai nay de adaptive cac game do Hoang viet port sang
 * do kh�ng muon thay doi cau truc cac ban tin dang co o client
 * so voi cai game tien lien
 * @DungDT
 * */
public abstract class AdaptiveRoom extends GameRoom{
	public AdaptiveRoom(GameExtension ext) {
		super(ext);
	}

	public void sendCardNotify2User(ArrayList<CardInfo> listc, boolean isUp, User user){
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
		ntfObj.putBool(ServerFieldID.EXT_FIELD_IS_TURN_UP_ALL, isUp);
        ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_CARD, toStateCard(listc, true));
        
        extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_LISTCARD_NTF, ntfObj, user);
	}
	
	public void sendCardNotify2Other(ArrayList<CardInfo> listc, boolean isUp, User user){
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
		ntfObj.putBool(ServerFieldID.EXT_FIELD_IS_TURN_UP_ALL, isUp);
        ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_CARD, toStateCard(listc, isUp));
        
        extension.sendGameEvent2Other(ServerMsgID.EXT_EVENT_LISTCARD_NTF, ntfObj, user.getId());
	}
	
	public void sendCardNotify2All(ArrayList<CardInfo> listc, boolean isUp, User user){
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
		ntfObj.putBool(ServerFieldID.EXT_FIELD_IS_TURN_UP_ALL, isUp);
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_CARD, toStateCard(listc, isUp));
        
        extension.sendGameEvent(ServerMsgID.EXT_EVENT_LISTCARD_NTF, ntfObj);
	}
	
	protected String toStateCard(ArrayList<CardInfo> listc, boolean isUp){
		String res = "";
		if (listc == null || listc.size() == 0)
			return res;
		
		if (listc.size() == 1)
			res = (isUp?card2String(listc.get(0)):"0_0");
		else
		{
			for (int i=0; i<listc.size() - 1; i++)
				res = res + (isUp?card2String(listc.get(i)):"0_0") + "-";
			
			res = res + (isUp?card2String(listc.get(listc.size() - 1)):"0_0");
		}
		
		return res;
	}
	
	public void sendLeaveNotify(PlayerInfo p)
	{
		 sendListUserUpdate();
        
         extension.notifyChangeRoomVariable(playerman.getListPlayer());
         extension.getParentZone().getExtension().handleInternalMessage(ServerMsgID.EXT_EVENT_USER_LEAVE_NOTIF, 
         		String.format("%s,%d,%d", p.getUser().getName(), extension.getParentZone().getId(), extension.getParentRoom().getId()));
	}
	
	public void sendJoinNotify(PlayerInfo p)
	{
		sendListUserUpdate();
		extension.notifyChangeRoomVariable(playerman.getListPlayer());
	}
	
	public void sendGameChangeNotify(User user)
	{
		SFSObject gameObj = new SFSObject();
        gameObj.putUtfString(ServerFieldID.EXT_FIELD_GAME_INFO, toString(user.getName()));
        extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_GAME_CHANGE_NOTIF, gameObj, user);
	}
	
	public void sendListUserUpdate()
	{
		SFSObject ntfObj = new SFSObject();
        ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, playerman.toPlayerString(";"));
        extension.sendGameEvent(ServerMsgID.EXT_EVENT_LIST_USER_UPDATE, ntfObj);
	}
	
	private String card2String(CardInfo cinfo)
	{
		return String.format("%d_%d", cinfo.getValue(), cinfo.getSubstance() - 1);
	}

	@Override
	public void startDealCard() {
		ArrayList<PlayerInfo> listPlayer = playerman.getListPlayer();
		for (int i=0; i<listPlayer.size(); i++)
			listPlayer.get(i).setPlaying(true);
		
		//sendListUserUpdate();
		dealCard();
	}

	protected abstract void dealCard();

	@Override
	public void leaveGame(PlayerInfo info) {
		extension.log("Not support");
	}
}
