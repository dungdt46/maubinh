package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;

public class SetBotCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new SetBotCmd();
	}

	@Override
	public void processEvent() {
		return;
	}

	@Override
	public void processRequest() {
		String mode = params.getUtfString(ServerFieldID.EXT_FIELD_BOT_TYPE);
		extension.log("Set bot mode : " + mode);
		
		if (mode.compareTo("on") == 0)
			extension.getGame().setBotMode(true);
		else if (mode.compareTo("off") == 0)
			extension.getGame().setBotMode(false);
		
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_SET_BOT_RES);
	}

}
