package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.GameResultCode;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class RegisterLeaveCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new RegisterLeaveCmd();
	}

	@Override
	public void processEvent() {
	}

	@Override
	public void processRequest() {
		PlayerInfo info = extension.getGame().getPlayerman().getInfo(user);
		if (info == null)
		{
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_LEAVE_GAME_RES);
			return;
		}
		
		Integer confirm = params.getInt(ServerFieldID.EXT_FIELD_LEAVE_STATUS);
		if (confirm != null)
		{
			if (confirm == 0)
				info.setLeave(false);
			else
			{
				if (info.isPlayer() == false || extension.getGame().getStatus() != GameRoom.GAME_TABLE_STATUS_PLAYING)
					extension.getApi().leaveRoom(user, extension.getGameRoom());
				else
					info.setLeave(true);
			}
			
			sendResponse(GameResultCode.GAME_SUCCESS);
		}
		else
			extension.log("Can not get leave status field");
	}

	private void sendResponse(int code){
		SFSObject resObj = new SFSObject();
        resObj.putInt(ServerFieldID.EXT_FIELD_RESULT, code);
        resObj.putInt(ServerFieldID.EXT_FIELD_OLD_RESULT, code);
        
        extension.send(ServerMsgID.EXT_EVENT_LEAVE_GAME_RES, resObj, user);
	}
}
