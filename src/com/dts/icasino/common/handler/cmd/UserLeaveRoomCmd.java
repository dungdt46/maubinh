package com.dts.icasino.common.handler.cmd;

import java.util.ArrayList;
import java.util.List;

import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.maubinh.MaubinhRoom;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;

public class UserLeaveRoomCmd extends BaseCmd {

	@Override
	public BaseCmd createCmd() {
		return new UserLeaveRoomCmd();
	}

	@Override
	public void processEvent() {
		extension.log(String.format("leavePlayer: Player %s(%d) leave", user.getName(), user.getId()));
		GameRoom room = extension.getGame();

		PlayerInfo playerInfo = room.getPlayerman().getInfo(user);

		if (playerInfo != null) {
			// neu la khach thi xoa info luon
			if (playerInfo.isPlayer() == false) {
				extension.getGame().removeInfo(playerInfo);
				extension.getParentZone().getExtension().handleInternalMessage(ServerMsgID.EXT_EVENT_USER_LEAVE_NOTIF,
						String.format("%s,%d,%d", playerInfo.getUser().getName(), extension.getParentZone().getId(),
								extension.getParentRoom().getId()));
			} else {
				// neu ko thi kiem tra xem van choi co dang dien ra hay ko
				if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING) {
					extension.log(String.format(
							"LeavePlayer: Game playing, player %s(%d) not remove, " + "only set disconnected",
							user.getName(), user.getId()));
					playerInfo.setDisconnected(true);
				} else {
					room.removeInfo(playerInfo);
					extension.log(String.format("LeavePlayer: Player %s(%d) not playing, remove", user.getName(),
							user.getId()));
					//update user count
					RoomVariable valNew = new SFSRoomVariable("params", room.getPlayerman().getListPlayer().size());
					List<RoomVariable> lst = new ArrayList<RoomVariable>();
					lst.add(valNew);
					
					((MaubinhRoom) extension.getGame()).sendListUserUpdate();
				}
			}

		} else
			extension.log(String.format("LeavePlayer: Exception Player %s(%d) not found", user.getName(), user.getId()));

		if (room.getPlayerman().countPlayer() == 0) {
			// Destroy room
			extension.log("Endgame() - cannot find any player in room - DESTROY ROOM!!!!! ");
			extension.destroyRoom();
		}
	}

	@Override
	public void processRequest() {
		return;
	}

}
