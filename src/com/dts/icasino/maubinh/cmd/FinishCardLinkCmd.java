package com.dts.icasino.maubinh.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.handler.cmd.BaseCmd;
import com.dts.icasino.maubinh.MaubinhPlayer;
import com.dts.icasino.maubinh.MaubinhRoom;

public class FinishCardLinkCmd extends BaseCmd {

	@Override
	public void processRequest() {
		// ham xu ly khi 1 thang xep bai xong
		if (((MaubinhRoom)extension.getGame()).isWaitLinkCard() == false) {
			extension.log("Can not FinishCardLinkCmd because invalid state");
			return;
		}

		MaubinhPlayer player = (MaubinhPlayer) extension.getGame().getPlayerman().getPlayerInfo(user);
		int status_sort = params.getInt(ServerFieldID.EXT_FIELD_ACT_MB);

		if (status_sort == MaubinhPlayer.STATUS_SORT_DONE) {
			player.setSortDone(true);
		} else if (status_sort == MaubinhPlayer.STATUS_SORTING) {
			player.setSortDone(false);
		}
		
		extension.updateLastGameAction();
		((MaubinhRoom) extension.getGame()).sendNotifySort(status_sort, player.getUser().getName());
		((MaubinhRoom) extension.getGame()).turnupAllCard(false);
		((MaubinhPlayer) extension.getGame().getPlayerman().getPlayerInfo(user)).setCountPlay(-1);
	}

	@Override
	public void processEvent() {

	}

	@Override
	public BaseCmd createCmd() {
		// TODO Auto-generated method stub
		return new FinishCardLinkCmd();
	}
}
