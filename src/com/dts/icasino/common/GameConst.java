package com.dts.icasino.common;

public class GameConst {
	public static final int GAME_TABLE_CARD_TYPE_NONE			= 0;
	public static final int GAME_TABLE_CARD_TYPE_ONE			= 1;
	public static final int GAME_TABLE_CARD_TYPE_TWO			= 2;
	public static final int GAME_TABLE_CARD_TYPE_THREE			= 3;
	public static final int GAME_TABLE_CARD_TYPE_FOUR			= 4;
	public static final int GAME_TABLE_CARD_TYPE_SEQUENCE		= 5;
	
	public static final int GAME_TABLE_CARD_TYPE_2				= 6;
	public static final int GAME_TABLE_CARD_TYPE_DOUBLE_2		= 7;
	public static final int GAME_TABLE_CARD_TYPE_3_THONG		= 8;
	public static final int GAME_TABLE_CARD_TYPE_4_THONG		= 9;
	public static final int GAME_TABLE_CARD_TYPE_DOUBLE_FOUR	= 10;
	
	public static final int GAME_TABLE_VICTORY_TYPE_NORMAL			= 1;
	public static final int GAME_TABLE_VICTORY_TYPE_TOI_TRANG		= 2;
	public static final int GAME_TABLE_VICTORY_TYPE_THUA_CONG		= 3;
	public static final int GAME_TABLE_VICTORY_TYPE_DANH_TOI_TRANG	= 4;
	
    public static final int GAME_TABLE_STATUS_START = 1;
    public static final int GAME_TABLE_STATUS_PLAYING = 2;
    public static final int GAME_TABLE_STATUS_FINISH = 3;
    public static final int GAME_TABLE_STATUS_BET_FOLD = 0;
    public static final int GAME_TABLE_STATUS_BET_RAISE = 1;
    public static final int GAME_TABLE_STATUS_BET_NONE = 2;
    public static final int GAME_TABLE_STATUS_BET_QUATER = 3;
    public static final int GAME_TABLE_STATUS_BET_HAFT = 4;
    public static final int GAME_TABLE_STATUS_BET_FOLLOW = 5;
    public static final int GAME_TABLE_STATUS_BET_DOUBLE = 6;
    public static final int GAME_TABLE_STATUS_BET_ALL = 7;
	
	public static final int GAME_CHANGE_BALANCE_TYPE_THOI_HEO		= 1;	//tru tien bi thoi 2
	public static final int GAME_CHANGE_BALANCE_TYPE_AN_THOI_HEO	= 2;	//cong tien vi oanh thang bet thoi 2
	public static final int GAME_CHANGE_BALANCE_TYPE_CHAT_HEO		= 3;	//an tien khi chat de 2 cua thang khac
	public static final int GAME_CHANGE_BALANCE_TYPE_BI_CHAT_HEO	= 4;	//bi tru tien khi bi chat 2
	public static final int GAME_CHANGE_BALANCE_RETURN_CHAT_HEO		= 5;	//hoan tien khi co thang khac chat de 2 thang vua an minh
	public static final int GAME_CHANGE_BALANCE_TYPE_DEN_LANG		= 6;	//tru tien bi den lang
	public static final int GAME_CHANGE_BALANCE_TYPE_DEN_BAO		= 7;	//tru tien bi den bao
	public static final int GAME_CHANGE_BALANCE_TYPE_TRU_GA			= 8;	//tru tien ghop ga
	public static final int GAME_CHANGE_BALANCE_TYPE_AN_GA			= 9;	//cong tien khi an ga
	public static final int GAME_CHANGE_BALANCE_TYPE_TOI_TRANG		= 10;	//Thay doi dien khi toi trang
	public static final int GAME_CHANGE_BALANCE_TYPE_THUA_CONG		= 11;	//tru tien khi bi thua cong
	public static final int GAME_CHANGE_BALANCE_TYPE_DUT_MU			= 12;	//tru tien khi bi dut mu
	public static final int GAME_CHANGE_BALANCE_TYPE_BAT_CONG_LANG	= 13;	//cong tien khi bat cong ca lang
	public static final int GAME_CHANGE_BALANCE_TYPE_XIN_RA			= 14;	//thay doi tien khi xin ra
	public static final int GAME_CHANGE_BALANCE_TYPE_THOI_TU_QUY	= 15;	//tru tien bi thoi tu quy
	public static final int GAME_CHANGE_BALANCE_TYPE_THOI_3_THONG	= 16;	//tru tien bi thoi 3 thong
	public static final int GAME_CHANGE_BALANCE_TYPE_THOI_4_THONG	= 17;	//tru tien bi thoi 4 thong
	public static final int GAME_CHANGE_BALANCE_TYPE_AN_THOI_TU_QUY	= 18;	//cong tien khi an thoi tu quy
	public static final int GAME_CHANGE_BALANCE_TYPE_AN_THOI_3_THONG	= 19;	//cong tien khi an thoi 3 thong
	public static final int GAME_CHANGE_BALANCE_TYPE_AN_THOI_4_THONG	= 20;	//cong tien khi an thoi 4 thong
	public static final int GAME_CHANGE_BALANCE_TYPE_DANH_TOI_TRANG		= 21;	//Thay doi dien khi toi trang
	public static final int GAME_CHANGE_BALANCE_TYPE_REFUND			= 22;	//Hoan tien cuoc
	
	public static final int GAME_SAM_TOI_TRANG_TYPE_5_DOI = 5;
	public static final int GAME_SAM_TOI_TRANG_TYPE_3_SAM = 4;
	public static final int GAME_SAM_TOI_TRANG_TYPE_DONG_CHAT = 3;
	public static final int GAME_SAM_TOI_TRANG_TYPE_4_HEO = 2;
	public static final int GAME_SAM_TOI_TRANG_TYPE_SANH_10 = 1;
	
	public static final int GAME_LIENG_BET = 1;
	public static final int GAME_LIENG_FOLD = 2;
	public static final int GAME_LIENG_FOLLOW = 3;
	public static final int GAME_LIENG_NEXT = 4;
	
    public static final int EXT_RESULT_LIENG_FOLD = 0;
	public static final int EXT_RESULT_LIENG_NORMAL = 1;
    public static final int EXT_RESULT_LIENG_ANH = 2;  
    public static final int EXT_RESULT_LIENG_LIENG = 3;
    public static final int EXT_RESULT_LIENG_SAP = 4;

	
	public static final int EXT_RESULT_LIENG_TYPE_NORMAL = 1;
    public static final int EXT_RESULT_LIENG_TYPE_ANH = 2;
    public static final int EXT_RESULT_LIENG_TYPE_SAP = 3;
    public static final int EXT_RESULT_LIENG_TYPE_LIENG = 4;
    public static final int EXT_RESULT_LIENG_TYPE_FOLD = 5;
    public static final int EXT_RESULT_LIENG_TYPE_VICTORY = 6;
    
    /*
     * Chuyen doi define tu cua DungDT ( de order theo dung thu tu manh yeu )
     * sang define cua Hoang cho do phai sua o phia client
     * */
    public static int liengVic2Vic(int type){
    	switch (type) {
		case EXT_RESULT_LIENG_FOLD:
			return EXT_RESULT_LIENG_TYPE_FOLD;
		case EXT_RESULT_LIENG_NORMAL:
			return EXT_RESULT_LIENG_TYPE_NORMAL;	
		case EXT_RESULT_LIENG_ANH:
			return EXT_RESULT_LIENG_TYPE_ANH;
		case EXT_RESULT_LIENG_LIENG:
			return EXT_RESULT_LIENG_TYPE_LIENG;
		case EXT_RESULT_LIENG_SAP:
			return EXT_RESULT_LIENG_TYPE_SAP;
		}
    	
    	return -1;
    }
    
    public static String liengVic2String(int type){
    	switch (type) {
		case EXT_RESULT_LIENG_FOLD:
			return "Úp bỏ";
		case EXT_RESULT_LIENG_NORMAL:
			return "Điểm";	
		case EXT_RESULT_LIENG_ANH:
			return "Ảnh";
		case EXT_RESULT_LIENG_LIENG:
			return "Liêng";
		case EXT_RESULT_LIENG_SAP:
			return "Sáp";
		}
    	
    	return "Unkown";
    }
    
    
	
	public static String reason2String(int reason)
	{
		switch (reason) {
		case GAME_CHANGE_BALANCE_TYPE_THOI_HEO:
			return "Thối hai";
		case GAME_CHANGE_BALANCE_TYPE_AN_THOI_HEO:
			return "Bắt thối hai";	
		case GAME_CHANGE_BALANCE_TYPE_CHAT_HEO:
			return "Chặt hai";
		case GAME_CHANGE_BALANCE_TYPE_BI_CHAT_HEO:
			return "Bị chặt hai";
		case GAME_CHANGE_BALANCE_TYPE_DEN_LANG:
			return "Đền làng";
		case GAME_CHANGE_BALANCE_TYPE_DEN_BAO:
			return "Đền báo";
		case GAME_CHANGE_BALANCE_TYPE_TRU_GA:
			return "Ghóp gà";
		case GAME_CHANGE_BALANCE_TYPE_AN_GA:
			return "Ăn gà";
		case GAME_CHANGE_BALANCE_TYPE_TOI_TRANG:
			return "Tới trắng";
		case GAME_CHANGE_BALANCE_TYPE_THUA_CONG:
			return "Thua cóng";
		case GAME_CHANGE_BALANCE_TYPE_DUT_MU:
			return "Đứt mù";
		case GAME_CHANGE_BALANCE_TYPE_BAT_CONG_LANG:
			return "Bắt cóng cả làng";
		case GAME_CHANGE_BALANCE_TYPE_XIN_RA:
			return "Xin ra";
		case GAME_CHANGE_BALANCE_TYPE_THOI_TU_QUY:
			return "Thối tứ quý";
		case GAME_CHANGE_BALANCE_TYPE_THOI_3_THONG:
			return "Thối 3 đôi thông";
		case GAME_CHANGE_BALANCE_TYPE_THOI_4_THONG:
			return "Thối 4 đôi thông";
		default:
			return "";
		}
	}
	
	public static String betType2String(int type)
	{
		switch (type) {
		case GAME_TABLE_STATUS_BET_FOLD:
			return "Úp bỏ";
		case GAME_TABLE_STATUS_BET_RAISE:
			return "Tố";	
		case GAME_TABLE_STATUS_BET_NONE:
			return "Nhường tố";
		case GAME_TABLE_STATUS_BET_QUATER:
			return "Tố 1/4";
		case GAME_TABLE_STATUS_BET_HAFT:
			return "Tố 1/2";
		case GAME_TABLE_STATUS_BET_FOLLOW:
			return "Theo";
		case GAME_TABLE_STATUS_BET_DOUBLE:
			return "Tố gấp đôi";
		case GAME_TABLE_STATUS_BET_ALL:
			return "Tố tất cả";
		default:
			return "";
		}
	}
	
	/*
	 * Tu loai toi trang => muc cuoc dc an
	 * */
	public static int toitrangSam2Muccuoc(int type){
		switch (type) {
		case GAME_SAM_TOI_TRANG_TYPE_5_DOI:
			return 10;
		case GAME_SAM_TOI_TRANG_TYPE_4_HEO:
			return 10;
		case GAME_SAM_TOI_TRANG_TYPE_3_SAM:
			return 10;
		case GAME_SAM_TOI_TRANG_TYPE_DONG_CHAT:
			return 20;
		case GAME_SAM_TOI_TRANG_TYPE_SANH_10:
			return 50;
		default:
			return 0;
		}
	}
	
	public static final int GAME_TIENLEN_TOI_TRANG_TYPE_TU_3 = 7;
	public static final int GAME_TIENLEN_TOI_TRANG_TYPE_TU_2 = 6;
	public static final int GAME_TIENLEN_TOI_TRANG_TYPE_4_SAM = 5;
	public static final int GAME_TIENLEN_TOI_TRANG_TYPE_6_DOI = 4;
	public static final int GAME_TIENLEN_TOI_TRANG_TYPE_5_THONG = 3;
	public static final int GAME_TIENLEN_TOI_TRANG_TYPE_DONG_MAU = 2;
	public static final int GAME_TIENLEN_TOI_TRANG_TYPE_SANH_RONG = 1;
	
	/*
	 * Tu loai toi trang => muc cuoc dc an
	 * */
	public static int toitrangTienLen2Muccuoc(int type){
		switch (type) {
		case GAME_TIENLEN_TOI_TRANG_TYPE_TU_3:
			return 20;
		case GAME_TIENLEN_TOI_TRANG_TYPE_TU_2:
			return 20;
		case GAME_TIENLEN_TOI_TRANG_TYPE_4_SAM:
			return 30;
		case GAME_TIENLEN_TOI_TRANG_TYPE_6_DOI:
			return 50;
		case GAME_TIENLEN_TOI_TRANG_TYPE_5_THONG:
			return 60;
		case GAME_TIENLEN_TOI_TRANG_TYPE_DONG_MAU:
			return 70;
		case GAME_TIENLEN_TOI_TRANG_TYPE_SANH_RONG:
			return 80;
		default:
			return 0;
		}
	}
	
	public static final int MONEY_TYPE_GOLD = 0;
    public static final int MONEY_TYPE_SILVER = 1;
    
    public static final String VAR_GOLD_VALUE = "amf";
    public static final String VAR_SILVER_VALUE = "amfs";
    public static final String VAR_ROOM_TYPE = "roomType";
}
