package com.dts.icasino.common;

import com.elcom.utils.DebugUtil;
import com.elcom.utils.thread.ReusableThread;
import com.elcom.utils.thread.ThreadWithQueue;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;

public class CDRWriter extends ThreadWithQueue {

    private Writer writer;
    private String cdrfile;
    private String tmpext;
    private String cdrext;
    private String prefix;
    private String cdrdir;
    private long createtime;
    private long period;
    private int maxline;
    private String header = "";
    private String dateformat;
    private int linecount = 0;
    public static final int ADD_NEW_LINE = 1;
    public static final int FINISH_FILE = 2;
    private CDRTimer timer = new CDRTimer("CDRwriter timer");

    public CDRWriter(String path, String pre, String tmp, String cdr, String hdr, String dateft, long period, int maxline) {
        this.cdrdir = path;
        this.prefix = pre;
        this.tmpext = tmp;
        this.cdrext = cdr;
        
        if (hdr != null && hdr.length() > 0)
            this.header = hdr + "\n";
        
        this.dateformat = dateft;
        this.period = period;
        this.maxline = maxline;

        checkAndCreateCDRFolder();
        timer.start();
    }

    //tao thu muc chua file CDR neu ko ton tai
    private void checkAndCreateCDRFolder() {
        File cdrfp = new File(cdrdir);
        if (!cdrfp.exists()) {
            String tmpPath = "";

            if (cdrdir.indexOf("/") == 0) {
                tmpPath = "/";
            }

            StringTokenizer st = new StringTokenizer(cdrdir, "/");

            while (st.hasMoreElements()) {
                if (tmpPath.equalsIgnoreCase("") || tmpPath.equalsIgnoreCase("/")) {
                    tmpPath = tmpPath + st.nextToken();
                } else {
                    tmpPath = tmpPath + "/" + st.nextToken();
                }

                File dirfp = new File(tmpPath);
                if (!dirfp.exists()) {
                    if (dirfp.mkdir()) {
                        if (logger.isInfoEnabled()) {
                            logger.info("Create folder " + tmpPath);
                        }

                        dirfp = null;
                    } else {
                        logger.error("Create folder " + tmpPath + " error");
                    }
                }
            }

        }

    }

    public void scanUnfinish() {

        File fp = new File(cdrdir);
        if (fp.exists()) {
            String[] listtmp = fp.list(new TempFilter(tmpext));
            if (listtmp != null && listtmp.length > 0) {
                for (int i = 0; i < listtmp.length; i++) {
                    String datestr = listtmp[i].substring(0, listtmp[i].length() - tmpext.length());

                    renameFile(cdrdir + "/" + listtmp[i],
                            cdrdir + "/" + prefix + datestr + cdrext);
                    /*File filefp = new File(config.getCdrWriterPath() + "/" + listtmp[i]);
                    
                    File cdrfilefp = new File(config.getCdrWriterPath() + "/" + prefix + datestr + cdrext);
                    
                    try {
                    FileUtils.copyFile(filefp, cdrfilefp);
                    filefp.delete();
                    logger.debug("Move " + config.getCdrWriterPath() + "/" + listtmp[i] + " to cdr " + 
                    config.getCdrWriterPath() + "/" + prefix + datestr + cdrext);
                    } catch (IOException e) {
                    DebugUtil.printStackTrace(e);
                    }*/
                }

            }
        } else {
            logger.error("CDR folder " + cdrdir + " not exist");
        }
    }

    private void finishCDR() {
        try {
            if (writer != null) {
                writer.close();
                writer = null;

                renameCdrFile();
                createNewCdrFile();
            }

            linecount = 0;
        } catch (Exception e) {
            DebugUtil.printStackTrace(e);
        }
    }

    private void renameCdrFile() {
        renameFile(cdrdir + "/" + cdrfile + tmpext,
                cdrdir + "/" + cdrfile + cdrext + "~");

        renameFile(cdrdir + "/" + cdrfile + cdrext + "~",
                cdrdir + "/" + cdrfile + cdrext);

    }

    private void renameFile(String srcfile, String desfile) {
        File filefp = new File(srcfile);
        File desfilefp = new File(desfile);

        if (filefp.renameTo(desfilefp))
            logger.info("Rename " + srcfile + " to " + desfile);
        else
            logger.error("Ca not rename " + srcfile + " to " + desfile);
    }

    private void createNewCdrFile() throws IOException {
        cdrfile = prefix + getFilenameFromDate();
        FileOutputStream fp = new FileOutputStream(cdrdir + "/" + cdrfile + tmpext, true);
        writer = new OutputStreamWriter(fp, "UTF-8");

        //writeHeader();
        createtime = System.currentTimeMillis();

        logger.info("Create new CDR file " + cdrfile + tmpext);
    }

    private void writeCDR(String cdr) {
        logger.info("Write: " + cdr + " into cdr file");
        cdr = cdr + "\n";

        if (linecount >= maxline) {
            finishCDR();
        }

        try {
            if (writer == null) {
                createNewCdrFile();
            }

            writer.write(cdr);
            writer.flush();
            linecount++;

        } catch (IOException e) {
            writer = null;
            DebugUtil.printStackTrace(e);
        }
    }

    @SuppressWarnings("unused")
    private void writeHeader() throws IOException {
        if (writer != null) {
            writer.write(header);
        }
    }

    private String getFilenameFromDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
        return sdf.format(cal.getTime());
    }

    public void addCdr(String line) {
        try {
            serviceMessage(ADD_NEW_LINE, 0, line);
        } catch (Exception e) {
            DebugUtil.printStackTrace(e);
        }
    }

    @Override
    protected boolean processMessage(int event, int subevent, Object data) {
        switch (event) {
            case ADD_NEW_LINE:
                writeCDR((String) data);
                break;
            case FINISH_FILE:
                finishCDR();
                break;
            default:
                break;
        }
        return true;
    }

    class CDRTimer extends ReusableThread {

        public CDRTimer(String name) {
            super(name);
        }

        @Override
        public void onStart() {
            logger.info("Thread " + getThreadName() + " start");
        }

        @Override
        public void onStop() {
            logger.info("Thread " + getThreadName() + " stop");
        }

        @Override
        public void process() {
            try {
                long currenttime = System.currentTimeMillis();
                if ((currenttime - createtime) >= period || linecount > maxline) {
                    if (linecount > 0) {
                        try {
                            serviceMessage(FINISH_FILE, 0, null);
                        } catch (Exception e) {
                            DebugUtil.printStackTrace(e);
                        }
                    }
                }

                Thread.sleep(1000);
            } catch (Exception e) {
                DebugUtil.printStackTrace(e);
            }
        }
    }

    class TempFilter implements FilenameFilter {

        private String extension;

        public TempFilter(String ext) {
            this.extension = ext;
        }

        public boolean accept(File dir, String name) {
            return name.endsWith(extension);
        }
    }

    /**
     * @return the prefix
     */
    public final String getPrefix() {
        return prefix;
    }

    /**
     * @param prefix the prefix to set
     */
    public final void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * @param tmpext the tmpext to set
     */
    public final void setTmpext(String tmpext) {
        this.tmpext = tmpext;
    }
}
