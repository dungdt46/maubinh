package com.dts.icasino.xito.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.dts.icasino.common.entity.CardInfo;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.util.StringUtil;
import com.dts.icasino.maubinh.MaubinhPlayer;
import com.smartfoxserver.bitswarm.sessions.Session;
import com.smartfoxserver.v2.entities.SFSUser;

public class MaubinhUtil {
	public static boolean haveTwo(ArrayList<CardInfo> listc) {
		int dem = 0;
		for (int i = 0; i < listc.size() - 1; i++) {
			if (listc.get(i).getValue() == listc.get(i + 1).getValue())
				dem++;
			else
				dem = 0;// reset
		}
		if (dem == 1)
			return true;

		return false;
	}

	public static boolean haveThree(ArrayList<CardInfo> listc) {
		int dem = 0;
		for (int i = 0; i < listc.size() - 1; i++) {
			if (listc.get(i).getValue() == listc.get(i + 1).getValue()) {
				dem++;
				if (dem == 2)
					break;
			} else {
				dem = 0;
			}
		}
		if (dem == 2)
			return true;
		return false;
	}

	public static int compareListCard(ArrayList<CardInfo> lst1, ArrayList<CardInfo> lst2) {
		if (lst1.size() == 0) {
			if (lst2.size() == 0)
				return 0;
			else
				return -1;
		}

		for (int i = 0; i < lst1.size(); i++) {
			CardInfo c1 = lst1.get(i);
			if (i >= lst2.size())
				return 1;

			CardInfo c2 = lst2.get(i);
			if (c1.getValue() > c2.getValue())
				return 1;
			else if (c1.getValue() < c2.getValue())
				return -1;
		}

		return lst1.get(0).getSubstance() > lst2.get(0).getSubstance() ? 1 : -1;
	}

	public static ArrayList<CardInfo> getLeftCard(ArrayList<CardInfo> child, ArrayList<CardInfo> org) {
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		for (int i = 0; i < org.size(); i++) {
			boolean found = false;
			for (int j = 0; j < child.size(); j++) {
				if (org.get(i).getId() == child.get(j).getId()) {
					found = true;
					break;
				}
			}

			if (found == false)
				res.add(org.get(i));
		}

		return res;
	}

	public static void concatCards(ArrayList<CardInfo> org, ArrayList<CardInfo> add) {
		for (int i = 0; i < add.size(); i++) {
			org.add(add.get(i));
		}
	}

	public static void copyCards(ArrayList<CardInfo> dst, ArrayList<CardInfo> src) {
		dst.clear();
		for (int i = 0; i < src.size(); i++) {
			dst.add(src.get(i));
		}
	}

	public static int getChiWinByType(int chiSrc) {
		// ham tinh so chi an duoc boi cac truong hop - neu cac truong hop dac
		// biet thi toi trang
		if (chiSrc == PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG_CUON)
			return 72;
		else if (chiSrc == PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG)
			return 36;
		else if (chiSrc == PlayerInfo.SPECIAL_5DOI_1SAM)
			return 18;
		else if (chiSrc == PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_THUNG)
			return 18;
		else if (chiSrc == PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_SANH)
			return 18;
		else if (chiSrc == PlayerInfo.SPECIAL_CARD_TYPE_LUC_PHE_BON)
			return 18;
		else if (chiSrc == PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI)
			return 6;
		else if (chiSrc == PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA)
			return 4;
		else if (chiSrc == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU)
			return 8;
		else if (chiSrc == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA)
			return 16;
		else if (chiSrc == PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU)
			return 10;
		else if (chiSrc == PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA)
			return 20;
		else if (chiSrc == PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG)
			return 2;
		else
			return 1;

	}

	// check toi trang
	public static int isToiTrang(ArrayList<CardInfo> cards) {
		int toitrangtype = 0;
		ArrayList<CardInfo> listcard = MaubinhUtil.copyArrayList(cards);
		if (listcard == null)
			return 0;
		if (listcard.size() != 13)
			return 0;
		// sort fisrt
		// Collections.sort(listcard);
		if (MaubinhUtil.checkRongCuon(listcard) == true) {
			toitrangtype = PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG_CUON;
			return toitrangtype;
		}
		// sanh rong binh thuong
		if (MaubinhUtil.checkRong(listcard) == true) {
			toitrangtype = PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG;
			return toitrangtype;
		}
		
		//lay tu quy truoc, vi neu ko sau ty quy cung bi lay lam bo 3
		ArrayList<CardInfo> list4 = get4(listcard);
		ArrayList<CardInfo> cardRemain = removeCardList(list4, listcard);
		//lay bo 3 cua phan con lai
		ArrayList<CardInfo> list3 = get3(cardRemain);
		if (list3.size() == 3){
			ArrayList<CardInfo> cardStillRemain = removeCardList(list3, cardRemain);
			ArrayList<CardInfo> list2 = get2(cardStillRemain);
			if (list2.size() + list4.size() == 10)
			{
				toitrangtype = PlayerInfo.SPECIAL_5DOI_1SAM;
				return toitrangtype;
			}
		}
		

		ArrayList<CardInfo> list2 = MaubinhUtil.get2(listcard);
		if (list2.size() == 12) {
			toitrangtype = PlayerInfo.SPECIAL_CARD_TYPE_LUC_PHE_BON;
			return toitrangtype;
		}

		if (MaubinhUtil.check3Thung(listcard) == true) {
			toitrangtype = PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_THUNG;
			return toitrangtype;
		}

		if (MaubinhUtil.check3Sanh(listcard) == true) {
			toitrangtype = PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_SANH;
			return toitrangtype;
		}
		return toitrangtype;
	}
	/*
	 * Ham check xem co sanh rong ko
	 */

	public static boolean checkRongCuon(ArrayList<CardInfo> listc) {
		if (listc == null)
			return false;
		if (listc.size() != 13)
			return false;
		Collections.sort(listc);
		boolean issequence = true;
		for (int i = 0; i < listc.size() - 1; i++) {
			if ((listc.get(i + 1).getRankValue() - listc.get(i).getRankValue() != 1
					&& listc.get(i + 1).getSubstance() != listc.get(i).getSubstance())
					|| listc.get(i + 1).getSubstance() != listc.get(i).getSubstance()
					|| listc.get(i + 1).getRankValue() - listc.get(i).getRankValue() != 1) {
				issequence = false;
				break;
			}
		}

		return issequence;
	}

	public static boolean checkRong(ArrayList<CardInfo> listc) {
		if (listc == null)
			return false;
		if (listc.size() != 13)
			return false;
		Collections.sort(listc);
		boolean issequence = true;
		for (int i = 0; i < listc.size() - 1; i++) {
			if (listc.get(i + 1).getRankValue() - listc.get(i).getRankValue() != 1) {
				issequence = false;
				break;
			}
		}

		return issequence;
	}

	/*
	 * ham lay cac bo doi lien nhau
	 */
	public static ArrayList<CardInfo> get2(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		if (listc == null)
			return res;
		if (listc.size() < 2)
			return res;

		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);

		Collections.sort(cards);
		int i = 0;
		while (true) {
			if (cards.size() < 2)
				break;

			if (i >= (cards.size() - 1))
				return res;

			if (cards.get(i).getRankValue() == cards.get(i + 1).getRankValue()) {
				res.add(cards.get(i));
				res.add(cards.get(i + 1));

				cards.remove(i);
				cards.remove(i);
			} else
				i++;
		}

		return res;
	}

	public static ArrayList<CardInfo> get3(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();

		if (listc.size() < 3)
			return res;

		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);

		Collections.sort(cards);
		int i = 0;
		while (true) {
			if (cards.size() < 3)
				break;

			if (i >= (cards.size() - 2))
				return res;

			if (cards.get(i).getRankValue() == cards.get(i + 1).getRankValue()
					&& cards.get(i).getRankValue() == cards.get(i + 2).getRankValue()) {
				res.add(cards.get(i));
				res.add(cards.get(i + 1));
				res.add(cards.get(i + 2));

				cards.remove(i);
				cards.remove(i);
				cards.remove(i);
			} else
				i++;
		}

		return res;
	}
	
	public static ArrayList<CardInfo> get3From5Doi1Xam(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> list4 = get4(listc);
		ArrayList<CardInfo> cardRemain = removeCardList(list4, listc);
		//lay bo 3 cua phan con lai
		ArrayList<CardInfo> list3 = get3(cardRemain);
		if (list3.size() == 3)
			return list3;
		else
			return null;
	}

	public static ArrayList<CardInfo> get4(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();

		if (listc.size() < 4)
			return res;

		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);

		Collections.sort(cards);
		int i = 0;
		while (true) {
			if (cards.size() < 4)
				break;

			if (i >= (cards.size() - 3))
				return res;

			if (cards.get(i).getRankValue() == cards.get(i + 1).getRankValue()
					&& cards.get(i).getRankValue() == cards.get(i + 2).getRankValue()
					&& cards.get(i).getRankValue() == cards.get(i + 3).getRankValue()) {
				res.add(cards.get(i));
				res.add(cards.get(i + 1));
				res.add(cards.get(i + 2));
				res.add(cards.get(i + 3));

				cards.remove(i);
				cards.remove(i);
				cards.remove(i);
				cards.remove(i);
			} else
				i++;
		}

		return res;
	}

	public static boolean check3Thung(ArrayList<CardInfo> listc) {
		if (listc == null)
			return false;
		ArrayList<CardInfo> listc1 = MaubinhUtil.getCardByLineID(1, listc);
		ArrayList<CardInfo> listc2 = MaubinhUtil.getCardByLineID(2, listc);
		ArrayList<CardInfo> listc3 = MaubinhUtil.getCardByLineID(3, listc);
		if (MaubinhUtil.isThung(listc1) && MaubinhUtil.isThung(listc2) && MaubinhUtil.isThung(listc3))
			return true;
		return false;
	}

	public static boolean check3Sanh(ArrayList<CardInfo> listc) {
		if (listc == null)
			return false;
		ArrayList<CardInfo> listc1 = MaubinhUtil.getCardByLineID(1, listc);
		ArrayList<CardInfo> listc2 = MaubinhUtil.getCardByLineID(2, listc);
		ArrayList<CardInfo> listc3 = MaubinhUtil.getCardByLineID(3, listc);
		if (MaubinhUtil.isSequence(listc1) && MaubinhUtil.isSequence(listc2) && MaubinhUtil.isSequence(listc3))
			return true;
		return false;
	}

	public static ArrayList<CardInfo> getCardByLineID(int lineID, ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> cardBuff = MaubinhUtil.copyArrayList(listc);
		ArrayList<CardInfo> listcard = new ArrayList<CardInfo>();
		for (int i = 0; i < cardBuff.size(); i++) {
			if (cardBuff.get(i).getLineid() == lineID)
				listcard.add(cardBuff.get(i));
		}

		return listcard;
	}

	// ham check xem co phai 1 day hay ko
	public static boolean isSequence(ArrayList<CardInfo> listc) {
		if (listc.size() == 0)
			return false;
		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);
		Collections.sort(cards);
		// check cho truong hop 1,2,3,4,5
		if (cards.get(0).getRankValue() == 2 && cards.get(cards.size() - 1).getRankValue() == 14) {
			CardInfo cardn = cards.remove(cards.size() - 1);
			cards.add(0, cardn);
		}

		boolean issequence = true;
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i + 1).getRankValue() == 2 && cards.get(i).getRankValue() == 14)
				continue;
			if (cards.get(i + 1).getRankValue() - cards.get(i).getRankValue() != 1) {
				issequence = false;
				break;
			}
		}

		return issequence;
	}

	// danh cho viec check sanh 1,2,3,4,5
	public static boolean isSpecialSequence(ArrayList<CardInfo> listc) {
		if (listc.size() == 0)
			return false;
		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);
		Collections.sort(cards);
		// check cho truong hop 1,2,3,4,5
		if (cards.get(0).getRankValue() == 2 && cards.get(cards.size() - 1).getRankValue() == 14) {
			return true;
		}
		return false;
	}

	public static boolean isBinhLung(ArrayList<CardInfo> listcard) {
		ArrayList<CardInfo> chi1 = MaubinhUtil.getCardByLineID(1, listcard);
		ArrayList<CardInfo> chi2 = MaubinhUtil.getCardByLineID(2, listcard);
		ArrayList<CardInfo> chi3 = MaubinhUtil.getCardByLineID(3, listcard);
		int typeChi1 = MaubinhUtil.convertMBSpecialToCompare(MaubinhUtil.getMaxCardType(chi1));
		int typeChi2 = MaubinhUtil.convertMBSpecialToCompare(MaubinhUtil.getMaxCardType(chi2));
		int typeChi3 = MaubinhUtil.convertMBSpecialToCompare(MaubinhUtil.getMaxCardType(chi3));
		if (typeChi1 < typeChi2)
			return true;
		if (typeChi1 < typeChi3)
			return true;
		if (typeChi2 < typeChi3)
			return true;
		if (typeChi1 == typeChi2 && MaubinhUtil.checkBinhLung(chi1, chi2, typeChi1))
			return true;
		if (typeChi2 == typeChi3 && MaubinhUtil.checkBinhLung(chi2, chi3, typeChi2))
			return true;

		return false;
	}

	public static boolean checkBinhLung(ArrayList<CardInfo> list1, ArrayList<CardInfo> list2, int typeChi1) {
		ArrayList<CardInfo> chi1 = MaubinhUtil.copyArrayList(list1);
		ArrayList<CardInfo> chi2 = MaubinhUtil.copyArrayList(list2);
		Collections.sort(chi1);
		Collections.sort(chi2);
		switch (typeChi1) {
		case PlayerInfo.GAME_CARD_TYPE_MAU_THAU: {
			for (int i = chi1.size() - 1, j = chi2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				if (chi1.get(i).getRankValue() < chi2.get(j).getRankValue()) {
					return true;
				} else if (chi1.get(i).getRankValue() > chi2.get(j).getRankValue()) {
					return false;
				}
			}
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH:
		case PlayerInfo.GAME_CARD_TYPE_SANH: {
			boolean isSpec1 = MaubinhUtil.isSpecialSequence(list1);
			boolean isSpec2 = MaubinhUtil.isSpecialSequence(list2);
			for (int i = chi1.size() - 1, j = chi2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				int rank_value1 = chi1.get(i).getRankValue();
				int rank_value2 = chi2.get(j).getRankValue();
				if (isSpec1 && rank_value1 == 14)
					rank_value1 = 1;
				if (isSpec2 && rank_value2 == 14)
					rank_value2 = 1;
				if (rank_value1 < rank_value2) {
					return true;
				} else if (rank_value1 > rank_value2) {
					return false;
				}
			}
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_THUNG: {
			for (int i = chi1.size() - 1, j = chi2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				int rank_value1 = chi1.get(i).getRankValue();
				int rank_value2 = chi2.get(j).getRankValue();
				if (rank_value1 < rank_value2) {
					return true;
				} else if (rank_value1 > rank_value2) {
					return false;
				}
			}
		}
			break;

		// boolean isTrue = chi1.get(chi1.size() - 1).getRankValue() <
		// chi2.get(chi2.size() - 1).getRankValue() ? true
		// : false;
		// return isTrue;
		case PlayerInfo.GAME_CARD_TYPE_THU: {
			// get list
			ArrayList<CardInfo> cards1 = MaubinhUtil.get2(chi1);
			ArrayList<CardInfo> cards2 = MaubinhUtil.get2(chi2);
			if (cards1.get(3).getRankValue() < cards2.get(3).getRankValue()) {
				return true;
			} else if (cards1.get(3).getRankValue() == cards2.get(3).getRankValue()) {
				if (cards1.get(0).getRankValue() < cards2.get(0).getRankValue()) {
					return true;
				} else if (cards1.get(0).getRankValue() == cards2.get(0).getRankValue()) {
					ArrayList<CardInfo> listCard1 = MaubinhUtil.removeCardList(cards1, chi1);
					ArrayList<CardInfo> listCard2 = MaubinhUtil.removeCardList(cards2, chi2);
					
					if (listCard1.get(0).getRankValue() > listCard2.get(0).getRankValue())
						return false;
					else if (listCard1.get(0).getRankValue() < listCard2.get(0).getRankValue())
						return true;
					else
						return false;
					
					/*if (chi1.get(chi1.size() - 1).getRankValue() < chi2.get(chi2.size() - 1).getRankValue()) {
						return true;
					} else {
						return false;
					}*/
				}
			}
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_DOI: {
			// get list
			ArrayList<CardInfo> cards1 = MaubinhUtil.get2(chi1);
			ArrayList<CardInfo> cards2 = MaubinhUtil.get2(chi2);
			if (cards1.size() == 0 || cards2.size() == 0)
				return false;
			if (cards1.get(0).getRankValue() < cards2.get(0).getRankValue()) {
				return true;
			} else if (cards1.get(0).getRankValue() == cards2.get(0).getRankValue()) {
				ArrayList<CardInfo> listCard1 = MaubinhUtil.removeCardList(cards1, chi1);
				ArrayList<CardInfo> listCard2 = MaubinhUtil.removeCardList(cards2, chi2);
				Collections.sort(listCard1);
				Collections.sort(listCard2);
				
				for (int i = listCard1.size() - 1, j = listCard2.size() - 1; i >= 0 && j >= 0; i--, j--) {
					if (listCard1.get(i).getRankValue() < listCard2.get(j).getRankValue())
						return true;
					else if (listCard1.get(i).getRankValue() > listCard2.get(j).getRankValue())
						return false;
				}
				
				return false;
				
				/*for (int i = chi1.size() - 1, j = chi2.size() - 1; i > 1 && j > 1; i--, j--) {
					if (chi1.get(i).getRankValue() < chi2.get(j).getRankValue()) {
						return true;
					} else if (chi1.get(i).getRankValue() > chi2.get(j).getRankValue()) {
						return false;
					}
				}*/
			} else {
				return false;
			}
		}
		//	break;
		case PlayerInfo.GAME_CARD_TYPE_SAM_CO:
		case PlayerInfo.GAME_CARD_TYPE_CU_LU: {
			ArrayList<CardInfo> cards1 = get3(chi1);
			ArrayList<CardInfo> cards2 = get3(chi2);
			if (cards1.size() == 0 || cards2.size() == 0)
				return false;
			if (cards1.get(2).getRankValue() > cards2.get(2).getRankValue())
				return false;
			else if (cards1.get(2).getRankValue() < cards2.get(2).getRankValue())
				return true;
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY: {
			ArrayList<CardInfo> cards1 = get4(chi1);
			ArrayList<CardInfo> cards2 = get4(chi2);
			if (cards1.size() == 0 || cards2.size() == 0)
				return false;
			if (cards1.get(0).getRankValue() > cards2.get(0).getRankValue())
				return false;
			else if (cards1.get(0).getRankValue() < cards2.get(0).getRankValue())
				return true;
		}
			break;
		default:
			return false;
		}
		return false;
	}

	public static int getMaxCardType(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> cardsBuff = new ArrayList<CardInfo>(listc);

		int maxCardtype = PlayerInfo.GAME_CARD_TYPE_MAU_THAU;
		System.out.println("Type card do compare = " + StringUtil.toCardString(cardsBuff));

		if (MaubinhUtil.isThungPhaSanh(cardsBuff)) {
			Collections.sort(cardsBuff);
			if (cardsBuff.get(0).getLineid() == 1)// thung chi dau
				return PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU;
			if (cardsBuff.get(0).getLineid() == 2)
				return PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA;
		}
		if (MaubinhUtil.isTuQuy(cardsBuff)) {
			Collections.sort(cardsBuff);
			if (cardsBuff.get(0).getLineid() == 1)// thung chi dau
				return PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU;
			if (cardsBuff.get(0).getLineid() == 2)
				return PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA;
		}
		if (MaubinhUtil.isCuLu(cardsBuff)) {
			Collections.sort(cardsBuff);
			if (cardsBuff.get(0).getLineid() == 2)
				return PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA;
			else
				return PlayerInfo.GAME_CARD_TYPE_CU_LU;
		}
		if (MaubinhUtil.isSanh(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_SANH;
		if (MaubinhUtil.isThungNormal(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_THUNG;
		if (MaubinhUtil.haveThree(cardsBuff)) {
			if (cardsBuff.get(0).getLineid() == 3)
				return PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI;
			else
				return PlayerInfo.GAME_CARD_TYPE_SAM_CO;
		}
		if (MaubinhUtil.isTwoPairs(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_THU;
		if (MaubinhUtil.isPair(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_DOI;

		return maxCardtype;
	}
	
	public static int getMaxCardType4Sort(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> cardsBuff = new ArrayList<CardInfo>(listc);

		int maxCardtype = PlayerInfo.GAME_CARD_TYPE_MAU_THAU;
		System.out.println("Type card do compare = " + StringUtil.toCardString(cardsBuff));

		if (MaubinhUtil.isThungPhaSanh(cardsBuff)) {
			Collections.sort(cardsBuff);
			return PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH;
		}
		if (MaubinhUtil.isTuQuy(cardsBuff)) {
			Collections.sort(cardsBuff);
			return PlayerInfo.GAME_CARD_TYPE_TU_QUY;
		}
		if (MaubinhUtil.isCuLu(cardsBuff)) {
			Collections.sort(cardsBuff);
			return PlayerInfo.GAME_CARD_TYPE_CU_LU;
		}
		if (MaubinhUtil.isSanh(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_SANH;
		if (MaubinhUtil.isThungNormal(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_THUNG;
		if (MaubinhUtil.haveThree(cardsBuff)) {
			return PlayerInfo.GAME_CARD_TYPE_SAM_CO;
		}
		if (MaubinhUtil.isTwoPairs(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_THU;
		if (MaubinhUtil.isPair(cardsBuff))
			return PlayerInfo.GAME_CARD_TYPE_DOI;

		return maxCardtype;
	}

	/*
	 * Ham check xem co phai la doi ko
	 */
	public static boolean isPair(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> listcard = MaubinhUtil.get2(listc);
		if (listcard.size() == 2)
			return true;
		return false;
	}

	public static boolean isTwoPairs(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		ArrayList<CardInfo> listcard = MaubinhUtil.get2(listc);
		if (listcard.size() == 4)
			return true;
		return false;
	}

	/*
	 * Ham check xem co phai sam co khong
	 */
	public static boolean isSamCo(ArrayList<CardInfo> listc) {
		if (listc.size() != 3)
			return false;

		if (listc.get(0).getValue() == listc.get(1).getValue() && listc.get(1).getValue() == listc.get(2).getValue()) {
			return true;
		} else
			return false;
	}

	public static boolean isSanh(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		return isSequence(listc);
	}

	public static boolean isThung(ArrayList<CardInfo> listc) {// danh cho truong
																// hop toi trang
		// if (listc.size() != 5)
		// return false;
		int demThung = 0;
		for (int i = 0; i < listc.size() - 1; i++) {
			if (listc.get(i + 1).getSubstance() == listc.get(i).getSubstance()) {
				demThung++;
			}
		}
		if (demThung >= (listc.size() - 1))
			return true;

		return false;
	}

	public static boolean isThungNormal(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		int demThung = 0;
		for (int i = 0; i < listc.size() - 1; i++) {
			if (listc.get(i + 1).getSubstance() == listc.get(i).getSubstance()) {
				demThung++;
			}
		}
		if (demThung >= (listc.size() - 1))
			return true;

		return false;
	}

	public static boolean isCuLu(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		Collections.sort(listc);
		if ((listc.get(0).getValue() == listc.get(1).getValue() && listc.get(2).getValue() == listc.get(3).getValue()
				&& listc.get(2).getValue() == listc.get(4).getValue())
				|| (listc.get(3).getValue() == listc.get(4).getValue()
						&& listc.get(0).getValue() == listc.get(1).getValue()
						&& listc.get(0).getValue() == listc.get(2).getValue()))
			return true;
		// if (MaubinhUtil.haveTwo(listc) && MaubinhUtil.haveThree(listc))
		// return true;
		return false;
	}

	/*
	 * Ham check xem co phai tu quy ko
	 */
	public static boolean isTuQuy(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);
		Collections.sort(cards);
		int i = 0;
		while (true) {

			if (i >= (cards.size() - 1))
				break;

			if (cards.get(i).getValue() == cards.get(i + 1).getValue()) {
				res.add(cards.get(i));
				res.add(cards.get(i + 1));

				cards.remove(i);
				cards.remove(i);
			} else
				i++;
		}
		if (res.size() != 4)
			return false;
		if (res.get(0).getValue() == res.get(1).getValue() && res.get(1).getValue() == res.get(2).getValue()
				&& res.get(2).getValue() == res.get(3).getValue()) {
			return true;
		} else
			return false;
	}

	public static boolean isTuQuyChi1(ArrayList<CardInfo> listc, int value) {
		int count = 0;
		for (int i = 0; i < listc.size(); i++) {
			if (listc.get(i).getValue() == value && listc.get(i).getLineid() == 1)
				count++;
		}

		if (count == 4)
			return true;
		else
			return false;
	}

	public static boolean isTuQuyChi2(ArrayList<CardInfo> listc, int value) {
		int count = 0;
		for (int i = 0; i < listc.size(); i++) {
			if (listc.get(i).getValue() == value && listc.get(i).getLineid() == 2)
				count++;
		}

		if (count == 4)
			return true;
		else
			return false;
	}

	public static boolean isThungPhaSanh(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);
		Collections.sort(cards);
		if (cards.get(0).getRankValue() == 2 && cards.get(cards.size() - 1).getRankValue() == 14) {
			CardInfo card = cards.get(cards.size() - 1);
			card.setValue(1);
			cards.remove(cards.size() - 1);
			cards.add(0, card);
		}

		boolean issequence = true;
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i + 1).getRankValue() - cards.get(i).getRankValue() != 1
					|| cards.get(i + 1).getSubstance() != cards.get(i).getSubstance()) {
				issequence = false;
				break;
			}
		}

		return issequence;
	}

	public static boolean isThungPhaSanhChi1(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);
		Collections.sort(cards);
		if (cards.get(0).getRankValue() == 2 && cards.get(cards.size() - 1).getRankValue() == 14) {
			CardInfo card = cards.get(cards.size() - 1);
			card.setValue(1);
			cards.remove(cards.size() - 1);
			cards.add(0, card);
		}
		boolean issequence = true;
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i + 1).getRankValue() - cards.get(i).getRankValue() != 1
					|| cards.get(i + 1).getSubstance() != cards.get(i).getSubstance()
					|| cards.get(i + 1).getLineid() == cards.get(i).getLineid() && cards.get(i + 1).getLineid() != 1) {
				issequence = false;
				break;
			}
		}

		return issequence;
	}

	public static boolean isThungPhaSanhChi2(ArrayList<CardInfo> listc) {
		if (listc.size() != 5)
			return false;
		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);
		Collections.sort(cards);
		if (cards.get(0).getRankValue() == 2 && cards.get(cards.size() - 1).getRankValue() == 14) {
			CardInfo card = cards.get(cards.size() - 1);
			card.setValue(1);
			cards.remove(cards.size() - 1);
			cards.add(0, card);
		}
		boolean issequence = true;
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i + 1).getRankValue() - cards.get(i).getRankValue() != 1
					|| cards.get(i + 1).getSubstance() != cards.get(i).getSubstance()
					|| cards.get(i + 1).getLineid() == cards.get(i).getLineid() && cards.get(i + 1).getLineid() != 2) {
				issequence = false;
				break;
			}
		}

		return issequence;
	}

	public static String getDescriptionToiTrangType(int type) {
		switch (type) {
		case PlayerInfo.GAME_CARD_TYPE_MAU_THAU:
			return "Mậu thầu";
		case PlayerInfo.GAME_CARD_TYPE_DOI:
			return "Đôi";
		case PlayerInfo.GAME_CARD_TYPE_THU:
			return "Thú";
		case PlayerInfo.GAME_CARD_TYPE_SAM_CO:
			return "Sám cô";
		case PlayerInfo.GAME_CARD_TYPE_SANH:
			return "Sảnh";
		case PlayerInfo.GAME_CARD_TYPE_THUNG:
			return "Thùng";
		case PlayerInfo.GAME_CARD_TYPE_CU_LU:
			return "Cù Lũ";
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY:
			return "Tứ quý";
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH:
			return "Thùng phá sảnh";
		case PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI:
			return "Sám chi cuối";
		case PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA:
			return "Cù lũ chi giữa";
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU:
			return "Thùng phá sảnh chi đầu";
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA:
			return "Thùng phá sảnh chi giữa";
		case PlayerInfo.SPECIAL_CARD_TYPE_LUC_PHE_BON:
			return "Lục phé bôn";
		case PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_SANH:
			return "3 cái sảnh";
		case PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_THUNG:
			return "3 cái thùng";
		case PlayerInfo.SPECIAL_5DOI_1SAM:
			return "5 đôi 1 sám";
		case PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG:
			return "Sảnh rồng";
		case PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG_CUON:
			return "Sảnh rồng cuốn";
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU:
			return "Tứ quý chi đầu";
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA:
			return "Tứ quý chi giữa";
		default:
			return "Không biết";
		}

	}

	public static int convertCardTypeToClient(int type)// convert send to client
														// type
	{
		/*
		 * Bài ăn trắng : 0 nếu ko có Rồng cuốn = 1 Sảnh rồng = 2 Đồng màu 1 =3
		 * Đồng màu 2: =4 5 đôi 1 sám = 5 Lục phé bôn = 6 3 thùng =7 3 sảnh = 8
		 * Mậu binh đặc biệt: -1 : bị binh lủng 0 nếu không có Sập hộ = 10 Bắt
		 * sập hộ = 11 Sập làng = 12 Bắt sập làng = 13 Sám chi đầu =14 Cù lũ chi
		 * giữa =115 Tứ quý chi cuối = 16 Tứ quý chi hai = 17 Thùng phá sảnh chi
		 * cuối = 18 Thùng phá sảnh chi hai = 19
		 * 
		 * Các cước bài của chi: Mậu thầu : 0 Đôi = 20 Xám chi (Sám cô) = 21
		 * Sảnh = 22 Thùng = 23 Thú = 24 Cù lũ = 25 Tứ quý = 26 Thùng phá sảnh =
		 * 27
		 * 
		 */
		switch (type) {
		case PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG_CUON:
			return 1;
		case PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG:
			return 2;
		case PlayerInfo.SPECIAL_CARD_TYPE_DONG_MAU_1:
			return 3;
		case PlayerInfo.SPECIAL_CARD_TYPE_DONG_MAU_2:
			return 4;
		case PlayerInfo.SPECIAL_5DOI_1SAM:
			return 5;
		case PlayerInfo.SPECIAL_CARD_TYPE_LUC_PHE_BON:
			return 6;
		case PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_THUNG:
			return 7;
		case PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_SANH:
			return 8;
		case PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG:
			return -1;
		case PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI:
			return 14;
		case PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA:
			return 15;
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU:
			return 16;
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA:
			return 17;
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU:
			return 18;
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA:
			return 19;
		case PlayerInfo.GAME_CARD_TYPE_MAU_THAU:
			return 20;
		case PlayerInfo.GAME_CARD_TYPE_DOI:
			return 21;
		case PlayerInfo.GAME_CARD_TYPE_THU:
			return 22;
		case PlayerInfo.GAME_CARD_TYPE_SAM_CO:
			return 23;
		case PlayerInfo.GAME_CARD_TYPE_SANH:
			return 24;
		case PlayerInfo.GAME_CARD_TYPE_THUNG:
			return 25;
		case PlayerInfo.GAME_CARD_TYPE_CU_LU:
			return 26;
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY:
			return 27;
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH:
			return 28;
		default:
			return 0;
		}

	}

	public static int getValueToCompare(ArrayList<CardInfo> listc, int typeCard) {
		int valueCompare = 0;
		switch (typeCard) {
		case PlayerInfo.GAME_CARD_TYPE_MAU_THAU:
			Collections.sort(listc);
			valueCompare = listc.get(listc.size() - 1).getRankValue();
			break;
		case PlayerInfo.GAME_CARD_TYPE_DOI: {
			ArrayList<CardInfo> cards = get2(listc);
			valueCompare = cards.get(0).getValue();
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_THU: {
			ArrayList<CardInfo> cards = get2(listc);
			valueCompare = cards.get(3).getValue();
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_SAM_CO:
		case PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI: {
			ArrayList<CardInfo> cards = get3(listc);
			valueCompare = cards.get(2).getValue();
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_SANH:
		case PlayerInfo.GAME_CARD_TYPE_THUNG:
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH:
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU:
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA:
			valueCompare = listc.get(listc.size() - 1).getValue();
			break;
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY:
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU:
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA: {
			ArrayList<CardInfo> cards = get4(listc);
			valueCompare = cards.get(0).getRankValue();
		}
			break;
		case PlayerInfo.GAME_CARD_TYPE_CU_LU:
		case PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA: {
			ArrayList<CardInfo> culu1 = MaubinhUtil.get3(listc);
			valueCompare = culu1.get(0).getValue();
		}
			break;

		default:
			break;
		}
		if (valueCompare == 15)
			return 2;

		return valueCompare;
	}

	public static int convertMBSpecialToCompare(int type) {
		switch (type) {
		case PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA:
			return PlayerInfo.GAME_CARD_TYPE_CU_LU;
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU:
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA:
			return PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH;
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU:
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA:
			return PlayerInfo.GAME_CARD_TYPE_TU_QUY;
		case PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI:
			return PlayerInfo.GAME_CARD_TYPE_SAM_CO;
		default:
			return type;
		}
	}

	public static int compareSameType(ArrayList<CardInfo> listc1, ArrayList<CardInfo> listc2, int type) {
		// 1 la listcard 1 lon hon, 2 la list card 2, 3 la bang nhau

		switch (type) {
		case PlayerInfo.GAME_CARD_TYPE_MAU_THAU: {
			Collections.sort(listc1);
			Collections.sort(listc2);
			for (int i = listc1.size() - 1, j = listc2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				if (listc1.get(i).getRankValue() < listc2.get(j).getRankValue()) {
					return 2;
				} else if (listc1.get(i).getRankValue() > listc2.get(j).getRankValue()) {
					return 1;
				}

			}
			return 3;
		}
		case PlayerInfo.GAME_CARD_TYPE_DOI: {
			ArrayList<CardInfo> doi1 = MaubinhUtil.get2(listc1);
			ArrayList<CardInfo> doi2 = MaubinhUtil.get2(listc2);

			if (doi1.get(0).getRankValue() > doi2.get(0).getRankValue())
				return 1;
			else if (doi1.get(0).getRankValue() < doi2.get(0).getRankValue())
				return 2;
			else {
				ArrayList<CardInfo> listCard1 = MaubinhUtil.removeCardList(doi1, listc1);
				ArrayList<CardInfo> listCard2 = MaubinhUtil.removeCardList(doi2, listc2);
				Collections.sort(listCard1);
				Collections.sort(listCard2);
				
				for (int i = listCard1.size() - 1; i >= 0 ; i--) {
					if (listCard1.get(i).getRankValue() < listCard2.get(i).getRankValue())
						return 2;
					else if (listCard1.get(i).getRankValue() > listCard2.get(i).getRankValue())
						return 1;
				}
				return 3;
			}

		}
		case PlayerInfo.GAME_CARD_TYPE_THU: {
			ArrayList<CardInfo> doi1 = get2(listc1);
			ArrayList<CardInfo> doi2 = get2(listc2);
			Collections.sort(doi1);
			Collections.sort(doi2);
			
			if (doi1.size() != 4 || doi2.size() != 4)
				return 1;
			if (doi1.get(3).getRankValue() < doi2.get(3).getRankValue())
				return 2;
			else if (doi1.get(3).getRankValue() == doi2.get(3).getRankValue()) {
				if (doi1.get(0).getRankValue() < doi2.get(0).getRankValue()) {
					return 2;
				} else if (doi1.get(0).getRankValue() > doi2.get(0).getRankValue()) {
					return 1;
				} else {
					ArrayList<CardInfo> listCard1 = MaubinhUtil.removeCardList(doi1, listc1);
					ArrayList<CardInfo> listCard2 = MaubinhUtil.removeCardList(doi2, listc2);
					
					if (listCard1.get(0).getRankValue() > listCard2.get(0).getRankValue())
						return 1;
					else if (listCard1.get(0).getRankValue() < listCard2.get(0).getRankValue())
						return 2;
					else
						return 3;
				}
			} else {
				return 1;
			}

		}
		case PlayerInfo.SPECIAL_5DOI_1SAM:
			ArrayList<CardInfo> lst1 = get3From5Doi1Xam(listc1);
			ArrayList<CardInfo> lst2 = get3From5Doi1Xam(listc2);
			if (lst1 != null && lst2 != null)
			{
				if (lst1.get(2).getRankValue() > lst2.get(2).getRankValue())
					return 1;
				else if (lst1.get(2).getRankValue() < lst2.get(2).getRankValue())
					return 2;
			}
			
			return 3;
		case PlayerInfo.GAME_CARD_TYPE_SAM_CO:
		case PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI: {
			ArrayList<CardInfo> cards1 = get3(listc1);
			ArrayList<CardInfo> cards2 = get3(listc2);
			if (cards1.get(2).getRankValue() > cards2.get(2).getRankValue())
				return 1;
			else if (cards1.get(2).getRankValue() < cards2.get(2).getRankValue())
				return 2;
			else
				return 3;
		}
		case PlayerInfo.GAME_CARD_TYPE_SANH:
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH:
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU:
		case PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA: {
			Collections.sort(listc1);
			Collections.sort(listc2);
			boolean isSpec1 = MaubinhUtil.isSpecialSequence(listc1);
			boolean isSpec2 = MaubinhUtil.isSpecialSequence(listc2);

			for (int i = listc1.size() - 1, j = listc2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				int rank_value1 = listc1.get(i).getRankValue();
				int rank_value2 = listc2.get(j).getRankValue();
				if (isSpec1 && rank_value1 == 14)
					rank_value1 = 1;
				if (isSpec2 && rank_value2 == 14)
					rank_value2 = 1;
				if (rank_value1 < rank_value2) {
					return 2;
				} else if (rank_value1 > rank_value2) {
					return 1;
				}
			}
			return 3;
		}
		case PlayerInfo.GAME_CARD_TYPE_THUNG: {
			Collections.sort(listc1);
			Collections.sort(listc2);
			for (int i = listc1.size() - 1, j = listc2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				if (listc1.get(i).getRankValue() < listc2.get(j).getRankValue()) {
					return 2;
				} else if (listc1.get(i).getRankValue() > listc2.get(j).getRankValue()) {
					return 1;
				}

			}
			return 3;
		}
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY:
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU:
		case PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA: {
			ArrayList<CardInfo> cards1 = get4(listc1);
			ArrayList<CardInfo> cards2 = get4(listc2);
			if (cards1.get(0).getRankValue() > cards2.get(0).getRankValue())
				return 1;
			else if (cards1.get(0).getRankValue() < cards2.get(0).getRankValue())
				return 2;
			else
				return 3;
		}
		case PlayerInfo.GAME_CARD_TYPE_CU_LU:
		case PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA: {
			ArrayList<CardInfo> cards1 = get3(listc1);
			ArrayList<CardInfo> cards2 = get3(listc2);
			if (cards1.get(2).getRankValue() > cards2.get(2).getRankValue())
				return 1;
			else if (cards1.get(2).getRankValue() < cards2.get(2).getRankValue())
				return 2;
			else
				return 3;
		}
		case PlayerInfo.SPECIAL_CARD_TYPE_LUC_PHE_BON: {
			ArrayList<CardInfo> doi1 = get2(listc1);
			ArrayList<CardInfo> doi2 = get2(listc2);
			Collections.sort(doi1);
			Collections.sort(doi2);
			for (int i = doi1.size() - 1, j = doi2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				if (doi1.get(i).getRankValue() < doi2.get(j).getRankValue()) {
					return 2;
				} else if (doi1.get(i).getRankValue() > doi2.get(j).getRankValue()) {
					return 1;
				}

			}
			
			ArrayList<CardInfo> lstremain1 = removeCardList(doi1, listc1);
			ArrayList<CardInfo> lstremain2 = removeCardList(doi2, listc2);
			if (lstremain1 != null && lstremain1.size() == 1 && lstremain2 != null && lstremain2.size() == 1){
				if (lstremain1.get(0).getRankValue() < lstremain2.get(0).getRankValue()) {
					return 2;
				} else if (lstremain1.get(0).getRankValue() > lstremain2.get(0).getRankValue()) {
					return 1;
				}
			}
			
			return 3;
		}
		case PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG_CUON:
		case PlayerInfo.SPECIAL_CARD_TYPE_SANH_RONG:
		case PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_THUNG:
		case PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_SANH: {
			Collections.sort(listc1);
			Collections.sort(listc2);
			for (int i = listc1.size() - 1, j = listc2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				if (listc1.get(i).getRankValue() < listc2.get(j).getRankValue()) {
					return 2;
				} else if (listc1.get(i).getRankValue() > listc2.get(j).getRankValue()) {
					return 1;
				}

			}
			return 3;
		}

		default:
			return 3;
		}
	}
	
	public static int Compare3Chi3Thung3Sanh(ArrayList<CardInfo> lst1, ArrayList<CardInfo> lst2, int type){
		int win = 0;
		
		for (int t = 0; t < 3; t++) {
			ArrayList<CardInfo> listc1 = getListSetCardByLine(t + 1, lst1);
			ArrayList<CardInfo> listc2 = getListSetCardByLine(t + 1, lst2);
			
			Collections.sort(listc1);
			Collections.sort(listc2);
			
			for (int i = listc1.size() - 1, j = listc2.size() - 1; i >= 0 && j >= 0; i--, j--) {
				if (listc1.get(i).getRankValue() < listc2.get(j).getRankValue()) {
					win--;
					break;
				} else if (listc1.get(i).getRankValue() > listc2.get(j).getRankValue()) {
					win++;
					break;
				}
			}
		}
		
		if (win == 0)
			return 3;
		else
		{
			if (win > 0)
				return 1;
			else
				return 2;
		}
	}
	
	public static ArrayList<CardInfo> getListSetCardByLine(int line, ArrayList<CardInfo> listsetcard) {
		ArrayList<CardInfo> listcard = new ArrayList<CardInfo>();
		for (int i = 0; i < listsetcard.size(); i++) {
			if (listsetcard.get(i).getLineid() == line)
				listcard.add(listsetcard.get(i));
		}
		return listcard;
	}

	public static ArrayList<CardInfo> removeCardList(ArrayList<CardInfo> listRemove, ArrayList<CardInfo> listcard) {

		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listcard);

		for (int j = 0; j < listRemove.size(); j++) {

			for (int i = 0; i < cards.size(); i++) {
				if (listRemove.get(j).getId() == cards.get(i).getId()) {
					cards.remove(i);
				}
			}
		}
		Collections.sort(cards);
		return cards;
	}

	public static ArrayList<CardInfo> copyArrayList(ArrayList<CardInfo> list1) {
		// ham de chi copy value, khong copy reference cua arraylist
		ArrayList<CardInfo> listCopy = new ArrayList<CardInfo>();
		for (int i = 0; i < list1.size(); i++) {
			CardInfo card = list1.get(i);
			// int id, int value, int substance, boolean turnup, int lineid
			listCopy.add(new CardInfo(card.getId(), card.getValue(), card.getSubstance(), false, card.getLineid()));
		}
		return listCopy;
	}

	public static int randomInRange(int minValue, int maxValue, int... exceptValue) {
		Random rand = new Random();
		int random = (minValue + rand.nextInt(maxValue - minValue + 1));
		for (int ex : exceptValue) {
			if (ex == random)
				random = (minValue + rand.nextInt(maxValue - minValue + 1));
		}
		return random;
	}

	public static ArrayList<CardInfo> AutoSortCardPro(ArrayList<CardInfo> listc) {
		ArrayList<CardInfo> cards = MaubinhUtil.copyArrayList(listc);
		ArrayList<CardInfo> list2 = MaubinhUtil.get2(cards);
		// khong co bo doi nao thi chi can sort lai list la ok
		if (list2.size() == 0) {
			Collections.sort(cards);
			Collections.reverse(cards);
			int chiID = 1;
			for (int i = 0; i < cards.size(); i++) {
				if (i > 4 && i < 10)
					chiID = 2;
				else if (i >= 10)
					chiID = 3;
				cards.get(i).setLineid(chiID);
			}
		} else {
			Collections.sort(list2);
			Collections.reverse(list2);
			ArrayList<CardInfo> newCards = new ArrayList<CardInfo>();
			ArrayList<CardInfo> cardRemove = new ArrayList<CardInfo>();
			cardRemove = MaubinhUtil.removeCardList(list2, cards);
			Collections.sort(cardRemove);
			Collections.reverse(cardRemove);
			for (int i = 0; i < 13; i++) {
				newCards.add(new CardInfo());
			}
			Random rand = new Random();
			int random = (1 + rand.nextInt(5));
			// vi tri co the add vao
			List<Integer> arrayIdx = null;
			if (random == 1) {
				arrayIdx = Arrays.asList(2, 1, 9, 6, 0, 3, 4, 8, 12, 11, 5, 10);
			} else if (random == 2) {
				arrayIdx = Arrays.asList(3, 1, 7, 6, 2, 0, 5, 8, 10, 12, 4, 9);
			} else if (random == 3) {
				arrayIdx = Arrays.asList(0, 4, 5, 9, 2, 3, 6, 8, 10, 12, 1, 11);
			} else if (random == 4) {
				arrayIdx = Arrays.asList(0, 3, 7, 9, 4, 2, 5, 6, 12, 11, 1, 10);
			} else {
				arrayIdx = Arrays.asList(0, 1, 5, 6, 2, 3, 7, 8, 10, 11, 4, 9);
			}
			
			System.out.println(random);

			Integer[] arrAdded = new Integer[list2.size()];// array da add cac
															// cap doi
			Integer[] arrEmpty = new Integer[13];// array chua cac vi tri co the
													// add
			// list moi
			for (int i = 0; i < list2.size(); i++) {
				int idxCanAdd = arrayIdx.get(i);
				newCards.set(idxCanAdd, list2.get(i));
				arrAdded[i] = idxCanAdd;
			}
			
			for (int i = 0; i < 13; i++) {
				arrEmpty[i] = i;
			}
			// Make the two lists
			List<Integer> listI1 = Arrays.asList(arrAdded);
			List<Integer> listI2 = Arrays.asList(arrEmpty);
			
			List<Integer> union = new ArrayList<Integer>(listI1);
			union.addAll(listI2);
			List<Integer> intersection = new ArrayList<Integer>(listI1);
			intersection.retainAll(listI2);
			union.removeAll(intersection);
			
			for (int i = 0; i < cardRemove.size(); i++) {
				newCards.set(union.get(i), cardRemove.get(i));
			}
			
			int chiID = 1;
			for (int i = 0; i < newCards.size(); i++) {
				if (i > 4 && i < 10)
					chiID = 2;
				else if (i >= 10)
					chiID = 3;
				newCards.get(i).setLineid(chiID);
			}

			ArrayList<CardInfo> listchi1 = MaubinhUtil.getCardByLineID(1, newCards);
			ArrayList<CardInfo> listchi2 = MaubinhUtil.getCardByLineID(2, newCards);
			ArrayList<CardInfo> listchi3 = MaubinhUtil.getCardByLineID(3, newCards);
			int maxCard1 = MaubinhUtil.getMaxCardType4Sort(listchi1);
			int maxCard2 = MaubinhUtil.getMaxCardType4Sort(listchi2);
			int maxCard3 = MaubinhUtil.getMaxCardType4Sort(listchi3);
			if (maxCard2 > maxCard1)// swap chi2 thanh chi 1
			{
				for (int i = 0; i < listchi2.size(); i++) {
					CardInfo card = listchi2.get(i);
					card.setLineid(1);
					newCards.set(i, card);
				}
				for (int i = 0; i < listchi1.size(); i++) {
					CardInfo card = listchi1.get(i);
					card.setLineid(2);
					newCards.set(i + 5, card);
				}
			} else if (maxCard1 == maxCard2) {
				int resultCompare = MaubinhUtil.compareSameType(listchi1, listchi1, maxCard1);
				if (resultCompare == 2)// chi2 lon hon, swap tiep
				{
					for (int i = 0; i < listchi2.size(); i++) {
						CardInfo card = listchi2.get(i);
						card.setLineid(1);
						newCards.set(i, card);
					}
					for (int i = 0; i < listchi1.size(); i++) {
						CardInfo card = listchi1.get(i);
						card.setLineid(2);
						newCards.set(i + 5, card);
					}
				}
			}

			if (maxCard3 == PlayerInfo.GAME_CARD_TYPE_SAM_CO || maxCard3 == PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI) {
				int idx = -1;
				CardInfo card = listchi3.get(0);
				for (int j = 0; j < union.size(); j++) {
					if (newCards.get(union.get(j)).getValue() != card.getValue()) {
						idx = union.get(j);
						break;
					}
				}
				
				// swap
				if (idx != -1)
				{
					newCards.set(10, newCards.get(idx));
					newCards.set(idx, card);
					chiID = 1;
					for (int i = 0; i < newCards.size(); i++) {
						if (i > 4 && i < 10)
							chiID = 2;
						else if (i >= 10)
							chiID = 3;
						newCards.get(i).setLineid(chiID);
					}
				}
				else
					System.out.println("Idx = -1");
			}

			return newCards;
		}
		return cards;
	}
	
	public static void printArrCard(ArrayList<CardInfo> lc, String title){
		System.out.println(title);
		for (int i=0; i<lc.size(); i++)
			System.out.print(lc.get(i).toDescription() + "(" + lc.get(i) + "), ");
		System.out.println();
	}
	
	public static void printArr(List<Integer> lc, String title){
		System.out.println(title);
		for (int i=0; i<lc.size(); i++)
			System.out.print(lc.get(i) + ", ");
		System.out.println();
	}
	
	public static ArrayList<CardInfo> reorderCard(ArrayList<CardInfo> listc,int type){
		ArrayList<CardInfo> lsttemp = copyArrayList(listc);
		Collections.sort(lsttemp);
		Collections.reverse(lsttemp);
		if (isSequence(lsttemp)){
			//kiem tra co phai dau la A, cuoi la 2 thi dao
			if (lsttemp.get(0).getValue() == 14 && lsttemp.get(lsttemp.size() - 1).getValue() == 15){
				CardInfo card0 = lsttemp.remove(0);
				lsttemp.add(card0);
			}
		}
		
		if (type == PlayerInfo.GAME_CARD_TYPE_DOI)
		{
			ArrayList<CardInfo> lst2 = get2(lsttemp);
			ArrayList<CardInfo> lstremain = removeCardList(lst2, lsttemp);
			Collections.sort(lstremain);
			Collections.reverse(lstremain);
			
			lst2.addAll(lstremain);
			return lst2;
		}
		else if (type == PlayerInfo.GAME_CARD_TYPE_SAM_CO || type == PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI || 
				type == PlayerInfo.GAME_CARD_TYPE_CU_LU || type == PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA)
		{
			ArrayList<CardInfo> lst3 = get3(lsttemp);
			ArrayList<CardInfo> lstremain = removeCardList(lst3, lsttemp);
			Collections.sort(lstremain);
			Collections.reverse(lstremain);
			
			lst3.addAll(lstremain);
			return lst3;
		}
		else if (type == PlayerInfo.GAME_CARD_TYPE_THU){
			ArrayList<CardInfo> lst2 = get2(lsttemp);
			Collections.sort(lst2);
			Collections.reverse(lst2);
			ArrayList<CardInfo> lstremain = removeCardList(lst2, lsttemp);
					
			lst2.addAll(lstremain);
			return lst2;
		}
		else if (type == PlayerInfo.GAME_CARD_TYPE_TU_QUY || type == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU|| type == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA){
			ArrayList<CardInfo> lst4 = get4(lsttemp);
			ArrayList<CardInfo> lstremain = removeCardList(lst4, lsttemp);
					
			lst4.addAll(lstremain);
			return lst4;
		}
		
		return lsttemp;
	}
	
	public static ArrayList<CardInfo> reorderToiTrangCard(ArrayList<CardInfo> listc,int type){
		ArrayList<CardInfo> lsttemp = copyArrayList(listc);
		Collections.sort(lsttemp);
		Collections.reverse(lsttemp);
		if (isSequence(lsttemp)){
			//kiem tra co phai dau la A, cuoi la 2 thi dao
			if (lsttemp.get(0).getValue() == 14 && lsttemp.get(lsttemp.size() - 1).getValue() == 15){
				CardInfo card0 = lsttemp.remove(0);
				lsttemp.add(card0);
			}
		}
		
		if (type == PlayerInfo.SPECIAL_5DOI_1SAM){
			ArrayList<CardInfo> list4 = get4(lsttemp);
			ArrayList<CardInfo> cardRemain = removeCardList(list4, lsttemp);
			//lay bo 3 cua phan con lai
			ArrayList<CardInfo> list3 = get3(cardRemain);
			ArrayList<CardInfo> cardStillRemain = removeCardList(list3, cardRemain);
			list4.addAll(cardStillRemain);
			list4.addAll(list3);
			
			return list4;
		}
		else if (type == PlayerInfo.SPECIAL_CARD_TYPE_LUC_PHE_BON){
			ArrayList<CardInfo> list2 = get2(lsttemp);
			ArrayList<CardInfo> cardRemain = removeCardList(list2, lsttemp);
			list2.addAll(cardRemain);
			
			return list2;
		}
		else if (type == PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_SANH || type == PlayerInfo.SPECIAL_CARD_TYPE_3_CAI_THUNG){
			ArrayList<CardInfo> lssort = new ArrayList<CardInfo>();
			for (int i=0; i<3; i++){
				ArrayList<CardInfo> lstcardchi = getListSetCardByLine(i+1, listc);
				int maxtype = MaubinhUtil.getMaxCardType(lstcardchi);
				
				ArrayList<CardInfo> lstcard = reorderCard(lstcardchi, maxtype);
				lssort.addAll(lstcard);
			}
			
			return lssort;
		}
		
		return lsttemp;
	}
	
	public static void main(String[] args){
		MaubinhPlayer p1 = new MaubinhPlayer();
		p1.setUser(new SFSUser("dungdt", new Session()));
		p1.setListSetCardFromUser("62,45,48,33,46,40,50,61,38,36,52,63,64");
		Collections.shuffle(p1.getListsetcard());
		
		printArrCard(p1.getListsetcard(), "");
		System.out.println("");
		
		ArrayList<CardInfo> lstsorted = AutoSortCardPro(p1.getListsetcard());
		printArrCard(lstsorted, "");
		
		System.out.println(isToiTrang(p1.getListsetcard()));
		
		ArrayList<CardInfo> lst1 = new ArrayList<CardInfo>();
		//K K 10 J 3
		/*lst1.add(new CardInfo(53, 13, 1));
		lst1.add(new CardInfo(56, 13, 4));
		lst1.add(new CardInfo(41, 10, 1));
		lst1.add(new CardInfo(46, 11, 2));
		lst1.add(new CardInfo(13, 3, 1));*/
		
		//K K 10 10 3
		lst1.add(new CardInfo(22, 5, 2));
		lst1.add(new CardInfo(63, 15, 3));
		lst1.add(new CardInfo(20, 4, 4));
		lst1.add(new CardInfo(60, 14, 4));
		lst1.add(new CardInfo(13, 3, 1));
		
		
		//K K J 5 9
		ArrayList<CardInfo> lst2 = new ArrayList<CardInfo>();
		/*lst2.add(new CardInfo(55, 13, 3));
		lst2.add(new CardInfo(54, 13, 2));
		lst2.add(new CardInfo(48, 11, 4));
		lst2.add(new CardInfo(23, 5, 3));
		lst2.add(new CardInfo(39, 9, 3));*/
		
		//K K 9 9 5
		lst2.add(new CardInfo(57, 14, 1));
		lst2.add(new CardInfo(60, 14, 4));
		lst2.add(new CardInfo(47, 11, 2));
		lst2.add(new CardInfo(39, 9, 3));
		lst2.add(new CardInfo(23, 5, 3));
		
		//System.out.println(compareSameType(lst1, lst2, PlayerInfo.GAME_CARD_TYPE_THU));
		//System.out.println(checkBinhLung(lst1, lst2, PlayerInfo.GAME_CARD_TYPE_DOI));
		System.out.println(StringUtil.list2String(reorderCard(lst1, 0)));
		System.out.println(StringUtil.list2String(reorderToiTrangCard(lst1, 0)));
	}
	
}
