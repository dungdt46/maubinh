package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;

public class SetBotCardCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new SetBotCardCmd();
	}

	@Override
	public void processEvent() {
		return;
	}

	@Override
	public void processRequest() {
		String player = params.getUtfString(ServerFieldID.EXT_FIELD_USERID);
		String listcard = params.getUtfString(ServerFieldID.EXT_FIELD_LIST_CARD);
		extension.log("Set player  " + player + " use cards " + listcard);
		
		extension.getGame().setListautocard(player, listcard);
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_SET_CARD_RES);
	}

}
