package com.dts.icasino.common.handler.cmd;

public class DefaultPlayCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new DefaultPlayCmd();
	}

	@Override
	public void processEvent() {
		extension.getGame().defaultPlay();
	}

	@Override
	public void processRequest() {
		return;
	}

}
