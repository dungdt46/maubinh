package com.dts.icasino.xito.util;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dts.icasino.common.GameConst;
import com.dts.icasino.common.entity.CardInfo;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.util.StringUtil;

public class SamLocUtil {
	
	private static Log logger = LogFactory.getLog(SamLocUtil.class);
	
	/*
	 * Ham check xem co phai la doi ko
	 * */
	public static boolean isTwo(ArrayList<CardInfo> listc)
	{
		logger.debug("IsTwo: " + StringUtil.toCardString(listc));
		
		if (listc.size() != 2)
			return false;
		
		if (listc.get(0).getValue() != listc.get(1).getValue())
		{
			logger.debug("CheckTow: First Round - 2 card not equal or subtance not valid");
			return false;
		}
		else
			return true;
	}
	
	/*
	 * Ham check xem co phai bo ba ko
	 * */
	public static boolean isThree(ArrayList<CardInfo> listc)
	{
		logger.debug("IsThree: " + StringUtil.toCardString(listc));
		
		if (listc.size() != 3)
			return false;
		
		if (listc.get(0).getValue() == listc.get(1).getValue() && listc.get(1).getValue() == listc.get(2).getValue())
		{
			logger.debug("CheckThree: First Round - 3 card equal");
			return true;
		}
		else
			return false;
	}
	
	/*
	 * Ham check xem co phai tu quy ko
	 * */
	public static boolean isFour(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 4)
			return false;
		
		if (listc.get(0).getValue() == listc.get(1).getValue() 
				&& listc.get(1).getValue() == listc.get(2).getValue()
				&& listc.get(2).getValue() == listc.get(3).getValue())
		{
			logger.debug("CheckFour: First Round - 4 card is equal");
			return true;
		}
		else
			return false;
	}
	
	/*
	 * Ham check xem co phai day ko
	 * */
	public static boolean isSequence(ArrayList<CardInfo> listc)
	{
		if (listc.size() < 3)
			return false;
		
		boolean issequence = true;
		//neu quan cuoi cung la 2
		if (listc.get(listc.size()-1).getValue() == 15)
		{
			int count = 2;
			//neu quan gan cuoi cung la A
			if (listc.get(listc.size() - 2).getValue() == 14)
				count = 3;
			
			//check xem co phai A23.. ko
			if (listc.get(0).getValue() == 3)
			{
				for (int i=0; i<listc.size() - count  ; i++)
				{
					if (listc.get(i+1).getValue() - listc.get(i).getValue() != 1)
					{
						issequence = false;
						break;
					}
				}
				
				if (issequence)
				{
					for (int i=0; i<count - 1; i++)
					{
						CardInfo cinfo = listc.remove(listc.size() - 1);
						listc.add(i, cinfo);	
					}
				}
				
				return issequence;
			}
			else
				return false;
		}
		else{
			for (int i=0; i<listc.size() -1  ; i++)
			{
				if (listc.get(i+1).getValue() - listc.get(i).getValue() != 1)
				{
					issequence = false;
					break;
				}
			}
		}
		
		
		return issequence;
	}
	
	/*
	 * Ham check xem co tu quy trong 1 danh sach quan bai ko
	 * */
	public static boolean have4(ArrayList<CardInfo> listc, int value)
	{
		int count=0;
		for (int i=0; i<listc.size(); i++)
		{
			if (listc.get(i).getValue() == value)
				count++;
		}
		
		if(count == 4)
			return true;
		else
			return false;
	}
	
	/*
	 * Ham check xem co sanh rong ko
	 * */
	public static boolean checkRong(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 10)
			return false;
		
		return isSequence(listc);
	}
	
	/*
	 * Check xem 10 quan bai cung chat hay ko
	 * */
	public static boolean checkSameSubstance(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 10)
			return false;
		
		boolean isLower = listc.get(0).getSubstance() < 3;
		
		for (int i=1; i<listc.size(); i++)
		{
			if ((listc.get(i).getSubstance() < 3) != isLower)
				return false;
		}
		
		return true;
	}
	
	/*
	 * ham lay cac bo doi lien nhau
	 * */
	public static ArrayList<CardInfo> get2(ArrayList<CardInfo> listc)
	{
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		
		if (listc.size() < 2)
			return res;
		
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
			cards.add(listc.get(i));
		
		Collections.sort(cards);
		
		int i=0;
		while (true)
		{
			if (cards.size() < 2)
				break;
			
			if (i >= (cards.size() - 1))
				return res;
			
			if (cards.get(i).getValue() == cards.get(i+1).getValue())
			{
				res.add(cards.get(i));
				res.add(cards.get(i+1));
				
				cards.remove(i);
				cards.remove(i);
			}
			else
				i++;
		}
		
		return res;
	}
	
	/*
	 * ham lay cac bo doi lien nhau
	 * */
	public static ArrayList<CardInfo> get3(ArrayList<CardInfo> listc)
	{
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		
		if (listc.size() < 3)
			return res;
		
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
			cards.add(listc.get(i));
		
		Collections.sort(cards);
		
		int i=0;
		while (true)
		{
			if (cards.size() < 3)
				break;
			
			if (i >= (cards.size() - 2))
				return res;
			
			if (cards.get(i).getValue() == cards.get(i+1).getValue() && cards.get(i+1).getValue() == cards.get(i+2).getValue())
			{
				res.add(cards.get(i));
				res.add(cards.get(i+1));
				res.add(cards.get(i+2));
				
				cards.remove(i);
				cards.remove(i);
				cards.remove(i);
			}
			else
				i++;
		}
		
		return res;
	}
	
	public static ArrayList<CardInfo> checkBetter(int value, ArrayList<CardInfo> listc, int count){

		if (count < 1)
			return null;
		
		if (listc.size() < count)
			return null;
		
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
			cards.add(listc.get(i));
		
		Collections.sort(cards);
		
		for (int i=0; i<cards.size() - (count - 1) ; i++)
		{
			if (cards.get(i).getValue() > value)
			{
				ArrayList<CardInfo> sublist = new ArrayList<CardInfo>();
				for (int j=0; j<count; j++)
					sublist.add(cards.get(i+j));
					
				if (count == 1)
					return sublist;
				else
				{
					boolean equal = true;
					for (int j=0; j<count-1; j++)
					{
						if (sublist.get(j).getValue() != sublist.get(j+1).getValue())
						{
							equal = false;
							break;
						}
					}
					
					if (equal)
						return sublist;
				}
			}
			
		}
		
		return null;
	}
	
	public static ArrayList<CardInfo> checkBetterSequence(int value, ArrayList<CardInfo> listc, int count){

		if (count < 3)
			return null;
		
		if (listc.size() < count)
			return null;
		
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
			cards.add(listc.get(i));
		
		Collections.sort(cards);
		int i=0;
		while (true)
		{
			if (i >= (cards.size() - 1))
				break;
			
			if (cards.get(i).getValue() == cards.get(i+1).getValue())
				cards.remove(i);
			else
				i++;
		}
		
		for (i=0; i<cards.size() - (count - 1) ; i++)
		{
			if (cards.get(i).getValue() > (value - count + 1))
			{
				ArrayList<CardInfo> sublist = new ArrayList<CardInfo>();
				for (int j=0; j<count; j++)
					sublist.add(cards.get(i+j));
					
				if (isSequence(sublist))
					return sublist;
			}
			
		}
		
		return null;
	}
	
	public static boolean checkCong(ArrayList<PlayerInfo> listp)
	{
		for (int i=0; i<listp.size(); i++)
		{
			PlayerInfo p = listp.get(i);
			
			if (p.isPlayer()){
				if (p.getListcard().size() < 10 )
					return false;
			}
		}
		
		return true;
	}
	
	public ArrayList<CardInfo> checkThoiHang(ArrayList<CardInfo> listc)
	{
		return null;
	}
	
	public static int calculateThoi(ArrayList<CardInfo>[] listthoi){
		int count = 0;
		//phan tu 1 la danh sach heo thoi
		ArrayList<CardInfo> listheo = listthoi[0];
		if (listheo != null && listheo.size()> 0)
			count = count + listheo.size() * 5;
		
		//tu quy
		ArrayList<CardInfo> list4 = listthoi[1];
		if (list4 != null && list4.size()> 0)
			count = count + list4.size()*5;
		
		return count;
	}
	
	public static ArrayList<CardInfo> calculateGreateCard(ArrayList<CardInfo> listc, ArrayList<CardInfo> previous, int type){
		switch (type) {
		case GameConst.GAME_TABLE_CARD_TYPE_ONE:
			return checkBetter(previous.get(0).getValue(), listc, 1);
		case GameConst.GAME_TABLE_CARD_TYPE_TWO:
			return checkBetter(previous.get(0).getValue(), listc, 2);
		case GameConst.GAME_TABLE_CARD_TYPE_THREE:
			return checkBetter(previous.get(0).getValue(), listc, 3);
		case GameConst.GAME_TABLE_CARD_TYPE_SEQUENCE:
			return checkBetterSequence(previous.get(previous.size()- 1).getValue(), listc, previous.size());
		case GameConst.GAME_TABLE_CARD_TYPE_FOUR:
			return checkBetter(previous.get(0).getValue(), listc, 4);
		case GameConst.GAME_TABLE_CARD_TYPE_2:
			return TienLenMienNamUtil.get4(listc);
		case GameConst.GAME_TABLE_CARD_TYPE_DOUBLE_2:
			ArrayList<CardInfo> list4 = TienLenMienNamUtil.get4(listc);
			if (list4.size() == 0)
				return list4;
		default:
			return null;
		}
	}
	
	public static ArrayList<CardInfo> checkHeoThoi(ArrayList<CardInfo> listc)
	{
		Collections.sort(listc);
		if (isSequence(listc))
			return null;
		
		ArrayList<CardInfo> listheo = new ArrayList<CardInfo>();
		
		for (int i=0; i<listc.size(); i++)
		{
			if (listc.get(i).getValue() == 15)
				listheo.add(listc.get(i));
		}
		
		return listheo;
	}
	
	public static void main(String[] args){
		ArrayList<CardInfo> list1 = new ArrayList<CardInfo>();
		list1.add(new CardInfo(61, 15, 1));
		list1.add(new CardInfo(62, 15, 2));
		list1.add(new CardInfo(63, 15, 3));
		list1.add(new CardInfo(63, 15, 4));
		list1.add(new CardInfo(22, 5, 2));
		list1.add(new CardInfo(24, 5, 4));
		list1.add(new CardInfo(37, 9, 1));
		list1.add(new CardInfo(41, 10, 1));
		list1.add(new CardInfo(42, 10, 2));
		list1.add(new CardInfo(44, 10, 4));
		//list1.add(new CardInfo(63, 15, 3));*/
		
		System.out.println(calculateThoi(TienLenMienNamUtil.calculateThoiHang(list1)));
		System.out.println(isSequence(list1));
		System.out.println(checkHeoThoi(list1));
		System.out.println(checkSameSubstance(list1));
		System.out.println(StringUtil.list2String(list1));
	}
}
