package com.dts.icasino.maubinh;

import java.io.Serializable;
import java.util.ArrayList;

import com.dts.icasino.common.entity.CardInfo;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.util.StringUtil;
import com.dts.icasino.xito.util.MaubinhUtil;

public class MaubinhPlayer extends PlayerInfo implements Serializable, Comparable<MaubinhPlayer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6161197235100489859L;
	public static int STATUS_SORTING = 0;// dang xep
	public static int STATUS_SORT_DONE = 1;// da xep xong

	private boolean isSortDone = false;// trang thai xep bai cua nguoi choi
	private boolean isBinhLung = false;//bi Binh Lung
	private int typeToiTrang = 0;
	private int winChi1 = 0;// so chi thang chi 1, < 0 thi la thua
	private int winChi2 = 0;// so chi thang chi 2
	private int winChi3 = 0;// so chi thang chi 3
	private int winTong3Chi = 0;
	private int pointBiSapHo = 0;// neu bat duoc 3 chi cua 1 user thi tang len 1
									// point
	private int pointAnSapHo = 0;// neu bi bat 3 chi cua 1 user thi tang len 1
									// point

	private int pointBonus = 0;// point danh cho truong hop an binh lung
	private int compareValue = 0;// de danh cho viec sort khi tinh tien
	private double balanceChange = 0;// so tien nguoi choi thay doi
	private int pointBinhLung = 0;// 0 la khong phai binh lung, <0 la bi binh
									// lung, >0 la an binh lung
	private int countPlay = 0;// dem so lan no thao tac den server thong qua
								// viec xep bai va xep xong
	private long winvalue = 0; 	//tong tien thang ( hoac thua) khong bi tru phe, cai nay chi danh cho bot de thong ke tien
	private ArrayList<CardInfo> listsetcard = new ArrayList<CardInfo>();// danh
																		// sach
																		// quan
																		// bai
																		// duoc
																		// xep

	private ArrayList<CardInfo> listdeal = new ArrayList<CardInfo>(); // danh
																		// sach
																		// quan
																		// bai
																		// duoc
																		// chia

	private ArrayList<CardInfo> listopen = new ArrayList<CardInfo>();
	// loai bai cua 3 chi
	private ArrayList<Integer> maxCardTypes = new ArrayList<Integer>();

	@Override
	public String toString() {
		return String.format("%d:%s:%d:%d:%f:%f:%s", user.getId(), user.getName(), isReady ? 1 : 0, isBoss ? 1 : 0,
				balance, currentbet, toStateCardString(false));
	}

	public String toString(boolean allcard) {
		// return String.format("%d:%s:%d:%d:%f:%f:%s", user.getId(),
		// user.getName(), isReady ? 1 : 0, isBoss ? 1 : 0,
		// balance, currentbet, toStateCardString(allcard));
		return String.format("%s|%s|%d", user.getName(), toStateCardString(allcard),
				this.isSortDone == true ? STATUS_SORT_DONE : STATUS_SORTING);
	}

	public String toStr() {
		return String.format("user:%s_balace:%f_currentbet:%f_fold:%d", user.getName(), balance, currentbet,
				isIspass() ? 1 : 0);
	}

	public void resetGame() {
		if (this.countPlay == -1)// van de nguyen
		{
			this.countPlay = 0;
		} else {
			this.countPlay++;// = 1 gia tri constance thi se kich ra khoi ban
		}
		this.setSortDone(false);
		this.resetCardTypes();
		this.isBinhLung = false;
		this.winChi1 = 0;
		this.winChi2 = 0;
		this.winChi3 = 0;
		this.winTong3Chi = 0;
		this.pointAnSapHo = 0;
		this.pointBiSapHo = 0;
		this.pointBonus = 0;
		this.compareValue = 0;
		this.balanceChange = 0;
		this.typeToiTrang = 0;
		this.pointBinhLung = 0;
		this.winvalue = 0;
		this.listdeal.clear();
		this.listsetcard.clear();
		super.resetGame();
	}

	public double getBalanceChange() {
		return balanceChange;
	}

	public void setBalanceChange(double balanceChange) {
		this.balanceChange += balanceChange;
	}

	public int getCompareValue() {
		return this.compareValue;
	}

	public void setCompareValue(int compareValue) {
		this.compareValue = compareValue;
	}

	public int getPointBonus() {
		return pointBonus;
	}

	public void setPointBonus(int _pointBonus) {
		this.pointBonus = _pointBonus;
	}

	public int getPointBiSapHo() {
		return pointBiSapHo;
	}

	public void setPointBiSapHo(int _pointBiSapHo) {
		this.pointBiSapHo = _pointBiSapHo;
	}

	public int getPointAnSapHo() {
		return this.pointAnSapHo;
	}

	public void setPointAnSapHo(int _pointAnSapHo) {
		this.pointAnSapHo = _pointAnSapHo;
	}

	public int getWinChi1() {
		return this.winChi1;
	}

	public void setWinChi1(int winChi1) {
		this.winChi1 = winChi1;
	}

	public int getWinChi2() {
		return this.winChi2;
	}

	public void setWinChi2(int winChi2) {
		this.winChi2 = winChi2;
	}

	public int getWinChi3() {
		return this.winChi3;
	}

	public void setWinChi3(int winChi3) {
		this.winChi3 = winChi3;
	}

	// danh cho truong hop toi trang
	public int getWinTong3Chi() {
		return this.winTong3Chi;
	}

	// danh cho truong hop toi trang
	public void setWinTong3Chi(int winTong3Chi) {
		this.winTong3Chi = winTong3Chi;
	}

	// danh cho truong hop binh thuong
	public int getWinTong3ChiNormal() {
		if (this.typeToiTrang == 0) {
			if (this.getPointAnSapHo() > 0 && this.getPointBiSapHo() > 0) {
				return ((this.winChi1 + this.winChi2 + this.winChi3) + this.getPointAnSapHo() * 6
						- this.getPointBiSapHo() * 6 + this.pointBonus);
			} else if (this.getPointAnSapHo() > 0) {
				// truong hop co thang binh lung
				return ((this.winChi1 + this.winChi2 + this.winChi3) + this.getPointAnSapHo() * 6 + this.pointBonus);
			} else if (this.getPointBiSapHo() > 0) {
				return ((this.winChi1 + this.winChi2 + this.winChi3) - this.getPointBiSapHo() * 6 + this.pointBonus);
			} else if (this.winTong3Chi != 0)// danh cho truong hop thua toi
												// trang
				return this.winTong3Chi;
			else// danh cho truong hop co thang bo cuoc
				return (this.winChi1 + this.winChi2 + this.winChi3 + this.pointBonus);

		} else if (this.typeToiTrang != PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG)
			if (this.winTong3Chi != 0)// danh cho truong hop thua toi trang
				return this.winTong3Chi;
			else
				return MaubinhUtil.getChiWinByType(this.typeToiTrang);
		else
			return 0;
	}

	public boolean isSortDone() {
		return isSortDone;
	}

	public void setSortDone(boolean isSortDone) {
		this.isSortDone = isSortDone;
	}

	public int getTypeToiTrang() {
		return typeToiTrang;
	}

	public void setTypeToiTrang(int typeToiTrang) {
		this.typeToiTrang = typeToiTrang;
	}

	private void initCardTypes() {
		for (int i = 0; i < 3; i++) {
			this.maxCardTypes.add(i, GAME_CARD_TYPE_MAU_THAU);
		}
	}

	public void resetCardTypes() {
		for (int i = 0; i < this.maxCardTypes.size(); i++) {
			this.maxCardTypes.set(i, GAME_CARD_TYPE_MAU_THAU);
		}
	}

	public void setMaxCardTypes(int currLine, int typeCard) {
		if (this.maxCardTypes.size() == 0)
			this.initCardTypes();
		this.maxCardTypes.set(currLine, typeCard);
	}

	public int getMaxCardTypes(int currLine) {
		return this.maxCardTypes.get(currLine);
	}

	public int getWinChiByLine(int lineid) {
		if (lineid == 3)
			return this.getWinChi3();
		else if (lineid == 2)
			return this.getWinChi2();
		else
			return this.getWinChi1();
	}

	public void setWinChiByLine(int lineid, int value) {
		if (lineid == 2)
			this.setWinChi3(this.getWinChi3() + value);
		else if (lineid == 1)
			this.setWinChi2(this.getWinChi2() + value);
		else
			this.setWinChi1(this.getWinChi1() + value);
	}

	@Override
	public void addCard(ArrayList<CardInfo> cards) {
		if (cards != null && cards.size() > 0) {
			for (int i = 0; i < cards.size(); i++) {
				listcard.add(cards.get(i));
			}
		}
	}

	// ham hien thi danh sach cac quan bai, neu up la -1
	public String toStateCardString(boolean isall) {
		ArrayList<CardInfo> listc = new ArrayList<CardInfo>();
		for (int i = 0; i < this.listsetcard.size(); i++) {
			if (ispass) {
				listc.add(new CardInfo(-1, 0, 0));
			} else {
				if (isall)
					listc.add(this.listsetcard.get(i));
				else
					listc.add(new CardInfo(-1, 0, 0));
			}
		}

		return StringUtil.list2String(listc);
	}

	public final ArrayList<CardInfo> getListopen() {
		return listopen;
	}

	// ham set listcard sau khi user da xep tu client vao list cua server
	public void setListSetCardFromUser(String listcard) {
		this.listsetcard.clear();
		String[] listStr = listcard.split(",");
		int chiID = 1;
		for (int i = 0; i < listStr.length; i++) {
			if (i > 4 && i < 10)
				chiID = 2;
			else if (i >= 10)
				chiID = 3;
			CardInfo cardInfo = this.getCardByString(listStr[i]);
			cardInfo.setLineid(chiID);
			this.listsetcard.add(cardInfo);
		}
	}

	public ArrayList<CardInfo> getListsetcard() {
		return listsetcard;
	}

	public void setListsetcard(ArrayList<CardInfo> listsetcard) {
		this.listsetcard = listsetcard;
	}

	public String getStringListSetCard() {
		// ham lay card da sap xep chuyen thanh string
		String listcard = "";
		for (int i = 0; i < listsetcard.size(); i++) {
			listcard += (String.valueOf(listsetcard.get(i).getId()) + ",");
		}
		return listcard;
	}

	private CardInfo getCardByString(String cardid) {
		// ham lay cardinfo bang chuoi
		int idcard = Integer.parseInt(cardid);
		int _value = idcard / 4;
		int _sub = idcard % 4;
		if (_sub == 0) {
			_value = _value - 1;
			_sub = 4;
		}
		return new CardInfo(idcard, _value, _sub);
	}

	public ArrayList<CardInfo> getListSetCardByLine(int line) {
		/*ArrayList<CardInfo> listcard = new ArrayList<CardInfo>();
		for (int i = 0; i < this.listsetcard.size(); i++) {
			if (this.listsetcard.get(i).getLineid() == line)
				listcard.add(this.listsetcard.get(i));
		}
		return listcard;*/
		
		return MaubinhUtil.getListSetCardByLine(line, this.listsetcard);
	}

	public void calculateToiTrangType() {
		if (this.listsetcard != null)
			this.typeToiTrang = MaubinhUtil.isToiTrang(this.listsetcard);
	}
	
	public void checkBinhLung(){
		isBinhLung = MaubinhUtil.isBinhLung(getListsetcard());
	}

	public static void main(String[] args) {
		/*
		// String listbt = "39,33,32,25,42,19,60,14,63,23,51,53,50";
		// MaubinhPlayer playerbt = new MaubinhPlayer();
		// playerbt.setListSetCardFromUser(listbt);
		// boolean isBfl = MaubinhUtil.isBinhLung(playerbt.getListsetcard());

		String list_sort_test = "31,53,56,30,27,60,43,45,25,44,64,61,62";
		// GenerateCard genCardLogic = new GenerateCard(3, 16);
		// ArrayList<CardInfo> listcardCC = genCardLogic.generateCard(13, true,
		// null);
		// listcardCC = MaubinhUtil.AutoSortCardPro(listcardCC);

		MaubinhPlayer player21 = new MaubinhPlayer();
		player21.setListSetCardFromUser(list_sort_test);
		ArrayList<CardInfo> listcc = MaubinhUtil.AutoSortCardPro(player21.getListsetcard());

		ArrayList<CardInfo> chi1 = player21.getListSetCardByLine(1);
		ArrayList<CardInfo> chi2 = player21.getListSetCardByLine(2);
		ArrayList<CardInfo> chi3 = player21.getListSetCardByLine(3);
		int typeChi1;
		typeChi1 = MaubinhUtil.getMaxCardType(chi1);
		int typeChi2;
		typeChi2 = MaubinhUtil.getMaxCardType(chi2);
		int typeChi3;
		typeChi3 = MaubinhUtil.getMaxCardType(chi3);

		CardInfo card = player21.getCardByString("57");
		// String listcard = "49,51,50,52,24,64,25,57,23,16,53,28,44";
		// String listcard = "64,46,63,48,25,42,44,33,51,35,40,55,14";
		String listcard = "56,43,54,44,49,51,47,46,30,31,25,61,37";
		String listcard2 = "21,19,20,59,50,41,33,38,16,45,17,36,64";
		String listcard3 = "60,64,16,20,24,28,32,36,40,42,48,52,56";// sanh rong
		String listcard3thung = "49,21,61,25,41,36,24,40,44,16,39,59,23";
		String listthungphasanh = "21,15,63,23,19,20,16,56,52,32,38,43,50";

		String listThung1 = "18,38,58,30,62,17,24,27,29,34,42,44,41";
		String listThung2 = "64,16,28,20,48,55,61,35,33,56,15,39,50";
		String binhlung = "48,57,54,16,63,40,55,20,58,36,35,52,18";

		String list3doi = "48,27,19,64,17,59,31,34,15,63,20,49,52";
		String listCrash = "64,31,45,48,62,63,56,20,61,13,47,39,46";

		String haithungphasanh = "62,14,18,22,26,28,32,36,40,44,46,47,45";

		String binhlung3 = "21,64,59,16,20,55,46,56,48,23,25,30,43";

		MaubinhPlayer player1 = new MaubinhPlayer();
		player1.setListSetCardFromUser(binhlung3);
		boolean isBl = MaubinhUtil.isBinhLung(player1.getListsetcard());

		player1.calculateToiTrangType();

		int maxCard = MaubinhUtil.getMaxCardType(player1.getListSetCardByLine(2));

		boolean isBinhLung = MaubinhUtil.isBinhLung(player1.getListsetcard());

		MaubinhPlayer player2 = new MaubinhPlayer();
		player2.setListSetCardFromUser(listThung2);

		int res = MaubinhUtil.compareSameType(player1.getListSetCardByLine(1), player2.getListSetCardByLine(1),
				PlayerInfo.GAME_CARD_TYPE_THUNG);

		boolean isThps = MaubinhUtil.isThungPhaSanh(player1.getListSetCardByLine(1));

		int isToiTrang = MaubinhUtil.isToiTrang(player1.getListsetcard());
		// MaubinhUtil.isBinhLung(player.getListsetcard());
		// player.calculateToiTrangType();
		// ArrayList<CardInfo> chi1 = player1.getListSetCardByLine(1);
		// ArrayList<CardInfo> chi2 = player1.getListSetCardByLine(2);
		// ArrayList<CardInfo> chi3 = player1.getListSetCardByLine(3);
		// int typeChi1;
		// typeChi1 = MaubinhUtil.getMaxCardType(chi1);
		// int typeChi2;
		// typeChi2 = MaubinhUtil.getMaxCardType(chi2);
		// int typeChi3;
		// typeChi3 = MaubinhUtil.getMaxCardType(chi3);
		// player1.setMaxCardTypes(0, typeChi1);
		// player1.setMaxCardTypes(1, typeChi2);
		// player1.setMaxCardTypes(2, typeChi3);

		// MaubinhPlayer player2 = new MaubinhPlayer();
		// player2.setListSetCardFromUser(listcard2);
		// chi1 = player2.getListSetCardByLine(1);
		// chi2 = player2.getListSetCardByLine(2);
		// chi3 = player2.getListSetCardByLine(3);
		// typeChi1 = MaubinhUtil.getMaxCardType(chi1);
		// typeChi2 = MaubinhUtil.getMaxCardType(chi2);
		// typeChi3 = MaubinhUtil.getMaxCardType(chi3);
		// player2.setMaxCardTypes(0, typeChi1);
		// player2.setMaxCardTypes(1, typeChi2);
		// player2.setMaxCardTypes(2, typeChi3);

		for (int t = 0; t < 3; t++) {
			// so tung chi
			int typeChiP1 = MaubinhUtil.convertMBSpecialToCompare(player1.getMaxCardTypes(t));
			int typeChiP2 = MaubinhUtil.convertMBSpecialToCompare(player2.getMaxCardTypes(t));
			if (typeChiP1 > typeChiP2) {
			} else if (typeChiP1 < typeChiP2) {
			} else if (typeChiP1 == typeChiP2) {
				// neu cung loai thi so sanh quan to nhat
				ArrayList<CardInfo> listp1 = player1.getListSetCardByLine(t + 1);
				ArrayList<CardInfo> listp2 = player2.getListSetCardByLine(t + 1);
				int resultCompare = MaubinhUtil.compareSameType(listp1, listp2, typeChiP1);
				if (resultCompare == 3) {
				}

			}
		}*/
	}

	@Override
	public int compareTo(MaubinhPlayer o) {
		// TODO Auto-generated method stub
		if (this.getCompareValue() < o.getCompareValue())
			return -1;
		else if (this.getCompareValue() == o.getCompareValue())
			return 0;
		else
			return 1;
	}

	public int getPointBinhLung() {
		return pointBinhLung;
	}

	public void setPointBinhLung(int pointBinhLung) {
		this.pointBinhLung = pointBinhLung;
	}

	public int getCountPlay() {
		return countPlay;
	}

	public void setCountPlay(int countPlay) {
		this.countPlay = countPlay;
	}
	
	public void addWinVal(long win){
		this.winvalue = this.winvalue + win;
	}
	
	public String toSortCard(){
		String lstsort = "";
		
		if (typeToiTrang == 0)
		{
			for (int i=0; i<3; i++){
				ArrayList<CardInfo> lstcardchi = getListSetCardByLine(i+1);
				int type = MaubinhUtil.getMaxCardType(lstcardchi);
				
				ArrayList<CardInfo> lstcard = MaubinhUtil.reorderCard(lstcardchi, type);
				lstsort = lstsort + StringUtil.list2String(lstcard, ",") + ",";
			}
		}
		else
		{
			ArrayList<CardInfo> lstordercard = MaubinhUtil.reorderToiTrangCard(getListsetcard(), typeToiTrang);
			lstsort = StringUtil.list2String(lstordercard, ",");
		}
		
		return lstsort;
	}

	public String getDump(){
		return String.format("User %s: Toi trang %d, winchi1 %d, winchi2 %d, winchi3 %d, wintotal %d, pointsapho %d, pointansapho %d, pointbonus %d, pointbinhlung %d", 
				user.getName(), typeToiTrang, winChi1, winChi2, winChi3, winTong3Chi, pointBiSapHo, pointAnSapHo, pointBonus, pointBinhLung);
	}

	public final boolean isBinhLung() {
		return isBinhLung;
	}

	public final long getWinvalue() {
		return winvalue;
	}
}
