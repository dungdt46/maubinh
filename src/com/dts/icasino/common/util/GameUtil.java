package com.dts.icasino.common.util;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.variables.UserVariable;

public class GameUtil {
	public static String getUserName(User user) {
        UserVariable aNval = user.getVariable("adn");
        String username = user.getName();
        if (aNval != null) {
            if (aNval.getStringValue().length() > 0) {
                username = aNval.getStringValue();
            }
        }

        return username;
    }

    public static String getUserName(String uid) {
        User user = SmartFoxServer.getInstance().getUserManager().getUserByName(uid);
        if (user != null) {
            UserVariable aNval = user.getVariable("adn");
            if (aNval != null) {
                if (aNval.getStringValue().length() > 0) {
                    return aNval.getStringValue();
                }
            }
        }
        
        return uid;
    }
}
