package com.dts.icasino.xito.util;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dts.icasino.common.GameConst;
import com.dts.icasino.common.entity.CardInfo;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.util.ListUtil;
import com.dts.icasino.common.util.StringUtil;

public class TienLenMienNamUtil {
	
	private static Log logger = LogFactory.getLog(TienLenMienNamUtil.class);
	
	/*
	 * Ham check xem co phai la doi ko
	 * */
	public static boolean isTwo(ArrayList<CardInfo> listc)
	{
		logger.debug("IsTwo: " + StringUtil.toCardString(listc));
		
		if (listc.size() != 2)
			return false;
		
		if (listc.get(0).getValue() != listc.get(1).getValue())
		{
			logger.debug("CheckTow: First Round - 2 card not equal or subtance not valid");
			return false;
		}
		else
			return true;
	}
	
	/*
	 * Ham check xem co phai bo ba ko
	 * */
	public static boolean isThree(ArrayList<CardInfo> listc)
	{
		logger.debug("IsThree: " + StringUtil.toCardString(listc));
		
		if (listc.size() != 3)
			return false;
		
		if (listc.get(0).getValue() == listc.get(1).getValue() && listc.get(1).getValue() == listc.get(2).getValue())
		{
			logger.debug("CheckThree: First Round - 3 card equal");
			return true;
		}
		else
			return false;
	}
	
	public static ArrayList<CardInfo> get4(ArrayList<CardInfo> listc)
	{
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		
		if (listc.size() < 4)
			return res;
		
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
			cards.add(listc.get(i));
		
		Collections.sort(cards);
		
		int i=0;
		while (true)
		{
			if (cards.size() < 4)
				break;
			
			if (i >= (cards.size() - 3))
				return res;
			
			if (cards.get(i).getValue() == cards.get(i+1).getValue() && cards.get(i+1).getValue() == cards.get(i+2).getValue() 
					&& cards.get(i+2).getValue() == cards.get(i+3).getValue())
			{
				res.add(cards.get(i));
				res.add(cards.get(i+1));
				res.add(cards.get(i+2));
				res.add(cards.get(i+3));
				
				cards.remove(i);
				cards.remove(i);
				cards.remove(i);
				cards.remove(i);
			}
			else
				i++;
		}
		
		return res;
	}
	
	/*
	 * Ham check xem co phai tu quy ko
	 * */
	public static boolean isFour(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 4)
			return false;
		
		if (listc.get(0).getValue() == listc.get(1).getValue() 
				&& listc.get(1).getValue() == listc.get(2).getValue()
				&& listc.get(2).getValue() == listc.get(3).getValue())
		{
			logger.debug("CheckFour: First Round - 4 card is equal");
			return true;
		}
		else
			return false;
	}
	
	/*
	 * Ham check xem co phai day ko
	 * */
	public static boolean isSequence(ArrayList<CardInfo> listc)
	{
		boolean issequence = true;
		for (int i=0; i<listc.size() -1  ; i++)
		{
			if (listc.get(i+1).getValue() - listc.get(i).getValue() != 1 || listc.get(i+1).getValue() == 15)
			{
				issequence = false;
				break;
			}
		}
		
		return issequence;
	}
	
	/*
	 * Ham check xem co tu quy trong 1 danh sach quan bai ko
	 * */
	public static boolean have4(ArrayList<CardInfo> listc, int value)
	{
		int count=0;
		for (int i=0; i<listc.size(); i++)
		{
			if (listc.get(i).getValue() == value)
				count++;
		}
		
		if(count == 4)
			return true;
		else
			return false;
	}
	
	/*
	 * Ham check xem co sanh rong ko
	 * */
	public static boolean checkRong(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 13)
			return false;
		
		boolean issequence = true;
		for (int i=0; i<listc.size() -1  ; i++)
		{
			if (listc.get(i+1).getValue() - listc.get(i).getValue() != 1)
			{
				issequence = false;
				break;
			}
		}
		
		return issequence;
	}
	
	/*
	 * Check xem 10 quan bai cung chat hay ko
	 * */
	public static boolean checkSameSubstance(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 13)
			return false;
		
		boolean isLower = listc.get(0).getSubstance() < 3;
		
		for (int i=1; i<listc.size(); i++)
		{
			if ((listc.get(i).getSubstance() < 3) != isLower)
				return false;
		}
		
		return true;
	}
	
	/*
	 * ham lay cac bo doi lien nhau
	 * */
	public static ArrayList<CardInfo> get2(ArrayList<CardInfo> listc)
	{
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		
		if (listc.size() < 2)
			return res;
		
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
			cards.add(listc.get(i));
		
		Collections.sort(cards);
		
		int i=0;
		while (true)
		{
			if (cards.size() < 2)
				break;
			
			if (i >= (cards.size() - 1))
				return res;
			
			if (cards.get(i).getValue() == cards.get(i+1).getValue())
			{
				res.add(cards.get(i));
				res.add(cards.get(i+1));
				
				cards.remove(i);
				cards.remove(i);
			}
			else
				i++;
		}
		
		return res;
	}
	
	/*
	 * Check xem co bi toi trang hay ko
	 * */
	public static int isToiTrang(ArrayList<CardInfo> listcard)
	{
		int toitrangtype = 0;
		if (listcard == null || listcard.size() != 13)
			return 0;
		
		if (TienLenMienNamUtil.checkRong(listcard) == true)
		{
			toitrangtype = GameConst.GAME_TIENLEN_TOI_TRANG_TYPE_SANH_RONG;
			return toitrangtype;
		}
		
		if (TienLenMienNamUtil.checkSameSubstance(listcard) == true)
		{
			toitrangtype = GameConst.GAME_TIENLEN_TOI_TRANG_TYPE_DONG_MAU;
			return toitrangtype;
		}
		
		ArrayList<CardInfo> list2 = TienLenMienNamUtil.get2(listcard);
		if (list2.size() >= 5)
		{
			ArrayList<CardInfo> listseq = TienLenMienNamUtil.getSequence(list2, 5);
			if (listseq != null && listseq.size() == 5)
			{
				toitrangtype = GameConst.GAME_TIENLEN_TOI_TRANG_TYPE_5_THONG;
				return toitrangtype;
			}
			
			if (list2.size() > 5)
			{
				toitrangtype = GameConst.GAME_TIENLEN_TOI_TRANG_TYPE_6_DOI;
				return toitrangtype;
			}
		}
		
		ArrayList<CardInfo> list3 = SamLocUtil.get3(listcard);
		if (list3.size() == 4)
		{
			toitrangtype = GameConst.GAME_TIENLEN_TOI_TRANG_TYPE_4_SAM;
			return toitrangtype;
		}
		
		if (TienLenMienNamUtil.have4(listcard, 15))
		{
			toitrangtype = GameConst.GAME_TIENLEN_TOI_TRANG_TYPE_TU_2;
			return toitrangtype;
		}
		
		if (TienLenMienNamUtil.have4(listcard, 3))
		{
			toitrangtype = GameConst.GAME_TIENLEN_TOI_TRANG_TYPE_TU_3;
			return toitrangtype;
		}
		
		return toitrangtype;
	}
	
	public static boolean is3Thong(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 6)
			return false;
		
		//không co 2 trong 3 doi thong
		if (listc.get(5).getValue() == 15)
			return false;
		
		if (listc.get(0).getValue() == listc.get(1).getValue() 
				&& listc.get(2).getValue() == listc.get(3).getValue()
				&& listc.get(4).getValue() == listc.get(5).getValue()
				&& listc.get(2).getValue() - listc.get(0).getValue() == 1
				&& listc.get(4).getValue() - listc.get(2).getValue() == 1
			)
		{	
			return true;
		}
		else
			return false;
	}
	
	public static boolean is4Thong(ArrayList<CardInfo> listc)
	{
		if (listc.size() != 8)
			return false;
		
		//không co 2 trong 3 doi thong
		if (listc.get(7).getValue() == 15)
			return false;
		
		if (listc.get(0).getValue() == listc.get(1).getValue() 
				&& listc.get(2).getValue() == listc.get(3).getValue()
				&& listc.get(4).getValue() == listc.get(5).getValue()
				&& listc.get(6).getValue() == listc.get(7).getValue()
				&& listc.get(2).getValue() - listc.get(0).getValue() == 1
				&& listc.get(4).getValue() - listc.get(2).getValue() == 1
				&& listc.get(6).getValue() - listc.get(4).getValue() == 1
			)
		{	
			return true;
		}
		else
			return false;
	}
	
	public static ArrayList<PlayerInfo> checkCong(ArrayList<PlayerInfo> listp, int count)
	{
		ArrayList<PlayerInfo> listcong = new ArrayList<PlayerInfo>();
		
		for (int i=0;i<listp.size(); i++)
		{
			if (listp.get(i).getListcard().size() == count)
				listcong.add(listp.get(i));
		}
		
		return listcong;
	}
	
	public static boolean inList(PlayerInfo info, ArrayList<PlayerInfo> list){
		for (int i=0; i<list.size(); i++)
		{
			if (list.get(i).getUser().getName().equalsIgnoreCase(info.getUser().getName()))
				return true;
		}
		
		return false;
	}
	
	public static ArrayList<CardInfo> get3Thong(ArrayList<CardInfo> listc){
		ArrayList<CardInfo> listcheck = new ArrayList<CardInfo>();
		
		ArrayList<CardInfo> cards = get2(listc);
		Collections.sort(cards);
		
		if (cards.size() < 6)
			return listcheck;
		
		for (int i=0; i<=cards.size()-6; i++)
		{
			for (int j=0; j<6; j++)
				listcheck.add(cards.get(i+j));
			
			if (is3Thong(listcheck))
				return listcheck;
			else
				listcheck.clear();
		}
		
		return listcheck;
		/*ArrayList<CardInfo> lst2 = get2(listc);
		if (lst2.size() >= 6)
		{
			for (int i=0; i<6;i++)
				listcheck.add(lst2.get(i));
		}
		
		return listcheck;*/
	}
	
	public static ArrayList<CardInfo> get4Thong(ArrayList<CardInfo> listc){
		ArrayList<CardInfo> listcheck = new ArrayList<CardInfo>();
		
		ArrayList<CardInfo> cards = get2(listc);
		Collections.sort(cards);
		if (cards.size() < 8)
			return listcheck;
		
		for (int i=0; i<=cards.size()-8; i++)
		{
			for (int j=0; j<8; j++)
				listcheck.add(cards.get(i+j));
			
			if (is4Thong(listcheck))
				return listcheck;
			else
				listcheck.clear();
		}
		
		return listcheck;
		/*ArrayList<CardInfo> lst2 = get2(listc);
		if (lst2.size() >= 8)
		{
			for (int i=0; i<8;i++)
				listcheck.add(lst2.get(i));
		}
		
		if (is4Thong(listcheck))
			return listcheck;
		else
			return new ArrayList<CardInfo>();*/
	}
	
	/*
	 * Ham check thoi heo, hang
	 * Heo do : 2 lan
	 * Heo den: 1 lan
	 * 3 doi thong : 2 lan
	 * tu quy: 3 lan
	 * 4 doi thong: 4 lan
	 * */
	@SuppressWarnings("unchecked")
	public static ArrayList<CardInfo>[] calculateThoiHang(ArrayList<CardInfo> listc){
		ArrayList<CardInfo>[] listthoi = new ArrayList[4];
		
		//phan tu 1 la danh sach heo thoi
		ArrayList<CardInfo> listheo = TienLenUtil.checkHeoThoi(listc);
		listthoi[0] = listheo;
		
		//phan tu 2 la danh sach tu quy
		ArrayList<CardInfo> list4 = get4(listc);
		listthoi[1] = list4;
		
		//phan tu 3 la danh sahc 3 doi thong hoac 4 doi thong
		ArrayList<CardInfo> listthong = get4Thong(listc);
		if (listthong.size() == 0)
		{
			listthong = get3Thong(listc);
			
		}
		
		listthoi[2] = listthong;
		
		//phan tu 4 la ba bich ( neu co )
		ArrayList<CardInfo> list3b = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
		{
			if (listc.get(i).getId() == 13)
				list3b.add(listc.get(i));
		}
		listthoi[3] = list3b;
		
		return listthoi;
	}

	public static ArrayList<CardInfo> getThoiHangList(ArrayList<CardInfo>[] listthoi){
		ArrayList<CardInfo> list = new ArrayList<CardInfo>();
		
		for (int i=0; i<listthoi.length; i++)
			ListUtil.addList2List(list, listthoi[i]);
		
		return list;
	}
	
	public static int calculateThoi(ArrayList<CardInfo>[] listthoi){
		int count = 0;
		//phan tu 1 la danh sach heo thoi
		ArrayList<CardInfo> listheo = listthoi[0];
		if (listheo != null)
		{
			for (int i=0; i<listheo.size(); i++)
			{
				if (listheo.get(i).getSubstance() < 3)
					count = count + 3;
				else
					count = count + 5;
			}
		}
		
		//tu quy
		ArrayList<CardInfo> list4 = listthoi[1];
		if (list4 != null)
			count = count + (list4.size()/4)*10;
		
		//thong
		ArrayList<CardInfo> listthong = listthoi[2];
		if (listthong != null){
			if (listthong.size() == 8)
				count = count + 20;
			else if (listthong.size() == 6)
				count = count + 5;
		}
		
		//3 bich
		ArrayList<CardInfo> list3b = listthoi[3];
		if (list3b != null && list3b.size() > 0)
			count = count + 5;
		
		return count;
	}
	
	public static void main(String[] args){
		ArrayList<CardInfo> list1 = new ArrayList<CardInfo>();
		list1.add(new CardInfo(61, 15, 3));
		list1.add(new CardInfo(13, 3, 1));
		list1.add(new CardInfo(14, 3, 2));
		list1.add(new CardInfo(15, 3, 3));
		list1.add(new CardInfo(17, 4, 1));
		list1.add(new CardInfo(18, 4, 2));
		list1.add(new CardInfo(19, 4, 3));
		list1.add(new CardInfo(22, 5, 2));
		list1.add(new CardInfo(24, 5, 4));
		list1.add(new CardInfo(34, 8, 2));
		list1.add(new CardInfo(48, 11, 4));
		//list1.add(new CardInfo(16, 3, 4));
		//list1.add(new CardInfo(61, 15, 1));
		//list1.add(new CardInfo(62, 15, 2));
		ArrayList<CardInfo> listc = new ArrayList<CardInfo>();
		//listc.add(new CardInfo(61, 15, 1));
		//listc.add(new CardInfo(62, 15, 2));
		//listc.add(new CardInfo(63, 15, 3));
		//listc.add(new CardInfo(64, 15, 4));
		
		//listc.add(new CardInfo(13, 3, 1));
		//listc.add(new CardInfo(57, 14, 1));
		//listc.add(new CardInfo(58, 14, 2));
		//listc.add(new CardInfo(59, 14, 3));
		listc.add(new CardInfo(40, 9, 4));
		
		listc.add(new CardInfo(39, 9, 3));
		//listc.add(new CardInfo(18, 4, 2));
		//listc.add(new CardInfo(19, 4, 3));
		//listc.add(new CardInfo(20, 4, 4));
		listc.add(new CardInfo(43, 10, 3));
		listc.add(new CardInfo(47, 11, 3));
		listc.add(new CardInfo(48, 11, 4));
		//listc.add(new CardInfo(26, 6, 2));
		listc.add(new CardInfo(50, 12, 2));
		//listc.add(new CardInfo(30, 7, 2));
		listc.add(new CardInfo(51, 12, 3));
		listc.add(new CardInfo(53, 13, 1));
		//listc.add(new CardInfo(54, 13, 2));
		//listc.add(new CardInfo(59, 14, 3));
		
		//System.out.println(isToiTrang(list1));
		//ArrayList<CardInfo>[] listcheck = calculateThoiHang(list1);
		//System.out.println(calculateThoi(listcheck));
		
		System.out.println(get3Thong(listc));
		System.out.println(get4Thong(listc));
		//System.out.println(SamLocUtil.calculateGreateCard(listc, list1, GameConst.GAME_TABLE_CARD_TYPE_SEQUENCE));
	}
	
	/*
	 * Tim sanh tu 1 bo bai
	 * */
	public static ArrayList<CardInfo> getSequence(ArrayList<CardInfo> listc, int count){

		if (count < 3)
			return null;
		
		if (listc.size() < count)
			return null;
		
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (int i=0; i<listc.size(); i++)
			cards.add(listc.get(i));
		
		Collections.sort(cards);
		int i=0;
		while (true)
		{
			if (i >= (cards.size() - 1))
				break;
			
			if (cards.get(i).getValue() == cards.get(i+1).getValue())
				cards.remove(i);
			else
				i++;
		}
		
		for (i=0; i<cards.size() - (count - 1) ; i++)
		{
			ArrayList<CardInfo> sublist = new ArrayList<CardInfo>();
			for (int j=0; j<count; j++)
				sublist.add(cards.get(i+j));
				
			if (isSequence(sublist))
				return sublist;
		}
		
		return null;
	}
}
