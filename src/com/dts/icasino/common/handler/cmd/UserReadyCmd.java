package com.dts.icasino.common.handler.cmd;


import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.maubinh.MaubinhPlayer;

public class UserReadyCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new UserReadyCmd();
	}

	@Override
	public void processEvent() {
		return;
	}

	@Override
	public void processRequest() {
		extension.log( String.format("PlayerReady: Player %s(%d) ready", user.getName(), user.getId()));
		GameRoom room = extension.getGame();
		
		PlayerInfo pinfo = extension.getGame().getPlayerman().getInfo(user);
		if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING || extension.getGame().getPlayerman().countPlayer() < 2)
		{
			extension.log( String.format("UserReady: Player %s(%d) ready not invalid", user.getName(), user.getId()));
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_READY_RES);
			return;
		}
		((MaubinhPlayer)pinfo).setCountPlay(-1);
		pinfo.setReady(true);
		extension.startGame();
	}

}
