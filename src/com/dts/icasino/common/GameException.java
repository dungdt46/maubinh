package com.dts.icasino.common;

public class GameException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1704459118453354389L;

	public GameException() {

	}

	public GameException(String arg0) {
		super(arg0);
	}

	public GameException(Throwable arg0) {
		super(arg0);
	}
}
