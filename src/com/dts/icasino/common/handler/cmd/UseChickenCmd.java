package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.PlayerInfo;

public class UseChickenCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new UseChickenCmd();
	}

	@Override
	public void processEvent() {
	}

	@Override
	public void processRequest() {
		PlayerInfo info = extension.getGame().getPlayerman().getInfo(user);
		if (info == null || extension.isPlaying() == true)
		{
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_USE_CHICKEN_RES);
			return;
		}
		
		Integer confirm = params.getInt(ServerFieldID.EXT_FIELD_CONFIRM);
		if (confirm != null)
		{
			info.setHaveChicken(confirm != 0);
			extension.setUseChickenVar(user, confirm);
			
			if (confirm == 0)
			{
				extension.getGame().removeFromChickenList(user);
				extension.getGame().checkChicken();
			}
		}
	}

}
