package com.dts.icasino.maubinh.cmd;

import java.util.ArrayList;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.handler.cmd.BaseCmd;
import com.dts.icasino.common.util.GameUtil;
import com.dts.icasino.maubinh.MaubinhExtension;
import com.dts.icasino.maubinh.MaubinhPlayer;
import com.dts.icasino.maubinh.MaubinhRoom;

public class FinishGameCmd extends BaseCmd {
	private MaubinhRoom gameRoom;
	private ArrayList<PlayerInfo> listPlayer;

	public FinishGameCmd(MaubinhRoom gameRoom, ArrayList<PlayerInfo> listPlayer) {
		super();
		this.gameRoom = gameRoom;
		this.listPlayer = listPlayer;
	}

	@Override
	public void processRequest() {
		// TODO Auto-generated method stub

	}

	@Override
	public void processEvent() {
		((MaubinhExtension) extension).cancelTimerFinishGame();
		MaubinhRoom roomgame = (MaubinhRoom)extension.getGame();
		String listRes = "";
		String listMoney = "";
		int chi_bocuoc_value = 0;// neu 1 thang bo cuoc, cac thang con lai se
									// default duoc thang 3 chi, tien thang =
									// binh lung
		ArrayList<PlayerInfo> listLeaveGame = extension.getGame().getPlayerman().getListLeaveGame();
		for (int i = 0; i < listLeaveGame.size(); i++) {
			chi_bocuoc_value += 6;
		}

		for (int i = 0; i < listPlayer.size(); i++) {
			int tong3chi = roomgame.isDraw() ? 0 : ((MaubinhPlayer) listPlayer.get(i)).getWinTong3ChiNormal() + chi_bocuoc_value;
			double moneyBalance = tong3chi * gameRoom.getConfig().getBetvalue();
			//String reasondes = " User " + listPlayer.get(i).getUser().getName() + " win tong 3 chi = "
			//		+ String.valueOf(tong3chi) + "bet room =" + String.valueOf(gameRoom.getConfig().getBetvalue());
			int mathType = GameRoom.GAME_MATCH_TYPE_WIN;
			int exp = 10;
			if (moneyBalance < 0) {
				mathType = GameRoom.GAME_MATCH_TYPE_LOST;
				moneyBalance = moneyBalance * (-1);
				exp = 4;
			}
			double realcharge = ((MaubinhPlayer) listPlayer.get(i)).getBalanceChange();
			listRes += GameUtil.getUserName(listPlayer.get(i).getUser()) + "|" + String.format("%.0f", realcharge) + "|"
					+ String.valueOf(tong3chi) + "|" + String.valueOf(chi_bocuoc_value) + ";";
			
			listMoney += listPlayer.get(i).getUser().getName() + "|" + ((MaubinhPlayer)listPlayer.get(i)).getWinvalue() + ";";

			// xu ly tru tien
			extension.getGame().increaseMatchNew(listPlayer.get(i), exp, (long) realcharge, mathType);

			if (realcharge < 0)
				realcharge = realcharge * (-1);// revert value
		}
		// danh sach nguoi bo cuoc
		for (int i = 0; i < listLeaveGame.size(); i++) {
			int tong3chi = -6;
			int realcharge = listPlayer.size() * tong3chi * extension.getGame().getConfig().getBetvalue();
			listRes += GameUtil.getUserName(listLeaveGame.get(i).getUser()) + "|" + String.format("%.0f", realcharge) + "|"
					+ String.valueOf(tong3chi) + "|" + "0" + ";";
		}
		gameRoom.sendVictoryNotify(listRes, listMoney);
		// remove list leave game
		gameRoom.getPlayerman().popBackListLeaveGame();
	}

	@Override
	public BaseCmd createCmd() {
		// TODO Auto-generated method stub
		return new FinishGameCmd(this.gameRoom, this.listPlayer);
	}

}
