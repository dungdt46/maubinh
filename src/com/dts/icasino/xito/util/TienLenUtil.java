package com.dts.icasino.xito.util;

import java.util.ArrayList;

import com.dts.icasino.common.entity.CardInfo;

public class TienLenUtil {
	public static double calculateHeoThoiMoney(ArrayList<CardInfo> listh, double betvalue)
	{
		double total = 0;
		
		for (int i=0; i<listh.size(); i++)
		{
			CardInfo info = listh.get(i);
			if (info.getValue() == 15)
			{
				switch (info.getSubstance()) {
				case CardInfo.CARD_SUBSTANCE_BICH:
					total = total + 1;
					break;
				case CardInfo.CARD_SUBSTANCE_TEP:
					total = total + 2;
					break;
				case CardInfo.CARD_SUBSTANCE_DIAMOND:
					total = total + 3;
					break;
				case CardInfo.CARD_SUBSTANCE_HEART:
					total = total + 4;
					break;
				default:
					break;
				}
			}
		}
		
		return total*betvalue;
	}
	
	public static ArrayList<CardInfo> checkHeoThoi(ArrayList<CardInfo> listc)
	{
		ArrayList<CardInfo> listheo = new ArrayList<CardInfo>();
		
		for (int i=0; i<listc.size(); i++)
		{
			if (listc.get(i).getValue() == 15)
				listheo.add(listc.get(i));
		}
		
		return listheo;
	}
	
	public static boolean isSequence(ArrayList<CardInfo> listc)
	{
		boolean issequence = true;
		for (int i=0; i<listc.size() -1  ; i++)
		{
			if (listc.get(i+1).getValue() - listc.get(i).getValue() != 1 
					|| listc.get(i+1).getSubstance() != listc.get(i).getSubstance() 
					||  listc.get(i+1).getValue() == 15)
			{
				issequence = false;
				break;
			}
		}
		
		return issequence;
	}
}
