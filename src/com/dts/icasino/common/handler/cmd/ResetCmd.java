package com.dts.icasino.common.handler.cmd;

import com.smartfoxserver.v2.entities.variables.RoomVariable;

public class ResetCmd extends BaseCmd {

	@Override
	public BaseCmd createCmd() {
		return new ResetCmd();
	}

	@Override
	public void processEvent() {
		extension.getGame().resetGame();
		RoomVariable val = extension.getGameRoom().getVariable("maintain");
		if (val != null) {
			boolean ismaintain = val.getBoolValue();
			extension.log("System set maintain is " + ismaintain);

			if (ismaintain == true) {
				extension.getGame().maintain();
				return;
			}
		}

		if (extension.getGame().getPlayerman().countPlayer() == 0) {
			// Destroy room
			extension.log("Endgame() - cannot find any player in room - DESTROY ROOM!!!!! ");
			extension.destroyRoom();
			return;
		}

		if (extension.getGame().getPlayerman().countPlayer() > 1) {
			extension.setReadyWaitVariable(extension.getConfig().getGameWaitStart());
			extension.getGame().sendNeedReady2All();
		}
		extension.cancelTimer();
		extension.getGame().joinQueuePlayer();
		extension.startWaitReadyTimer();
		extension.notifyChangeRoomVariable(extension.getGame().getPlayerman().getListPlayer());
	}

	@Override
	public void processRequest() {
	}

}
