package com.dts.icasino.common.entity;

import java.io.Serializable;


public class TableConfig implements Serializable{

	public TableConfig() {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int betvalue = 1000;			//so tien dat toi thieu
	
	private int feepercent = 5;				//% phe
	
	//muc toi thieu de join vao phong
	private long minbalancelevel = 10*betvalue;
	
	//muc toi thieu de dc choi tiep
	private long minbalance = 0;
	
	//tien den khi ket thuc van choi
	private long leavegamefee = 20*betvalue;
	
	public final long getMinbalance() {
		return minbalance;
	}

	public final void setMinbalance(long minbalance) {
		this.minbalance = minbalance;
	}

	private boolean haveChicken = false;

	public TableConfig(int betvalue, int feepercent, long minlevel,
			boolean haveChicken) {
		super();
		this.betvalue = betvalue;
		this.feepercent = feepercent;
		this.minbalancelevel = minlevel;
		this.haveChicken = haveChicken;
		this.leavegamefee = 30*betvalue;
		this.minbalance = this.betvalue * 40;
	}

	/**
	 * @return the betvalue
	 */
	public final int getBetvalue() {
		return betvalue;
	}

	/**
	 * @param betvalue the betvalue to set
	 */
	public final void setBetvalue(int betvalue) {
		this.betvalue = betvalue;
	}

	/**
	 * @return the feepercent
	 */
	public final int getFeepercent() {
		return feepercent;
	}

	/**
	 * @param feepercent the feepercent to set
	 */
	public final void setFeepercent(int feepercent) {
		this.feepercent = feepercent;
	}

	/**
	 * @return the minlevel
	 */
	public final long getMinlevel() {
		return minbalancelevel;
	}

	/**
	 * @param minlevel the minlevel to set
	 */
	public final void setMinlevel(long minlevel) {
		this.minbalancelevel = minlevel;
	}

	/**
	 * @return the haveChicken
	 */
	public final boolean isHaveChicken() {
		return haveChicken;
	}

	/**
	 * @param haveChicken the haveChicken to set
	 */
	public final void setHaveChicken(boolean haveChicken) {
		this.haveChicken = haveChicken;
	}

	public final long getMinbalancelevel() {
		return minbalancelevel;
	}

	public final void setMinbalancelevel(long minbalancelevel) {
		this.minbalancelevel = minbalancelevel;
	}

	public final long getLeavegameFee() {
		return leavegamefee;
	}

	public final void setLeavegameFee(long leavegame) {
		this.leavegamefee = leavegame;
	}

}
