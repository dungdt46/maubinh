package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class UserJoinRoomCmd extends BaseCmd {

	@Override
	public BaseCmd createCmd() {
		return new UserJoinRoomCmd();
	}

	@Override
	public void processEvent() {
		extension.log(String.format("JoinPlayer: Player %s(%d) join, is player : " + user.isPlayer(), user.getName(),
				user.getId()));
		GameRoom room = extension.getGame();

		if (user.isPlayer())
		{
			if (room.getPlayerman().isLeavingGame(user) == true) {
				extension.log(
						String.format("JoinPlayer: Player %s(%d) already leave, not rejoin", user.getName(), user.getId()));
				extension.getApi().leaveRoom(user, extension.getParentRoom());
				return;
			}

			PlayerInfo playerInfo = room.getPlayerman().getInfo(user);

			if (playerInfo == null) { // not exist, create new
				extension.log(
						String.format("JoinPlayer: Player %s(%d) not exist, create it info", user.getName(), user.getId()));

				playerInfo = room.newPlayer(user);

				if (playerInfo.getBalance() < room.getConfig().getMinbalance()) {
					extension.log(
							String.format("JoinPlayer: Player %s(%d) have balance %f not enough money min %d join. Leave game",
									user.getName(), user.getId(), playerInfo.getBalance(), room.getConfig().getMinbalance()));

					SFSObject exntfObj = new SFSObject();
					exntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
					extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_USER_EXPIRE_MONEY_NOTIF, exntfObj, user);
					extension.getApi().leaveRoom(user, extension.getParentRoom());
					return;
				}

				if (room.getPlayerman().countPlayer() + room.getPlayerman().countQueue() >= extension.getGameRoom()
						.getMaxUsers()) {
					extension.log(String.format(
							"JoinPlayer: Player %s(%d) have spectator because game have max player (%d) or playing",
							user.getName(), user.getId(), extension.getGameRoom().getMaxUsers()));
					extension.getApi().leaveRoom(user, extension.getParentRoom());
				}

				// neu ban dang choi, chuyen thanh dang ki choi
				if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING) {
					playerInfo.setPlayer(false);
					room.getPlayerman().player2Guest(playerInfo);

					room.getPlayerman().queuePlayer(playerInfo);
					room.sendListUserUpdate();
					room.sendGameChangeNotify(user);
					room.sendQueueUpdate();
					room.sendChickenUpdate2User(user);

					return;
				} else {
					// kiem tra xem list rong tuc la vao dau tien
					// va se duoc gan la boss
					room.getPlayerman().joinPlayer(playerInfo);
					room.setPlayerListVar();
					if (room.getPlayerman().getBoss() == null) {
						extension.log(String.format("JoinPlayer: Player %s(%d) join fist, is boss", user.getName(),
								user.getId()));
						room.getPlayerman().findNewBoss();
					}
					// neu khong phai boss thi san sang luon
					else {
						playerInfo.setReady(true);

						// neu la luot choi dau tien va nguoi thu 2 join vao thi gui
						// notify ready cho boss
						// if (extension.getGame().getPlayerman().countPlayer() == 2
						// && extension.isFirst() == true) {
						// // send ready to all user
						// for (int i = 0; i <
						// extension.getGame().getPlayerman().getListPlayer().size();
						// i++) {
						// extension.getGame()
						// .sendNeedReady(extension.getGame().getPlayerman().getListPlayer().get(i).getUser());
						// }
						// }
						// neu la luot choi dau tien va nguoi thu 2 join vao thi gui
						// notify ready cho boss
						if (extension.getGame().getPlayerman().countPlayer() == 2 && extension.isFirst() == true)
							extension.getGame().sendNeedReady(extension.getGame().getPlayerman().getBoss().getUser());
						// extension.getGame().sendNeedReady(extension.getGame().getPlayerman().getBoss().getUser());
					}
				}

			} else {
				extension.log(
						String.format("JoinPlayer: Player %s(%d) existed, update user info", user.getName(), user.getId()));

				playerInfo.setDisconnected(false);
				playerInfo.setReady(true);
				playerInfo.setUser(user);

				if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING)
					extension.getZonehandler().ntfUserBeginPlayingGameNtf(user, true);
			}

			room.sendJoinNotify(playerInfo);
			room.sendChickenUpdate2User(user);
			// extension.setRoomVaribale(user);

			if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING)
				room.sendGameChangeNotify(user);
			room.sendQueueUpdate();
		}
		else{
			room.sendListUserUpdate();
			room.sendGameChangeNotify(user);
			room.sendQueueUpdate();
			room.sendChickenUpdate2User(user);
		}
	}

	@Override
	public void processRequest() {
		return;
	}

}
