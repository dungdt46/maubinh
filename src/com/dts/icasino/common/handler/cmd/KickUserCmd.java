package com.dts.icasino.common.handler.cmd;


import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.handler.GameEventHandler;
import com.dts.icasino.common.util.StringUtil;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class KickUserCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new KickUserCmd();
	}

	@Override
	public void processEvent() {
		return;
	}

	@Override
	public void processRequest() {
		String playerid = params.getUtfString(ServerFieldID.EXT_FIELD_USERID);
		extension.log( String.format("kickPlayer: Player %s(%d) kick player %s", user.getName(), user.getId(), playerid));
		
		PlayerInfo pinfo = extension.getGame().getPlayerman().getInfo(user);
		if (pinfo == null || pinfo.isBoss() == false)
		{
			extension.log( String.format("kickPlayer: Player %s(%d) not boss, invalid", user.getName(), user.getId()));
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_KICK_RES);
			return;
		}
		
		GameRoom room = extension.getGame();
		if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING)
		{
			extension.log("kickPlayer: Game playing. Cannot kick");
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_KICK_RES);
			return;
		}
		
		int pos = room.getPlayerman().getPlayerPos(playerid);
		if (pos == -1 )
		{
			extension.log("kickPlayer: Can not kick because not found uid " + playerid);
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_KICK_RES);
			return;
		}
		
		PlayerInfo p = room.getPlayerman().getPlayerbyPos(pos);
		if (p != null)
		{
			extension.log( String.format("kickPlayer: Player %s(%d) is kicked  by player %s(%d)", 
					p.getUser().getName(), p.getUser().getId(), user.getName(), user.getId()));
			
			//neu nguoi bi kich dang ghop ga, thi se khong dc kick
			if (extension.getGame().getChickenvalue() > 0 && extension.getGame().haveInChickenList(p.getUser()))
			{
				extension.log( String.format("kickPlayer: Player %s(%d) is have chicken, not kick", p.getUser().getName(), p.getUser().getId()));
				extension.sendInvalid(user, ServerMsgID.EXT_EVENT_KICK_RES);
				return;
			}
			
			p.setPlayer(false);

			SFSObject ntfObj = new SFSObject();
	        ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, p.getUser().getName());
	        ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, room.getPlayerman().toPlayerString("-"));
	        
	        extension.sendGameEvent(ServerMsgID.EXT_EVENT_USER_KICKED_NOTIF, ntfObj);
	        
	        extension.getApi().leaveRoom(p.getUser(), extension.getParentRoom(), true, false);
	        
	        UserLeaveRoomCmd cmd  = new UserLeaveRoomCmd();
			cmd.setUser(p.getUser());
			try {
				extension.getGamehandler().serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				extension.log(StringUtil.stackTraceToString(e));
			} 
		}
	}

}
