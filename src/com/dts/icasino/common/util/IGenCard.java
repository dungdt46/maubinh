package com.dts.icasino.common.util;

import java.util.ArrayList;

import com.dts.icasino.common.entity.CardInfo;

public interface IGenCard {
	public CardInfo getCard(int cardid);
	
	public ArrayList<CardInfo> generateCard(int count, boolean isopen, String listauto);
	
	public void clear();
}
