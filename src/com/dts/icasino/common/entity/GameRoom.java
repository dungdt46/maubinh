package com.dts.icasino.common.entity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

import com.dts.icasino.common.GameConst;
import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.am.BalanceResp;
import com.dts.icasino.common.am.MatchFinishResult;
import com.dts.icasino.common.am.MatchResult;
import com.dts.icasino.common.handler.cmd.Spectator2PlayerCmd;
import com.dts.icasino.common.util.IGenCard;
import com.dts.icasino.common.util.StringUtil;
import com.dts.icasino.maubinh.MaubinhExtension;
import com.dts.icasino.maubinh.MaubinhPlayer;
import com.elcom.utils.DateTimeUtil;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSVariableException;

//class abstract xu ly cac event co ban cua 1 room

public abstract class GameRoom {

	public static final int GAME_TABLE_STATUS_START = 1;
	public static final int GAME_TABLE_STATUS_PLAYING = 2;
	public static final int GAME_TABLE_STATUS_FINISH = 3;

	public static final int GAME_MATCH_TYPE_WIN = 1;
	public static final int GAME_MATCH_TYPE_LOST = 2;
	public static final int GAME_MATCH_TYPE_DRAW = 3;

	protected GameExtension extension;
	
	protected int moneytype = 0;

	protected StringBuffer description = new StringBuffer();

	protected String listUser = "";

	protected TableConfig config;

	// auto card for bot
	protected HashMap<String, String> listautocard = new HashMap<String, String>();

	protected boolean isBotMode = false;

	protected PlayerMannager playerman = new PlayerMannager();

	protected IGenCard genCardLogic;

	protected int status = GAME_TABLE_STATUS_START;

	protected double chickenvalue = 0;

	protected int chickenlevel = 0;

	protected boolean isFirstPlay = true;

	protected boolean isFirstRound = true;

	protected int current_play = 0;

	protected String matchid = "";

	protected ArrayList<PlayerInfo> listHaveChicken = new ArrayList<PlayerInfo>();

	public GameRoom(GameExtension ext) {
		super();
		this.extension = ext;
	}

	public abstract void startDealCard();

	public boolean checkStart() {
		return true;
	}

	public void start() {
		if (status == GAME_TABLE_STATUS_PLAYING) {
			extension.log("Gameroom already start");
			return;
		}

		extension.log("Start: Start game, deal all card to player");
		status = GAME_TABLE_STATUS_PLAYING;
		matchid = UUID.randomUUID().toString();
		listUser = StringUtil.toPlayerString(playerman.getListPlayer());

		playerman.sendGameStatus2Zone(extension, true);

		if (isBotMode == true) {
			extension.log("Start: Bot mode is on, wait bot send start deal");
			return;
		} else
			startDealCard();
	}

	public void startForBotMode() {
		if (status == GAME_TABLE_STATUS_PLAYING) {
			extension.log("Gameroom already start");
			return;
		}
		extension.cancelTimer();
		extension.setStartWaitReady(extension.getConfig().getGameWaitStart() + 10);
		extension.setPlaying(true);
		extension.log("Start: Start game, deal all card to player");
		status = GAME_TABLE_STATUS_PLAYING;
		matchid = UUID.randomUUID().toString();
		listUser = StringUtil.toPlayerString(playerman.getListPlayer());

		playerman.sendGameStatus2Zone(extension, true);

		startDealCard();
	}

	protected abstract void victory();

	public TableConfig getConfig() {
		return config;
	}

	public void setConfig(TableConfig config) {
		this.config = config;
	}

	public int getStatus() {
		return status;
	}

	public abstract void onResetGame();

	public abstract void leaveGame(PlayerInfo info);

	public void resetGame() {
		extension.log("ResetGame: Set all game info default");
		extension.cancelTimer();
		((MaubinhExtension) extension).setCurrentTimeSort(0);

		this.isFirstPlay = true;
		this.isFirstRound = true;
		this.current_play = -1;
		this.isBotMode = false;
		this.listautocard.clear();
		this.description.setLength(0);
		this.listUser = "";

		this.status = GAME_TABLE_STATUS_START;
		genCardLogic.clear();

		playerman.resetGame(extension, config);
		checkChicken();

		onResetGame();
		sendListUserUpdate();
		setPlayerListVar();
	}

	public void maintain() {
		extension.log("ResetGame: Game maintain, destroy room anh div chicken");
		if (chickenvalue > 0 && listHaveChicken != null && listHaveChicken.size() > 0)
			divChicken();

		extension.destroyRoom();
	}

	public void joinQueuePlayer() {
		int count = extension.getGameRoom().getMaxUsers() - playerman.countPlayer();
		int size = count < playerman.getListQueue().size() ? count : playerman.getListQueue().size();
		if (size > 0) {
			extension.log(String.format("Join more %d queue player", count));

			for (int i = 0; i < size; i++) {
				PlayerInfo pinfo = playerman.getListQueue().remove(0);
				// playerman.removeInfo(pinfo.getUser());

				try {
					extension.getApi().spectatorToPlayer(pinfo.getUser(), extension.getGameRoom(), true, false);
					Spectator2PlayerCmd cmd = new Spectator2PlayerCmd();
					cmd.setUser(pinfo.getUser());
					cmd.setExtension(extension);
					cmd.processEvent();

				} catch (Exception e) {
					extension.log(StringUtil.stackTraceToString(e));
				}
			}

			// sendListUserUpdate();
		}
	}

	public void sendLeaveNotify(PlayerInfo p) {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, p.getUser().getName());
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, playerman.toPlayerString("-"));
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_USER_LEAVE_NOTIF, ntfObj);
		extension.notifyChangeRoomVariable(playerman.getListPlayer());

		extension.notifyLeave2Zone(p.getUser());
	}

	public void sendJoinNotify(PlayerInfo p) {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, p.getUser().getName());
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, playerman.toPlayerString("-"));
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_USER_JOIN_NOTIF, ntfObj);
		extension.notifyChangeRoomVariable(playerman.getListPlayer());
	}

	public void sendGameChangeNotify(User user) {
		SFSObject gameObj = new SFSObject();
		gameObj.putUtfString(ServerFieldID.EXT_FIELD_GAME_INFO, toString(user.getName()));
		extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_GAME_CHANGE_NOTIF, gameObj, user);
	}

	public void sendListUserUpdate() {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, playerman.toPlayerString("-"));
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_LIST_USER_UPDATE, ntfObj);
	}

	public void sendQueueUpdate2User(User user) {
		SFSObject queuentf = new SFSObject();
		// queuentf.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER,
		// StringUtil.list2String(playerman.getListQueue(), "-"));
		queuentf.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, playerman.toQueueString());
		extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_QUEUE_GAME_NTF, queuentf, user);
	}

	public void sendQueueUpdate() {
		SFSObject queuentf = new SFSObject();
		// queuentf.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER,
		// StringUtil.list2String(playerman.getListQueue(), "-"));
		queuentf.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, playerman.toQueueString());
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_QUEUE_GAME_NTF, queuentf);
	}

	public abstract void defaultPlay();

	public abstract String toString(String name);
	
	protected double getLastBalance(BalanceResp res){
		if (moneytype == GameConst.MONEY_TYPE_GOLD)
			return res.getLastBalance();
		else
			return res.getLastSilver();
	}
	
	protected long getRealChangeBalance(BalanceResp res){
		return res.getAmount();
	}
	
	private long getRealChangeBalance(MatchFinishResult res){
		if (moneytype == GameConst.MONEY_TYPE_GOLD)
			return res.getRealGold();
		else
			return res.getRealSilver();
	}
	
	
	private double getLastBalance(MatchFinishResult res){
		if (moneytype == GameConst.MONEY_TYPE_GOLD)
			return res.getLastGold();
		else
			return res.getLastSilver();
	}

	protected double subPlayerBalance(PlayerInfo player, String relateuser, double value, int reason, String reasondes)
	{
		double subvalue = player.getBalance() > value ? value  : player.getBalance();
		BalanceResp res = extension.getAccman().subBalance(extension.getPartnerid(), extension.getGameid(), matchid, config.getBetvalue(), extension.getGameRoom().getName(),
				player.getUser(), relateuser, subvalue, reasondes, moneytype);
		
		long realval = getRealChangeBalance(res);
		extension.log( String.format("subPlayerBalance: Player %s(%d) sub balance %d, reason %s result %d", 
    			player.getUser().getName(), player.getUser().getId(), realval, reasondes, res.getErrCode()));
		
		
		if (res.getErrCode() == 0)
		{
			addDescription(String.format("Trừ người chơi %s số tiền %d với lý do %s", player.getUser().getName(), realval, reasondes));
			player.setBalance(getLastBalance(res));
			player.addCurrentbet(realval);
			setPlayerBalance(player.getUser(), res.getLastBalance(), res.getLastSilver(), res.getLastExp(), res.getLastLevel());
			
			if (realval != 0)
				sendChangeBalanceNotif(player, 0-realval, reason);        
	        
			return realval;
		}
		
		return 0;
	}

	protected double addPlayerBalance(PlayerInfo player, String relateuser, double value, int reason, String reasondes)
	{
		BalanceResp res = extension.getAccman().addBalance(extension.getPartnerid(), extension.getGameid(), matchid, config.getBetvalue(), extension.getGameRoom().getName(), 
				player.getUser(), relateuser, value, reasondes, moneytype);
		
		long realval = getRealChangeBalance(res);
		extension.log( String.format("addPlayerBalance: Player %s(%d) add balance %d, reason %s result %d", 
    			player.getUser().getName(), player.getUser().getId(), realval, reasondes, res.getErrCode()));
		
		if (res.getErrCode() == 0)
		{
			addDescription(String.format("Cộng người chơi %s số tiền %d với lý do %s", player.getUser().getName(), realval, reasondes));
			
			player.setBalance(getLastBalance(res));
			//player.addCurrentbet(res.getAmount());
			setPlayerBalance(player.getUser(), res.getLastBalance(), res.getLastSilver(), res.getLastExp(), res.getLastLevel());
			
			sendChangeBalanceNotif(player, realval, reason);
			return realval;
		}
		
		return 0;
	}

	protected double refund(PlayerInfo player, double value, int reason, String reasondesc){
		BalanceResp res = extension.getAccman().refund(extension.getPartnerid(), extension.getGameid(), extension.getGameRoom().getName(), 
				matchid, player.getUser().getName(), (long)value, reasondesc, moneytype);
		
		//long realval = getRealChangeBalance(res);
		long realval = res.getAmount();
		extension.log( String.format("refund: Player %s(%d) add balance %d, reason %s result %d", 
    			player.getUser().getName(), player.getUser().getId(), realval, reasondesc, res.getErrCode()));
		
		if (res.getErrCode() == 0)
		{
			addDescription(String.format("Cộng người chơi %s số tiền %d với lý do %s", player.getUser().getName(), realval, reasondesc));
			
			player.setBalance(getLastBalance(res));
			setPlayerBalance(player.getUser(), res.getLastBalance(), res.getLastSilver(), res.getLastExp(), res.getLastLevel());
			
			sendChangeBalanceNotif(player, realval, reason);
			return realval;
		}
		
		return 0;
	}
	
	protected double finishGame(PlayerInfo player, String relateuser, double value, int reason, String reasondes, int exp, int matchtype)
	{
		MatchFinishResult res = extension.getAccman().finishMatch(extension.getPartnerid(), extension.getGameid(), matchid, config.getBetvalue(), extension.getGameRoom().getName(), 
				player.getUser(), relateuser, value, exp, matchtype, reasondes, moneytype);
		
		long realchange = getRealChangeBalance(res);
		extension.log( String.format("finishGame: Player %s(%d) change balance %d, reason %s result %d, resstring = %s", 
    			player.getUser().getName(), player.getUser().getId(), realchange, reasondes, res.getErrCode(), res.getErrDesc()));
		
		if (res.getErrCode() == 0)
		{
			addDescription(String.format("Cộng người chơi %s số tiền %d với lý do %s", player.getUser().getName(), realchange, reasondes));
			player.setBalance(getLastBalance(res));
			setUserInfoAfterGame(player.getUser(), res);
			
			sendChangeBalanceNotif(player, realchange, reason);
			return Math.abs(realchange);
		}
		
		return 0;
	}

	public void sendNeedReady(User user) {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
		extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_START_READY_TIMER_NOTIF, ntfObj, user);
	}

	public void sendNeedReady2All() {
		ArrayList<PlayerInfo> listPlayer = playerman.getListPlayer();
		for (int i = 0; i < listPlayer.size(); i++)
			sendNeedReady(listPlayer.get(i).getUser());
	}

	public void sendChangeBalanceNotif(PlayerInfo player, double value, int reason) {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, player.getUser().getName());
		ntfObj.putDouble(ServerFieldID.EXT_VAL_AMOUNT_BALANCE, value);
		ntfObj.putDouble(ServerFieldID.EXT_FIELD_BET_VALUE, player.getCurrentbet());
		ntfObj.putInt(ServerFieldID.EXT_FIELD_CHANGE_BALANCE_TYPE, reason);

		extension.sendGameEvent(ServerMsgID.EXT_EVENT_CHANGE_BALANCE_NOTIF, ntfObj);
	}

	public abstract PlayerInfo createNewPlayer();

	public PlayerInfo newPlayer(User user) {
		PlayerInfo playerInfo = createNewPlayer();
		// playerInfo.setPlayer(user.isPlayer());
		playerInfo.setPlayer(true);
		playerInfo.setUser(user);
		playerInfo.getUserBalance(moneytype);
		extension.log( String.format("NewPlayer: Player %s(%d) have balance %f", 
    			user.getName(), user.getId(), playerInfo.getBalance()));

		UserVariable chickenval = user.getVariable(ServerFieldID.EXT_VAL_USE_CHICKEN);
		if (chickenval != null) {
			extension.log(String.format("NewPlayer: Player %s(%d) have chicken status %d", user.getName(), user.getId(),
					chickenval.getIntValue()));
			playerInfo.setHaveChicken(chickenval.getIntValue() == 1);
		}

		return playerInfo;
	}

	public void removeInfo(PlayerInfo info) {
		playerman.removeInfo(info.getUser());

		sendLeaveNotify(info);
		setPlayerListVar();

		// neu nguoi leave la khach thi cap nhat lai danh sach hang doi
		if (info.isPlayer() == false)
			sendQueueUpdate();

		if (extension.getGame().getConfig().isHaveChicken())
			extension.getGame().removeFromChickenList(info.getUser());

		if (listHaveChicken.size() == 1)
			eatChicken(listHaveChicken.get(0));
	}

	public void setPlayerBalance(User user, double value) {
		extension.log(
				String.format("setPlayerBalance: user %s(%d) set balance %f", user.getName(), user.getId(), value));
		UserVariable val = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_BALANCE, value);

		ArrayList<UserVariable> listval = new ArrayList<UserVariable>();
		listval.add(val);
		extension.getApi().setUserVariables(user, listval);
	}

	public void setPlayerBalance(User user, double value, double silver, int exp, int level)
	{
		extension.log(String.format("setPlayerBalance: user %s(%d) set balance %f, silver %f, exp %d, level %d", 
				user.getName(), user.getId(), value, silver, exp, level));
		UserVariable moneyval = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_BALANCE, value);
		UserVariable silverval = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_SILVER_BALANCE, silver);
		UserVariable expval   = new SFSUserVariable(ServerFieldID.EXT_VAL_EXP, exp);
		UserVariable levelval = new SFSUserVariable(ServerFieldID.EXT_VAL_LEVEL, level);
		
		
		ArrayList<UserVariable> listval = new ArrayList<UserVariable>();
		listval.add(moneyval);
		listval.add(silverval);
		listval.add(expval);
		listval.add(levelval);
		extension.getApi().setUserVariables(user, listval);
	}
	
	protected void addWin(User user, int exp){
		MatchResult res = extension.getAccman().addWin(extension.getPartnerid(), extension.getGameid(), matchid, config.getBetvalue(), user, exp, moneytype);
		if (res.getResultCode() == 0)
			setUserInfoAfterGame(user, res);
	}
	
	protected void addLoss(User user, int exp){
		MatchResult res = extension.getAccman().addLost(extension.getPartnerid(), extension.getGameid(), matchid, config.getBetvalue(), user, exp, moneytype);
		if (res.getResultCode() == 0)
			setUserInfoAfterGame(user, res);
	}
	
	private void setUserInfoAfterGame(User user, MatchResult res)
	{
		extension.log(String.format("Update %s(%d) balance = %d, award = %d, silver %d, awardsilver %d, level = %d, exp = %d", 
				user.getName(), user.getId(), res.getLastBalance(), res.getAwardGold(), res.getLastSilver(), res.getAwardSilver(), 
				res.getLastLevel(), res.getLastExp()));
		UserVariable moneyval = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_BALANCE, res.getLastBalance());
		UserVariable silverval = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_SILVER_BALANCE, res.getLastSilver());
		UserVariable awardval = new SFSUserVariable(ServerFieldID.EXT_VAL_AWARD_MONEY, res.getAwardGold());
		UserVariable awardsilverval = new SFSUserVariable(ServerFieldID.EXT_VAL_AWARD_SILVER, res.getAwardSilver());
		UserVariable expval   = new SFSUserVariable(ServerFieldID.EXT_VAL_EXP, res.getLastExp());
		UserVariable levelval = new SFSUserVariable(ServerFieldID.EXT_VAL_LEVEL, res.getLastLevel());
		
		ArrayList<UserVariable> listval = new ArrayList<UserVariable>();
		listval.add(moneyval);
		listval.add(awardval);
		listval.add(expval);
		listval.add(levelval);
		listval.add(silverval);
		listval.add(awardsilverval);
		extension.getApi().setUserVariables(user, listval);
	}
	
	private void setUserInfoAfterGame(User user, MatchFinishResult res)
	{
		extension.log(String.format("Update %s(%d) balance = %d, award = %d, level = %d, exp = %d", user.getName(), user.getId(), 
				res.getLastGold(), res.getAwardGold(), res.getLastLevel(), res.getLastExp()));
		UserVariable moneyval = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_BALANCE, res.getLastGold());
		UserVariable silverval = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_SILVER_BALANCE, res.getLastSilver());
		UserVariable awardval = new SFSUserVariable(ServerFieldID.EXT_VAL_AWARD_MONEY, res.getAwardGold());
		UserVariable awardsilverval = new SFSUserVariable(ServerFieldID.EXT_VAL_AWARD_SILVER, res.getAwardSilver());
		UserVariable expval   = new SFSUserVariable(ServerFieldID.EXT_VAL_EXP, res.getLastExp());
		UserVariable levelval = new SFSUserVariable(ServerFieldID.EXT_VAL_LEVEL, res.getLastLevel());
		
		ArrayList<UserVariable> listval = new ArrayList<UserVariable>();
		listval.add(moneyval);
		listval.add(awardval);
		listval.add(expval);
		listval.add(levelval);
		listval.add(silverval);
		listval.add(awardsilverval);
		extension.getApi().setUserVariables(user, listval);
	}

	public void increaseMatchNew(PlayerInfo user, int exp, long value, int typeMatch) {
		/*
		 * incMatchNew(String partnerid, int gameid, String matchid, long
		 * betvalue, User user, int exp, long amount, int type);
		 */
		MatchResult res = extension.getAccman().incMatchNew(extension.getPartnerid(), extension.getGameid(), matchid,
				config.getBetvalue(), user.getUser(), exp, value, typeMatch, moneytype);
		extension.log("Increase MatchNew code = " + res.getResultCode());
		if (res.getResultCode() == 0) {
			double changeBalance = ((MaubinhPlayer) user).getBalanceChange();
			sendChangeBalanceNotif(user, changeBalance, 0);
			setUserInfoAfterGame(user.getUser(), res);
		}
	}

	protected void eatChicken(PlayerInfo player) {
		if (haveInChickenList(player.getUser())) {
			extension.log(String.format("EatChicken: Player %s(%d) eat chicken", player.getUser().getName(),
					player.getUser().getId()));
			addPlayerBalance(player, "", chickenvalue, GameConst.GAME_CHANGE_BALANCE_TYPE_AN_GA, "Ä‚n gÃ ");

			chickenvalue = 0;
			chickenlevel = 0;
			sendChickenUpdate();
			listHaveChicken.clear();
		}
	}

	/*
	 * Tru ga
	 */
	protected void subChicken() {
		ArrayList<PlayerInfo> listSubChicken = playerman.getListPlayer();
		if (chickenlevel > extension.getConfig().getThresholdChickenLevel())
			listSubChicken = listHaveChicken;

		if (countUseChicken(listSubChicken) > 1) {
			for (int i = 0; i < listSubChicken.size(); i++) {
				PlayerInfo p = listSubChicken.get(i);
				if (p.isHaveChicken() == true) {
					double subval = 0;
					if (haveInChickenList(p.getUser()) == true) {
						subval = subPlayerBalance(p, "", config.getBetvalue(),
								GameConst.GAME_CHANGE_BALANCE_TYPE_TRU_GA, "VÃ o gÃ ");
						extension.log(String.format("Player %s sub 1 cuoc, chicken level = %d", p.getUser().getName(),
								chickenlevel + 1));
					} else {
						subval = subPlayerBalance(p, "", (chickenlevel + 1) * config.getBetvalue(),
								GameConst.GAME_CHANGE_BALANCE_TYPE_TRU_GA, "VÃ o gÃ ");
						extension.log(
								String.format("Player %s sub %d cuoc, chicken level = %d, add to list have chicken",
										p.getUser().getName(), chickenlevel + 1, chickenlevel + 1));
						listHaveChicken.add(p);
					}

					chickenvalue = chickenvalue + subval;
				}
			}

			chickenlevel++;
			sendChickenUpdate();
		}

	}

	private int countUseChicken(ArrayList<PlayerInfo> listSubChicken) {
		int count = 0;
		for (int i = 0; i < listSubChicken.size(); i++) {
			PlayerInfo p = listSubChicken.get(i);
			if (p.isHaveChicken() == true)
				count++;
		}
		return count;
	}

	/*
	 * Chia ga
	 */
	protected void divChicken() {

		for (int i = 0; i < listHaveChicken.size(); i++) {
			PlayerInfo p = listHaveChicken.get(i);
			if (p.isPlayer()) {
				double realval = addPlayerBalance(p, "", chickenvalue / listHaveChicken.size(),
						GameConst.GAME_CHANGE_BALANCE_TYPE_AN_GA, "Chia gÃ ");
				extension.log(String.format("Player %s dived chicken %f", p.getUser().getName(), realval));
			}
		}
	}

	public boolean haveInChickenList(User user) {
		for (int i = 0; i < listHaveChicken.size(); i++) {
			if (listHaveChicken.get(i).getUser().getName().equalsIgnoreCase(user.getName()))
				return true;
		}

		return false;
	}

	public void removeFromChickenList(User user) {
		for (int i = 0; i < listHaveChicken.size(); i++) {
			if (listHaveChicken.get(i).getUser().getName().equalsIgnoreCase(user.getName())) {
				extension.log("RemoveChickenList: user " + user.getName() + " remove from chicken list, list remain "
						+ StringUtil.list2String(listHaveChicken) + ", chickenval = " + chickenvalue
						+ ", chicken level = " + chickenlevel);
				listHaveChicken.remove(i);
				break;
			}
		}
	}

	public String getChickenList() {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < listHaveChicken.size(); i++)
			list.add(listHaveChicken.get(i).getUser().getName());

		return StringUtil.list2String(list);
	}

	public void checkChicken() {
		if (listHaveChicken.size() == 0) {
			chickenvalue = 0;
			chickenlevel = 0;
			sendChickenUpdate();
			listHaveChicken.clear();
		}

		if (listHaveChicken.size() == 1)
			eatChicken(listHaveChicken.get(0));
	}

	protected void sendChickenUpdate() {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putDouble(ServerFieldID.EXT_FIELD_CHICKEN_VALUE, chickenvalue);
		ntfObj.putInt(ServerFieldID.EXT_FIELD_CHICKEN_LEVEL, chickenlevel);
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, getChickenList());

		extension.sendGameEvent(ServerMsgID.EXT_EVENT_CHICKEN_UPDATE_NTF, ntfObj);
	}

	public void sendChickenUpdate2User(User user) {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putDouble(ServerFieldID.EXT_FIELD_CHICKEN_VALUE, chickenvalue);
		ntfObj.putInt(ServerFieldID.EXT_FIELD_CHICKEN_LEVEL, chickenlevel);
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, getChickenList());

		extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_CHICKEN_UPDATE_NTF, ntfObj, user);
	}

	public final void setListautocard(String player, String listautocard) {
		this.listautocard.remove(player);
		this.listautocard.put(player, listautocard);
	}

	public String getListautocard(String player) {
		if (this.listautocard != null)
			return this.listautocard.get(player);
		return null;
	}

	public final void setBotMode(boolean isBotMode) {
		this.isBotMode = isBotMode;
	}

	public boolean getBotMode() {
		return this.isBotMode;
	}

	public final PlayerMannager getPlayerman() {
		return playerman;
	}

	public final double getChickenvalue() {
		return chickenvalue;
	}

	public final int getChickenlevel() {
		return chickenlevel;
	}

	public void addDescription(String des) {
		description.append(DateTimeUtil.formatDate(Calendar.getInstance().getTime()) + " - " + des);
		description.append("\r\n");
	}

	public void description2File() {
		if (description.length() > 0) {
			String filename = String.format("%d-%s-%d-%s-%s", extension.getGameRoom().getId(),
					extension.getGameRoom().getName(), extension.getGameid(), listUser, matchid);

			File fp = new File("history/" + filename);
			try {
				FileWriter wr = new FileWriter(fp);
				wr.write(description.toString());
				wr.flush();
				wr.close();
			} catch (IOException e) {
				extension.log(StringUtil.stackTraceToString(e));
			}
		}
	}

	public void setPlayerListVar() {
		RoomVariable listuserval = new SFSRoomVariable(ServerFieldID.EXT_FIELD_LIST_USER,
				playerman.getPlayerListString());
		
		listuserval.setGlobal(true);
		
		try {
			extension.getParentRoom().setVariable(listuserval);
		} catch (SFSVariableException e) {
			extension.log(StringUtil.stackTraceToString(e));
		}
	}

	protected boolean checkRightPlay(PlayerInfo p) {
		if (p == null)
			return false;

		if (p.isNeedplay() == false) {
			extension.log(String.format("Exception, player %s not need play", p));
			return false;
		}

		PlayerInfo info = playerman.getPlayerbyPos(current_play);
		if (info == null) {
			extension.log("Exception, can not get info of cuurent play in pos" + current_play);
			return false;
		}

		if (info.getUser().getName().compareTo(p.getUser().getName()) != 0) {
			extension.log(String.format("Exception: playcard is %s(%d) but current play is %s(%d) in pos %d",
					p.getUser().getName(), p.getUser().getId(), info.getUser().getName(), info.getUser().getId(),
					current_play));
			return false;
		}

		return true;
	}

	public String getMatchid() {
		return matchid;
	}

	public void setMatchid(String matchid) {
		this.matchid = matchid;
	}

	public final int getMoneytype() {
		return moneytype;
	}

	public final void setMoneytype(int moneytype) {
		this.moneytype = moneytype;
	}
}
