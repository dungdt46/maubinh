package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class ConfigChickenCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new ConfigChickenCmd();
	}

	@Override
	public void processEvent() {
		
	}

	@Override
	public void processRequest() {
		int config = params.getInt(ServerFieldID.EXT_FIELD_CHICKKEN_STATUS);
		extension.log( String.format("ConfigChicken: Player %s(%d) set chicken %d", user.getName(), user.getId(), config));
		if (config == 1){
			extension.getGame().getConfig().setHaveChicken(true);
			sendChikkenConfigNotif(config);
		}
		else if (config == 0){

			if (extension.getGame().getChickenvalue() == 0)
			{
				extension.getGame().getConfig().setHaveChicken(false);
				sendChikkenConfigNotif(config);
			}
			else
			{
				extension.log("Can not off chickken when value != 0");
				extension.sendInvalid(user, ServerMsgID.EXT_EVENT_GAME_CHIKKEN_CONFIG_RES);
			}
		}
		else
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_GAME_CHIKKEN_CONFIG_RES);
	}

	private void sendChikkenConfigNotif(int status){
		SFSObject resObj = new SFSObject();
        resObj.putInt(ServerFieldID.EXT_FIELD_CHICKKEN_STATUS, status);
        
        extension.sendGameEvent(ServerMsgID.EXT_EVENT_GAME_CHIKKEN_CONFIG_NOTIFY, resObj);
        extension.setChickenStatus();
	}
}
