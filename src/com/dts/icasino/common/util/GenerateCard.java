package com.dts.icasino.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.dts.icasino.common.entity.CardInfo;

public class GenerateCard implements IGenCard {

	// Cac quan bai
	private ArrayList<CardInfo> listCard = new ArrayList<CardInfo>();

	// Cac quan bai da sinh ra va lay ra
	private ArrayList<CardInfo> listRemain = new ArrayList<CardInfo>();

	private int min;

	private int max;

	private int shift = 0;

	public GenerateCard(int min, int max) {
		this.min = min;
		this.max = max;
		regenerateCardList(listCard);
		regenerateCardList(listRemain);
		Collections.shuffle(listRemain);
	}

	public GenerateCard(int min, int max, int shift) {
		this.min = min;
		this.max = max;
		this.shift = shift;
		regenerateCardList(listCard);
		regenerateCardList(listRemain);
		Collections.shuffle(listRemain);
	}

	private void regenerateCardList(ArrayList<CardInfo> listc) {
		listc.clear();
		for (int i = min; i < max; i++) {
			for (int j = 1; j < 5; j++) {
				CardInfo info = new CardInfo((i - shift) * 4 + j, i, j);
				listc.add(info);
			}
		}
	}

	public void clear() {
		// listGenerated.clear();
		regenerateCardList(listRemain);
		Collections.shuffle(listRemain);
	}

	public CardInfo getCard(int cardid) {
		for (int i = 0; i < listCard.size(); i++) {
			if (listCard.get(i).getId() == cardid)
				return listCard.get(i);
		}

		return null;
	}

	public ArrayList<CardInfo> generateCard(int count, boolean isopen, String listauto) {
		// gen quan bai
		Random rand = new Random();
		ArrayList<CardInfo> res = new ArrayList<CardInfo>();
		int autocount = 0;
		int chiID = 1;
		if (listauto != null && listauto.length() > 0) {
			String[] listidx = listauto.split(",");

			for (int i = 0; i < listidx.length; i++) {
				int idx = Integer.parseInt(listidx[i].trim());
				if (i > 4 && i < 10)
					chiID = 2;
				else if (i >= 10)
					chiID = 3;
				int pos = -1;
				for (int j = 0; j < listRemain.size(); j++) {
					if (listRemain.get(j).getId() == idx) {
						pos = j;
						break;
					}
				}

				if (pos != -1) {
					// listGenerated.add(pos);
					CardInfo c = listRemain.remove(pos);
					c.setLineid(chiID);
					res.add(c);
					autocount++;
				}
			}
		}

		count = count - autocount;
		
		for (int i = 0; i < count; i++) {
			if (i > 4 && i < 10)
				chiID = 2;
			else if (i >= 10)
				chiID = 3;
			int nextnumber = rand.nextInt(listRemain.size());
			CardInfo c = listRemain.remove(nextnumber);
			c.setLineid(chiID);
			c.setTurnup(isopen);
			res.add(c);
		}

		return res;
	}

	public void printAllCard() {
		for (int i = 0; i < listCard.size(); i++)
			System.out.println(listCard.get(i).toStr());
	}

	public static void main(String[] args) {
		(new GenerateCard(3, 16)).printAllCard();
	}
}
