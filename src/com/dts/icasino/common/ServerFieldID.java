package com.dts.icasino.common;

public class ServerFieldID
{

	public static final String EXT_FIELD_USERID = "uid";
    public static final String EXT_FIELD_POSITION = "p";
    public static final String EXT_FIELD_IS_TURN_UP_ALL = "tua";
    public static final String EXT_FIELD_LIST_CARD = "lc";
    public static final String EXT_FIELD_LIST_USER = "lu";
    public static final String EXT_FIELD_LIST_QUEUE_USER = "lqu";
    public static final String EXT_FIELD_ERROR_STRING = "eexp";
    public static final String EXT_FIELD_GAME_RESULT = "rg";
    public static final String EXT_FIELD_BET_TYPE			= "bet";
    public static final String EXT_FIELD_BET_VALUE			= "betvl";
    public static final String EXT_FIELD_CARD_ID			= "crdvl";
    public static final String EXT_FIELD_GAME_ADD_BET_VALUE = "gabv";
    public static final String EXT_FIELD_RESULT = "rscode";
    public static final String EXT_FIELD_OLD_RESULT = "rslt";
    public static final String EXT_FIELD_GAME_INFO = "ginf";
    public static final String EXT_FIELD_WIN_INFO = "winf";
    public static final String EXT_FIELD_GAME_EVENT = "gchginf";
    public static final String EXT_FIELD_CARD_OPEN = "cropn";
    public static final String EXT_FIELD_VICTORY_TYPE = "vttp";
    public static final String EXT_FIELD_TOITRANG_TYPE = "ttt";
    public static final String EXT_FIELD_VICTORY_POS  = "vtps";
    public static final String EXT_FIELD_IS_FIRST_ROUND  = "isfr";
    public static final String EXT_FIELD_CARD_COUNT = "cc";
    public static final String EXT_FIELD_LIST_2CARD = "l2c";
    public static final String EXT_FIELD_CHANGE_BALANCE_TYPE = "cbt";
    public static final String EXT_FIELD_BOT_TYPE = "bottp";
    public static final String EXT_FIELD_CHICKEN_VALUE = "chkvl";
    public static final String EXT_FIELD_CHICKEN_LEVEL = "chklv";
    public static final String EXT_FIELD_SAM_LIST = "smlst";
    public static final String RECHARGE_RESULT = "rcs";
    public static final String EXT_FIELD_RESULT_GAME = "rg";
    public static final String EXT_FIELD_GAME_BET_VALUE = "gbv";
    public static final String EXT_FIELD_ERROR_CODE = "errc";
    public static final String EXT_FIELD_CHICKKEN_STATUS = "chks";
    public static final String EXT_FIELD_DURATION = "drt";
    public static final String EXT_FIELD_CONFIRM = "cf";
    public static final String EXT_FIELD_LEAVE_STATUS = "lstt";
    public static final String EXT_FIELD_GAME_CHIP = "gmch";
    public static final String EXT_FIELD_MAX_CHIP = "maxch";
    public static final String EXT_FIELD_BUY_CHIP = "bych";
    public static final String EXT_FIELD_BRING_CHIP = "brch";
    public static final String EXT_FIELD_MIN_BET="mb";
    
    public static final String EXT_FIELD_LIST_CARD_OF_OTHER = "olc";
    public static final String EXT_FIELD_BET_TYPE_ALLOW		= "betal";
    public static final String EXT_FIELD_BET_TOTAL			= "bettt";
    public static final String EXT_FIELD_CURRENT_BET		= "crbet";
    public static final String EXT_FIELD_ALL_PLAYER_CARD = "alllc";
    
    public static final String EXT_FIELD_ANIMAL_ID = "aid";
    public static final String EXT_FIELD_TOTAL_BET_OF_CELL = "tboc";
    public static final String EXT_FIELD_TOTAL_BET_OF_USER = "tbou";
    
    //vartibale
    public static final String EXT_VAL_AMOUNT_BALANCE = "amf";
    public static final String EXT_VAL_AMOUNT_SILVER_BALANCE = "amfs";
    public static final String EXT_VAL_LEVEL = "lvl";
    public static final String EXT_VAL_EXP = "exp";
    public static final String EXT_VAL_AWARD_MONEY = "awm";
    public static final String EXT_VAL_AWARD_SILVER = "awms";
    public static final String EXT_VAL_USE_CHICKEN = "uck";
    public static final String EXT_VAL_MAX_CHIP = "mxc";
    
    public static final String EXT_FIELD_ROOM_ID = "roomid";                // room id
    public static final String EXT_FIELD_ZONE_ID = "zoneid";                // zone id
    public static final String EXT_FIELD_PLAYING_STATUS = "plistatus";      // player playing status
    public static final String EXT_FIELD_USERNAME = "usrn";
    public static final String EXT_FIELD_GAME_ID = "gameid";
  //MAU BINH
    public static final String EXT_FIELD_ACT_MB = "act";
    public static final String EXT_FIELD_CARD_SORT = "cs";
    public static final String EXT_FIELD_GAME_STATUS = "gs";
    public static final String EXT_FIELD_TIME = "time";
}
