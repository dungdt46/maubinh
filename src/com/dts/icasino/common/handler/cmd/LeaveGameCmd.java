package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.GameResultCode;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.handler.GameEventHandler;
import com.dts.icasino.common.util.StringUtil;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class LeaveGameCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new LeaveGameCmd();
	}

	@Override
	public void processEvent() {
		
	}

	@Override
	public void processRequest() {
		PlayerInfo info = extension.getGame().getPlayerman().getInfo(user);
		if (info == null)
		{
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_GAME_LEAVE_RES);
			return;
		}
		
		if (extension.isPlaying() == false){
			extension.getApi().leaveRoom(user, extension.getGameRoom(), true, false);
			
			UserLeaveRoomCmd cmd  = new UserLeaveRoomCmd();
			cmd.setUser(user);
			try {
				extension.getGamehandler().serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				extension.log(StringUtil.stackTraceToString(e));
			} 
		}
		else
		{
			if (info.isPlayer())
			{
				Integer confirm = params.getInt(ServerFieldID.EXT_FIELD_CONFIRM);
				if (confirm == null || confirm == 0)
				{
					SFSObject resObj = new SFSObject();
			        resObj.putInt(ServerFieldID.EXT_FIELD_RESULT, GameResultCode.GAME_IS_PLAYING);
			        resObj.putInt(ServerFieldID.EXT_FIELD_OLD_RESULT, GameResultCode.GAME_IS_PLAYING);
			        
			        extension.send(ServerMsgID.EXT_EVENT_GAME_LEAVE_RES, resObj, user);
				}
				else
					extension.getGame().leaveGame(info);
				
			}
			else
				extension.getApi().leaveRoom(user, extension.getGameRoom());
		}
	}

}
