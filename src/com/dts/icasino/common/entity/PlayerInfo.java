package com.dts.icasino.common.entity;

import java.util.ArrayList;

import com.dts.icasino.common.GameConst;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.util.StringUtil;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.variables.UserVariable;

public class PlayerInfo {

	public static final int GAME_JOIN_TYPE_NONE = 0;
	public static final int GAME_JOIN_TYPE_PLAY = 1;
	public static final int GAME_JOIN_TYPE_QUEUE = 2;
	public static final int GAME_JOIN_TYPE_VIEW = 3;
	// cac bo cua 1 chi
	public static final int GAME_CARD_TYPE_NONE = 0;
	public static final int GAME_CARD_TYPE_MAU_THAU = 1;
	public static final int GAME_CARD_TYPE_DOI = 2;
	public static final int GAME_CARD_TYPE_THU = 3;
	public static final int GAME_CARD_TYPE_SAM_CO = 4;
	public static final int GAME_CARD_TYPE_SANH = 5;
	public static final int GAME_CARD_TYPE_THUNG = 6;
	public static final int GAME_CARD_TYPE_CU_LU = 7;
	public static final int GAME_CARD_TYPE_TU_QUY = 8;
	public static final int GAME_CARD_TYPE_THUNG_PHA_SANH = 9;
	//cac mau binh dac biet
	public static final int GAME_CARD_TYPE_SAM_CHI_CUOI = 10;
	public static final int GAME_CARD_TYPE_CU_LU_CHI_GIUA = 11;
	public static final int GAME_CARD_TYPE_TU_QUY_CHI_DAU = 12;
	public static final int GAME_CARD_TYPE_TU_QUY_CHI_GIUA = 13;
	public static final int GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU = 14;
	public static final int GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA = 15;
	
	// cac bo toi trang
	public static final int SPECIAL_CARD_TYPE_3_CAI_SANH = 17;
	public static final int SPECIAL_CARD_TYPE_3_CAI_THUNG = 18;
	public static final int SPECIAL_CARD_TYPE_LUC_PHE_BON = 19;//6 doi va 1 cay le
	public static final int SPECIAL_5DOI_1SAM = 20;
	public static final int SPECIAL_CARD_TYPE_DONG_MAU_2 = 21;//12 la cung mau
	public static final int SPECIAL_CARD_TYPE_DONG_MAU_1 = 22;//13 la cung mau
	public static final int SPECIAL_CARD_TYPE_SANH_RONG = 23;//tu 2 den at khong dong mau
	public static final int SPECIAL_CARD_TYPE_SANH_RONG_CUON = 24;// tu 2 den at dong mau
	public static final int SPECIAL_CARD_TYPE_BINH_LUNG = 25;

	public static final int RES_BO_CUOC = 1;
	public static final int RES_END_GAME = 2;
	public static final int MAX_COUNT_AUTO = 2;
	public PlayerInfo() {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5215702013319912476L;

	protected int level;

	// so tien ca cuoc
	protected double currentbet = 0;

	// toi luot to
	protected boolean needplay = false;

	protected double balance = 0;

	// thoi luot danh vua roi
	protected boolean ispass = false;

	protected ArrayList<CardInfo> listcard = new ArrayList<CardInfo>();

	protected boolean isPlayer = false;

	protected boolean isReady = false;

	protected boolean playing = false;

	protected boolean isDisconnected = false;

	// dang ki leave cuoi van
	protected boolean isLeave = false;

	protected boolean isBoss = false;

	protected int countAuto = 0;

	protected boolean haveChicken = true;

	protected User user;

	@Override
	public String toString() {
		return String.format("%d:%s:%d:%d:%d", user.getId(), user.getName(), isBoss ? 1 : 0, isReady ? 1 : 0,
				listcard.size());
	}

	public String toString(boolean turn) {
		return String.format("%d:%s:%d:%s", user.getId(), user.getName(), isBoss ? 1 : 0, toStateCardString(turn));
	}

	// ham hien thi danh sach cac quan bai, neu up la -1
	public String toStateCardString(boolean isturn) {
		ArrayList<CardInfo> listc = new ArrayList<CardInfo>();
		for (int i = 0; i < listcard.size(); i++) {
			if (isturn)
				listc.add(listcard.get(i));
			else
				listc.add(new CardInfo(-1, 0, 0));
		}

		return StringUtil.list2String(listc);
	}

	public void selectOpencard(int cardid) {
		for (int i = 0; i < listcard.size(); i++) {
			if (listcard.get(i).getId() == cardid) {
				listcard.get(i).setTurnup(true);
				return;
			}
		}
	}

	public ArrayList<CardInfo> getListcard() {
		return listcard;
	}

	public void dealCard(CardInfo info) {
		listcard.add(info);
	}

	public void resetRound() {
		setIspass(false);
	}

	public void resetGame() {
		setCurrentbet(0);
		setNeedplay(false);
		setReady(true);
		setIspass(false);
		setPlaying(false);

		listcard.clear();
	}

	public void setListcard(ArrayList<CardInfo> listcard) {
		this.listcard = listcard;
	}

	public double getCurrentbet() {
		return currentbet;
	}

	public void addCurrentbet(double bet) {
		this.currentbet = this.currentbet + bet;
	}

	public void subCurrentbet(double bet) {
		this.currentbet = this.currentbet - bet;
	}

	public void setCurrentbet(double currentbet) {
		this.currentbet = currentbet;
	}

	public void defaultOpenCard() {
		if (isOpenedFirstCard() == false)
			listcard.get(0).setTurnup(true);
	}

	public boolean isOpenedFirstCard() {
		if (listcard.size() != 2)
			return true;

		for (int i = 0; i < listcard.size(); i++) {
			// tim lat quan bai dau tien
			CardInfo info = listcard.get(i);
			// da co quan lat roi
			if (info.isTurnup())
				return true;
		}

		return false;
	}

	public void addBalance(double val) {
		balance = this.balance + val;
	}

	public double subBalance(double val) {
		double realval = this.balance > val ? val : this.balance;
		this.balance = this.balance - realval;
		return realval;
	}

	public void betAll() {
		balance = 0;
		currentbet = this.currentbet + balance;
	}

	public final boolean isPlayer() {
		return isPlayer;
	}

	public final void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
	}

	public final boolean isReady() {
		return isReady;
	}

	public final void setReady(boolean isReady) {
		this.isReady = isReady;
	}

	public final User getUser() {
		return user;
	}

	public final void setUser(User user) {
		this.user = user;
	}

	public String toCardString() {
		return StringUtil.list2String(listcard);
	}

	public void clearCard() {
		listcard.clear();
	}

	public boolean turnUpAllCard() {
		boolean isOpenAll = true;
		if (listcard != null && listcard.size() > 0) {
			for (int i = 0; i < listcard.size(); i++) {
				if (listcard.get(i).isTurnup() == false) {
					listcard.get(i).setTurnup(true);
					isOpenAll = false;
				}
			}
		}

		return isOpenAll;
	}

	public void addCard(CardInfo card) {
		listcard.add(card);
	}

	public void addCard(ArrayList<CardInfo> cards) {
		if (cards != null && cards.size() > 0) {
			for (int i = 0; i < cards.size(); i++)
				listcard.add(cards.get(i));
		}

	}

	public final boolean isDisconnected() {
		return isDisconnected;
	}

	public final void setDisconnected(boolean isDisconnected) {
		this.isDisconnected = isDisconnected;
	}

	public final boolean isBoss() {
		return isBoss;
	}

	public final void setBoss(boolean isBoss) {
		this.isBoss = isBoss;
	}

	public final boolean isNeedplay() {
		return needplay;
	}

	public final void setNeedplay(boolean needplay) {
		this.needplay = needplay;
	}

	public final boolean isIspass() {
		return ispass;
	}

	public final void setIspass(boolean ispass) {
		this.ispass = ispass;
	}

	public final double getBalance() {
		return balance;
	}

	public final void setBalance(double balance) {
		this.balance = balance;
	}

	public String toDesString() {
		return "PlayerInfo [currentbet=" + currentbet + ", needplay=" + needplay + ", balance=" + balance + ", ispass="
				+ ispass + ", listcard=" + listcard + ", isPlayer=" + isPlayer + ", isReady=" + isReady
				+ ", isDisconnected=" + isDisconnected + ", isBoss=" + isBoss + ", user=" + user + "]";
	}

	public final int getLevel() {
		return level;
	}

	public final void setLevel(int level) {
		this.level = level;
	}

//	@Override
//	public int compareTo(PlayerInfo other) {
//		return 0;
//	}

	public final int getCountAuto() {
		return countAuto;
	}

	public final void incCountAuto() {
		this.countAuto++;
	}

	public final void setCountAuto(int countAuto) {
		this.countAuto = countAuto;
	}

	public final boolean isPlaying() {
		return playing;
	}

	public final void setPlaying(boolean playing) {
		this.playing = playing;
	}

	public final boolean isLeave() {
		return isLeave;
	}

	public final void setLeave(boolean isLeave) {
		this.isLeave = isLeave;
	}

	public final boolean isHaveChicken() {
		return haveChicken;
	}

	public final void setHaveChicken(boolean haveChicken) {
		this.haveChicken = haveChicken;
	}
	
	public void getUserBalance(int type){
		UserVariable val = user.getVariable(ServerFieldID.EXT_VAL_AMOUNT_SILVER_BALANCE);
		if (type == GameConst.MONEY_TYPE_GOLD){
			val = user.getVariable(ServerFieldID.EXT_VAL_AMOUNT_BALANCE);
		}
		
		if (val != null)
			this.balance = val.getDoubleValue();
	}
}
