package com.dts.icasino.common.handler;

import java.util.concurrent.TimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.handler.cmd.BaseCmd;
import com.dts.icasino.common.handler.cmd.CmdFactory;
import com.dts.icasino.common.util.StringUtil;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

public class EventHandler extends BaseServerEventHandler{
	
	private CmdFactory factory = CmdFactory.getInstance();
	
	private Log logger = LogFactory.getLog(this.getClass());

	@Override
	public void handleServerEvent(ISFSEvent event) throws SFSException {
		GameExtension extension = (GameExtension) getParentExtension();
		BaseCmd cmd = factory.getCmd(event.getType().name());
		
		if (cmd == null)
		{
			logger.error("Can not find cmd of event " + event.getType().name());
			return;
		}
		
		User user = (User) event.getParameter(SFSEventParam.USER);
		
		cmd.setEvent(event);
		cmd.setUser(user);
		//cmd.setExtension(extension);
		
		try {
			extension.getGamehandler().serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
		} catch (TimeoutException e) {
			extension.log("Exception " + StringUtil.stackTraceToString(e));
		} catch (InterruptedException e) {
			extension.log("Exception " + StringUtil.stackTraceToString(e));
		}
		
	}

	public static void main(String[] args)
	{
		String s = SFSEventType.BUDDY_ADD.name();
		System.out.println(s);
	}
}
