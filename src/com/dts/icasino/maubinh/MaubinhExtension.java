package com.dts.icasino.maubinh;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.configuration.XMLConfiguration;

import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.handler.GameEventHandler;
import com.dts.icasino.common.handler.RequestHandler;
import com.dts.icasino.common.handler.cmd.CmdFactory;
import com.dts.icasino.common.util.StringUtil;
import com.dts.icasino.maubinh.cmd.AutoOpenCardCmd;
import com.dts.icasino.maubinh.cmd.FinishCardLinkCmd;
import com.dts.icasino.maubinh.cmd.FinishGameCmd;
import com.dts.icasino.maubinh.cmd.ListCardLinkCmd;
import com.elcom.utils.DebugUtil;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;

public class MaubinhExtension extends GameExtension {
	public static int TIME_SO_CHI = 15000;
	public static int TIME_KO_SO_CHI = 8000;
	public static int TIME_RESET_SO_CHI = 22000;
	public static int TIME_RESET_KO_SO_CHI = 15000;
	
	private int open_auto_card_time = 60 * 1000;
	private int delay_finish_game = 15 * 1000;
	private long currentTimeSort = 0;
	private Timer timerFinishGame;

	// update for read config
	private long lastFileModify = 0;
	private XMLConfiguration configMB;
	private int minratioBalance = 20;
	@Override
	public void onInit() {
		gameid = 137;
		gamename = "MauBinh";

		CmdFactory.getInstance().addCommand(ServerMsgID.EXT_EVENT_LIST_CARD_LINK, new ListCardLinkCmd());
		CmdFactory.getInstance().addCommand(ServerMsgID.EXT_EVENT_FINISH_CARD_LINK_REQ, new FinishCardLinkCmd());
		addRequestHandler(ServerMsgID.EXT_EVENT_LIST_CARD_LINK, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_FINISH_CARD_LINK_REQ, RequestHandler.class);

		game = new MaubinhRoom(this);
		readConfigForMaubinh();
	}

	@Override
	public void onDestroy() {
		cancelTimerFinishGame();
	}

	public void startOpenCardTimer() {
		timer = new Timer(getTimerName());
		timer.schedule(new OpenCardTask(), open_auto_card_time);// 45s to turnup
																// all cards
		this.currentTimeSort = System.currentTimeMillis();
		log(String.format("Start open card timer, duration = " + open_auto_card_time + "/current Time = "
				+ this.currentTimeSort));
	}

	public void startFinishGameTimer(MaubinhRoom gameRoom, ArrayList<PlayerInfo> players) {
		timerFinishGame = new Timer(getTimerName());
		timerFinishGame.schedule(new FinishGameTask(gameRoom, players), this.delay_finish_game);
		log(String.format("startFinishGameTimer, duration = %ds ", this.delay_finish_game));
	}

	public class OpenCardTask extends TimerTask {
		@Override
		public void run() {
			AutoOpenCardCmd cmd = new AutoOpenCardCmd();
			try {
				gamehandler.serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				log("Exception : " + StringUtil.stackTraceToString(e));
			}
		}
	}

	public class FinishGameTask extends TimerTask {
		private MaubinhRoom gameRoom;
		private ArrayList<PlayerInfo> listPlayer;

		public FinishGameTask(MaubinhRoom gameRoom, ArrayList<PlayerInfo> players) {
			super();
			this.gameRoom = gameRoom;
			this.listPlayer = players;
		}

		@Override
		public void run() {
			FinishGameCmd cmd = new FinishGameCmd(gameRoom, listPlayer);
			try {
				gamehandler.serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				log("Exception : " + StringUtil.stackTraceToString(e));
			}
		}
	}

	public void setRoomVaribale() {
		super.setRoomVaribale();
		RoomVariable opencard = new SFSRoomVariable("opencardtime", config.getOpenFirstCardTime());
		opencard.setGlobal(true);
		
		RoomVariable sochitime = new SFSRoomVariable("timesochi", 4);
		opencard.setGlobal(true);

		List<RoomVariable> lst = new ArrayList<RoomVariable>();
		lst.add(opencard);
		lst.add(sochitime);

		// getApi().setRoomVariables(user, getGameRoom(), lst);
		getParentRoom().setVariables(lst);
	}

	@Override
	public void sendEndEvent() {
		ISFSObject ntfObj = new SFSObject();
		String listInfo = "";// Username|listcard;username|listcard
		ArrayList<PlayerInfo> listPlayer = this.getGame().getPlayerman().getListPlayer();
		for (int i = 0; i < listPlayer.size(); i++) {
			String username = listPlayer.get(i).getUser().getName();
			String listcard = ((MaubinhPlayer) listPlayer.get(i)).toSortCard();
			listInfo += username + "|" + listcard + ";";
		}

		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_CARD, listInfo);
		this.sendGameEvent(ServerMsgID.EXT_EVENT_END_GAME_NOTIF, ntfObj);
	}

	public int getOpen_auto_card_time() {
		return open_auto_card_time;
	}

	public void setOpen_auto_card_time(int open_auto_card_time) {
		this.open_auto_card_time = open_auto_card_time;
	}

	public long getCurrentTimeSort() {
		return currentTimeSort;
	}

	public void setCurrentTimeSort(int currentTimeSort) {
		this.currentTimeSort = currentTimeSort;
	}

	public int getDelay_finish_game() {
		return delay_finish_game;
	}

	public void setDelay_finish_game(int delay_finish_game) {
		this.delay_finish_game = delay_finish_game;
	}

	public void cancelTimerFinishGame() {
		if (timerFinishGame != null) {
			timerFinishGame.cancel();
			timerFinishGame = null;
		}
	}
	
	private void readConfigForMaubinh() {
		String currentDir = System.getProperty("user.dir");
		String configPath = currentDir + "/" + "config/extension-config.xml";

		File fp = new File(configPath);
		long fileModify = fp.lastModified();
		log("Read file config for Mau Binh " + configPath + "..." + fileModify);
		if (fileModify != lastFileModify) {
			try {
				configMB = new XMLConfiguration(configPath);
				minratioBalance = configMB.getInt("extension.maubinh.minbalance", minratioBalance);
				log("Ratio = " + minratioBalance);
				long betvalue = game.getConfig().getBetvalue();
				game.getConfig().setMinlevel(minratioBalance * betvalue);

			} catch (Exception e) {
				DebugUtil.printStackTrace(e);
			}

			lastFileModify = fileModify;
		}

	}
}
