package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.GameResultCode;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class UserUnreadyCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new UserUnreadyCmd();
	}

	@Override
	public void processEvent() {
		return;
	}

	@Override
	public void processRequest() {
		extension.log( String.format("PlayerUnready: Player %s(%d) Unready", user.getName(), user.getId()));
		GameRoom room = extension.getGame();
		
		if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING)
		{
			extension.log( String.format("PlayerReady: Player %s(%d) unready not ok because table is playing", user.getName(), user.getId()));
			extension.sendInvalid(user, ServerMsgID.EXT_EVENT_UNREADY_RES);
            return;
		}
		
		PlayerInfo pi = room.getPlayerman().getPlayerInfo(user);
        if( pi==null )
        	return;
        
        pi.setReady(false);	
        
        extension.log(String.format("Cancel ready timer of boss %s", room.getPlayerman().getBoss().toString()));
        extension.log( String.format("PlayerReady: Player %s(%d) unready ok", 
    			user.getName(), user.getId()));
    	
        SFSObject resObj = new SFSObject();
        resObj.putInt(ServerFieldID.EXT_FIELD_RESULT, GameResultCode.GAME_SUCCESS);
        resObj.putInt(ServerFieldID.EXT_FIELD_OLD_RESULT, GameResultCode.GAME_SUCCESS);
        extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_UNREADY_RES, resObj, user);
        
        SFSObject ntfObj = new SFSObject();
        ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
        extension.sendGameEvent(ServerMsgID.EXT_EVENT_UNREADY_NTF, ntfObj);
	}

}
