package com.dts.icasino.common.util;

import java.util.ArrayList;

import com.dts.icasino.common.entity.CardInfo;
import com.dts.icasino.common.entity.PlayerInfo;

public class StringUtil {
	public static String list2String(ArrayList<?> list)
	{
		if (list != null && list.size() > 0)
		{
			if (list.size() == 1)
				return list.get(0).toString();
			else
			{
				String res = "";
				for (int i=0; i<list.size() - 1; i++)
				{
					res = res + list.get(i).toString() + ",";
				}
				
				return res + list.get(list.size() - 1).toString();
			}
		}
		else
			return " ";
	}
	
	public static String list2String(ArrayList<?> list, String delim)
	{
		if (list != null && list.size() > 0)
		{
			if (list.size() == 1)
				return list.get(0).toString();
			else
			{
				String res = "";
				for (int i=0; i<list.size() - 1; i++)
				{
					res = res + list.get(i).toString() + delim;
				}
				
				return res + list.get(list.size() - 1).toString();
			}
		}
		else
			return " ";
	}
	
	public static String toCardString(ArrayList<CardInfo> listc)
	{
		if (listc != null && listc.size() > 0)
		{
			if (listc.size() == 1)
				return listc.get(0).toStr();
			else
			{
				String res = "";
				for (int i=0; i<listc.size() - 1; i++)
				{
					res = res + listc.get(i).toStr() + ",";
				}
				
				return res + listc.get(listc.size() - 1).toStr();
			}
		}
		else
			return " ";
	}
	
	public static String toPlayerString(ArrayList<PlayerInfo> listp)
	{
		if (listp != null && listp.size() > 0)
		{
			if (listp.size() == 1)
				return listp.get(0).getUser().getName();
			else
			{
				String res = "";
				for (int i=0; i<listp.size() - 1; i++)
				{
					res = res + listp.get(i).getUser().getName() + ",";
				}
				
				return res + listp.get(listp.size() - 1).getUser().getName();
			}
		}
		else
			return " ";
	}
	
	public static String toCardDescription(ArrayList<CardInfo> listc)
	{
		if (listc != null && listc.size() > 0)
		{
			if (listc.size() == 1)
				return listc.get(0).toDescription();
			else
			{
				String res = "";
				for (int i=0; i<listc.size() - 1; i++)
				{
					res = res + listc.get(i).toDescription() + ";";
				}
				
				return res + listc.get(listc.size() - 1).toDescription();
			}
		}
		else
			return " ";
	}
	
	public static String stackTraceToString(Throwable e) {
	    StringBuilder sb = new StringBuilder();
	    sb.append(e.getMessage());
	    sb.append("\n");
	    for (StackTraceElement element : e.getStackTrace()) {
	        sb.append(element.toString());
	        sb.append("\n");
	    }
	    return sb.toString();
	}
}
