package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;

public class DisconnectedUserCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new DisconnectedUserCmd();
	}

	@Override
	public void processEvent() {
		extension.log( String.format("DisconnectedPlayer: Player %s(%d) disconnected", 
				user.getName(), user.getId()));
		GameRoom room = extension.getGame();
		
		PlayerInfo playerInfo = room.getPlayerman().getInfo(user);
		
        if (playerInfo != null)
        {	
        	UserLeaveRoomCmd leavecmd = new UserLeaveRoomCmd();
			leavecmd.setExtension(extension);
			leavecmd.setUser(user);
			leavecmd.setEvent(event);
			leavecmd.processEvent();
		}
        else
        	extension.log( String.format("DisconnectedPlayer: Player %s(%d) not found", 
    				user.getName(), user.getId()));
	}

	@Override
	public void processRequest() {
		return;
	}

}
