package com.dts.icasino.common.handler.cmd;


import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Spectator2PlayerCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new Spectator2PlayerCmd();
	}

	@Override
	public void processEvent() {
		extension.log( String.format("Spectator2Player: Player %s(%d) change from spectator to " +
				"player", user.getName(), user.getId()));
		GameRoom room = extension.getGame();
		
		if (extension.getGame().getPlayerman().countPlayer() >= extension.getGameRoom().getMaxUsers() || room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING)
		{
			extension.log("Spectator2Player: can not 2 player because max player or playing");
			return;
		}		
		
		PlayerInfo playerInfo = room.getPlayerman().getInfo(user);

        if( playerInfo==null ){ //not exist, create new
        	extension.log( String.format("Spectator2Player: Player %s(%d) not exist, creat info", 
        			user.getName(), user.getId()));
    		
            playerInfo = room.newPlayer(user);
            playerInfo.setReady(true);	
        }else{
        	extension.log( String.format("Spectator2Player: Player %s(%d) existed, update info", 
        			user.getName(), user.getId()));
    		
        	playerInfo.setUser(user);
            playerInfo.setPlayer(true);
            playerInfo.getUserBalance(room.getMoneytype());
            playerInfo.setReady(true);	
            playerInfo.setDisconnected(false);
        }
        
        if (playerInfo.getBalance() < room.getConfig().getMinbalance())
		{
			extension.log( String.format("spectator2Player: Player %s(%d) have balance %f not enough money join. Kick out", 
        			user.getName(), user.getId(), playerInfo.getBalance()));
			
			SFSObject exntfObj = new SFSObject();
            exntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
            extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_USER_EXPIRE_MONEY_NOTIF, exntfObj, user);
            extension.getApi().leaveRoom(user, extension.getParentRoom());
	        
	        return;
		}
        
        room.getPlayerman().guest2Player(playerInfo);
        room.sendJoinNotify(playerInfo);
        
        //neu la luot choi dau tien va nguoi thu 2 join vao thi gui notify ready cho boss
		if (extension.getGame().getPlayerman().countPlayer() == 2 && extension.isFirst() == true)
			extension.getGame().sendNeedReady(extension.getGame().getPlayerman().getBoss().getUser());
	}

	@Override
	public void processRequest() {
		return;
	}

}
