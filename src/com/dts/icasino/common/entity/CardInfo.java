package com.dts.icasino.common.entity;

import java.io.Serializable;

public class CardInfo implements Serializable, Comparable<CardInfo> {
	public CardInfo() {
		super();
	}

	@Override
	public String toString() {
		return String.format("%d", id);
	}

	public String toStr() {
		return String.format("%s %s(%d)", getCardName(this.value), getTypeCardName(this.substance), this.getId());
	}

	public String toDescription() {
		return String.format("%s %s", getCardName(this.value), getTypeCardName(this.substance));
	}

	public void parseString(String info) {
		String[] lst = info.split("_");

		if (lst.length != 4)
			return;

		this.id = Integer.parseInt(lst[0]);
		this.value = Integer.parseInt(lst[1]);
		this.substance = Integer.parseInt(lst[2]);
		turnup = Integer.parseInt(lst[3]) == 1;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 802888231679090687L;

	public static final int CARD_SUBSTANCE_BICH = 1;
	public static final int CARD_SUBSTANCE_TEP = 2;
	public static final int CARD_SUBSTANCE_DIAMOND = 3;
	public static final int CARD_SUBSTANCE_HEART = 4;

	private int id; // cai nay chi can thang game service biet, client ko can

	private int value; // gia tri quan bai 2,3,4...

	private int substance; // chat

	private boolean turnup = false; // quan bai dang mo hay up

	private int lineid; // chi cua quan bai

	public CardInfo(int id, int value, int substance, boolean turnup, int lineid) {
		super();
		this.id = id;
		this.value = value;
		this.substance = substance;
		this.turnup = turnup;
		this.lineid = lineid;
	}

	public CardInfo(int id, int value, int substance) {
		this.id = id;
		this.value = value;
		this.substance = substance;
		this.turnup = false;
	}

	public CardInfo(int id, int value, int substance, boolean turnup) {
		this.id = id;
		this.value = value;
		this.substance = substance;
		this.turnup = turnup;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getSubstance() {
		return substance;
	}

	public void setSubstance(int substance) {
		this.substance = substance;
	}

	public boolean isTurnup() {
		return turnup;
	}

	public void setTurnup(boolean turnup) {
		this.turnup = turnup;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLineid() {
		return lineid;
	}

	public void setLineid(int lineid) {
		this.lineid = lineid;
	}

	@Override
	public int compareTo(CardInfo o) {
		int valueCompare = this.getRankValue();
		if (valueCompare < o.getRankValue())
			return -1;
		else if (valueCompare == o.getRankValue()) {
			if (substance < o.getSubstance())
				return -1;
			else if (substance == o.getSubstance())
				return 0;
			else
				return 1;
		} else
			return 1;
	}

	public String getTypeCardName(int type) {
		switch (type) {
		case CARD_SUBSTANCE_BICH:
			return "Bich";
		case CARD_SUBSTANCE_TEP:
			return "Tep";
		case CARD_SUBSTANCE_HEART:
			return "Co";
		case CARD_SUBSTANCE_DIAMOND:
			return "Do";
		}
		return "Khong biet";
	}

	public String getCardName(int num) {
		if (num > 0 && num < 11)
			return String.valueOf(num);
		switch (num) {
		case 11:
			return "J";
		case 12:
			return "Q";
		case 13:
			return "K";
		case 14:
			return "A";
		case 15:
			return "2";
		}
		return "Khong biet";
	}

	public int getRankValue() {
		int _value = this.getValue();
		if (this.getValue() == 15)
			_value = 2;
		return _value;
	}

}
