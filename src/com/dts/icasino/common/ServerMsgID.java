/**
 * 
 */
package com.dts.icasino.common;

/**
 * @author Pham Cong Phan
 *
 */
public class ServerMsgID {
	
	
	public static final String EXT_EVENT_LISTCARD_NTF = "lcntf";
    public static final String EXT_EVENT_START = "s";
    public static final String EXT_EVENT_END = "e";
    public static final String EXT_EVENT_READY_REQ = "rr";
    public static final String EXT_EVENT_READY_RES = "rdres";
    public static final String EXT_EVENT_UNREADY_REQ = "urr";
    public static final String EXT_EVENT_UNREADY_RES = "urdres";
    public static final String EXT_EVENT_READY_NTF = "rntf";
    public static final String EXT_EVENT_UNREADY_NTF = "urntf";
    public static final String EXT_EVENT_TURNUP_CARD = "tuc";
    public static final String EXT_EVENT_LIST_USER_UPDATE = "luu";
    public static final String EXT_EVENT_GAME_RESULT = "grs";
    public static final String EXT_EVENT_DEAL_CARD_REQ					= "dcrq";
	public static final String EXT_EVENT_DEAL_CARD_RES					= "dcrs";
	public static final String EXT_EVENT_KICK_REQ						= "kckrq";
	public static final String EXT_EVENT_KICK_RES						= "kckrs";
	public static final String EXT_EVENT_PLAY_CARD_REQ					= "plcrq";
	public static final String EXT_EVENT_PLAY_CARD_RES					= "plcrs";
	public static final String EXT_EVENT_PASS_CARD_REQ					= "pscrq";
	public static final String EXT_EVENT_PASS_CARD_RES					= "pscrs";
	public static final String EXT_EVENT_SELECT_OPEN_CARD_REQ			= "slstrq";
	public static final String EXT_EVENT_SELECT_OPEN_CARD_RES			= "slsrrs";
	public static final String EXT_EVENT_OPEN_LAST_CARD_REQ				= "olstrq";
	public static final String EXT_EVENT_OPEN_LAST_CARD_RES				= "olstrs";
	public static final String EXT_EVENT_VICTORY_NOTIF					= "vicntf";
	public static final String EXT_EVENT_GAME_CHANGE_NOTIF				= "gchntf";
	public static final String EXT_EVENT_USER_JOIN_NOTIF				= "jrntf";
	public static final String EXT_EVENT_USER_LEAVE_NOTIF				= "lrntf";
	public static final String EXT_EVENT_USER_READY_NOTIF				= "rdyntf";
	public static final String EXT_EVENT_DEAL_CARD_NOTIF				= "dlcntf";
	public static final String EXT_EVENT_START_GAME_NOTIF				= "strntf";
	public static final String EXT_EVENT_NEAD_PLAY_NOTIF				= "ndpntf";
	public static final String EXT_EVENT_PLAY_CARD_NOTIF				= "plcntf";
	public static final String EXT_EVENT_PASS_CARD_NOTIF				= "pscntf";
	public static final String EXT_EVENT_END_GAME_NOTIF					= "endntf";
	public static final String EXT_EVENT_OPEN_CARD_NOTIF				= "opcntf";
	public static final String EXT_EVENT_OPEN_ALL_CARD_NOTIF			= "opaltf";
	public static final String EXT_EVENT_CHANGE_BALANCE_NOTIF			= "cblltf";
	public static final String EXT_EVENT_USER_EXPIRE_MONEY_NOTIF		= "expmntf";
	public static final String CMD_ASK_ACC_PLAYING						= "aapi";
	public static final String EXT_EVENT_USER_PLAYING_NOTIF				= "apin";
	public static final String RES_ASK_ACC_PLAYING						= "rapi";
	public static final String EXT_EVENT_USER_KICKED_NOTIF    			= "kkntf";
	public static final String EXT_EVENT_USER_HAVE_SET_NOTIF    		= "hvntf";
	public static final String EXT_EVENT_SET_BOT_REQ					= "setbrq";
	public static final String EXT_EVENT_SET_BOT_RES					= "setbrs";
	public static final String EXT_EVENT_SET_CARD_REQ					= "setcrq";
	public static final String EXT_EVENT_SET_CARD_RES					= "setcrs";
	public static final String EXT_EVENT_START_DEAL_REQ					= "strdrq";
	public static final String EXT_EVENT_START_DEAL_RES					= "strdrs";
	public static final String EXT_EVENT_START_READY_TIMER_NOTIF		= "rdtntf";
	public static final String EXT_EVENT_SAM_REQ						= "smrq";
	public static final String EXT_EVENT_SAM_RES						= "smrs";
	public static final String EXT_EVENT_UNSAM_REQ						= "usmrq";
	public static final String EXT_EVENT_UNSAM_RES						= "usmrs";
	public static final String EXT_EVENT_SAM_NOTIF						= "smntf";
	public static final String EXT_EVENT_UNSAM_NOTIF					= "usmntf";
	public static final String EXT_EVENT_BAO_NTF						= "baontf";
	public static final String EXT_EVENT_CHICKEN_UPDATE_NTF				= "chkntf";
	public static final String EXT_EVENT_RAISE_REQ						= "rsrq";
	public static final String EXT_EVENT_RAISE_RES						= "rsrs";
	public static final String EXT_EVENT_RAISE_NOTIF					= "rsntf";
	public static final String EXT_EVENT_NEAD_RAISE_NOTIF				= "ndrntf";
	public static final String EXT_EVENT_NEXT_TURN						= "nt";
	public static final String NOTIFY_RECEIVED_MONEY_FROM_FRIEND 		= "notiRMFF";
	public static final String EXT_EVENT_GAME_BET_REQ 					= "gbr";
	public static final String EXT_EVENT_GAME_BET_RES 					= "gbres";
	public static final String EXT_EVENT_GAME_BET_ERROR_NOTIFY 			= "e_bntf";
	public static final String EXT_EVENT_GAME_BET_NTF 					= "gb_ntf";
	public static final String EXT_EVENT_GAME_CHIKKEN_CONFIG_REQ 		= "ckcrq";
	public static final String EXT_EVENT_GAME_CHIKKEN_CONFIG_RES 		= "ckcrs";
	public static final String EXT_EVENT_GAME_CHIKKEN_CONFIG_NOTIFY		= "ckcntf";
	public static final String EXT_EVENT_GAME_LEAVE_REQ					= "lvgrq";
	public static final String EXT_EVENT_GAME_LEAVE_RES					= "lvgrs";
	public static final String EXT_EVENT_GAME_LEAVE_NOTIF				= "lvgntf";
	public static final String EXT_EVENT_QUEUE_GAME_REQ					= "prr";
	public static final String EXT_EVENT_QUEUE_GAME_RES					= "prs";
	public static final String EXT_EVENT_USE_CHICKEN_REQ				= "ucr";
	public static final String EXT_EVENT_USE_CHICKEN_RES				= "ucs";
	public static final String EXT_EVENT_QUEUE_GAME_NTF					= "rqntf";
	public static final String EXT_EVENT_LEAVE_GAME_REQ					= "lgrq";
	public static final String EXT_EVENT_LEAVE_GAME_RES					= "lgrs";
	public static final String EXT_EVENT_ACTION_NTF						= "atntf";
	public static final String EXT_EVENT_GET_GAME_INFO_REQ 				= "ggir";
	public static final String EXT_EVENT_SUB_BALANCE_FIRST_NOTIF		= "sfstntf";
	public static final String EXT_EVENT_WAIT_BET_TIMER_NTF				= "wbntf";
	public static final String EXT_EVENT_SETUP_CHIP_REQ					= "stchrq";
	public static final String EXT_EVENT_SETUP_CHIP_RES					= "stchrs";
	public static final String EXT_EVENT_EXPIRE_CHIP_NTF				= "expcntf";
	public static final String EXT_EVENT_DRAW_NTF						= "drwntf";
	
	//MAU BINH 
	public static final String EXT_EVENT_LIST_CARD_LINK = "lcl";
	public static final String EXT_EVENT_FINISH_CARD_LINK_REQ = "efclrq";
	public static final String EXT_EVENT_FINISH_CARD_LINK_NOTIFY = "efcln";
	
}
