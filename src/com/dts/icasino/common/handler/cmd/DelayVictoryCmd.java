package com.dts.icasino.common.handler.cmd;

public class DelayVictoryCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new DelayVictoryCmd();
	}

	@Override
	public void processEvent() {
		extension.victory();
	}

	@Override
	public void processRequest() {
	}

}
