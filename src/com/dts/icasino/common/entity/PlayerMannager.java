package com.dts.icasino.common.entity;

import java.util.ArrayList;

import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.util.StringUtil;
import com.dts.icasino.maubinh.MaubinhPlayer;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;

public class PlayerMannager {

	private ArrayList<PlayerInfo> listLeaveGame = new ArrayList<PlayerInfo>();

	private ArrayList<PlayerInfo> listPlayer = new ArrayList<PlayerInfo>();

	private ArrayList<PlayerInfo> listGuest = new ArrayList<PlayerInfo>();

	private ArrayList<PlayerInfo> listQueue = new ArrayList<PlayerInfo>();

	private PlayerInfo boss;

	private int readycount = 0;
	private boolean checkAllReady = true;
	private boolean needBossReady = true;

	public int getPlayerPos(int id) {
		for (int i = 0; i < listPlayer.size(); i++) {
			if (listPlayer.get(i).getUser().getId() == id)
				return i;
		}

		return -1;
	}

	public int getPlayerPos(String name) {
		for (int i = 0; i < listPlayer.size(); i++) {
			if (listPlayer.get(i).getUser().getName().equalsIgnoreCase(name))
				return i;
		}

		return -1;
	}

	public PlayerInfo getPlayerInfo(User user) {
		for (int i = 0; i < listPlayer.size(); i++) {
			if (listPlayer.get(i).getUser().getName().equalsIgnoreCase(user.getName()))
				return listPlayer.get(i);
		}

		return null;
	}

	public PlayerInfo getGuestInfo(User user) {
		for (int i = 0; i < listGuest.size(); i++) {
			if (listGuest.get(i).getUser().getName().equalsIgnoreCase(user.getName()))
				return listGuest.get(i);
		}

		return null;
	}

	public boolean isLeavingGame(User user) {
		for (int i = 0; i < listLeaveGame.size(); i++) {
			if (listLeaveGame.get(i).getUser().getName().equalsIgnoreCase(user.getName()))
				return true;
		}

		return false;
	}

	public PlayerInfo getInfo(User user) {
		for (int i = 0; i < listPlayer.size(); i++) {
			if (listPlayer.get(i).getUser().getName().equalsIgnoreCase(user.getName()))
				return listPlayer.get(i);
		}

		for (int i = 0; i < listGuest.size(); i++) {
			if (listGuest.get(i).getUser().getName().equalsIgnoreCase(user.getName()))
				return listGuest.get(i);
		}

		for (int i = 0; i < listQueue.size(); i++) {
			if (listQueue.get(i).getUser().getName().equalsIgnoreCase(user.getName()))
				return listQueue.get(i);
		}

		return null;
	}

	public PlayerInfo getPlayerbyPos(int pos) {
		if (pos < 0 || pos >= listPlayer.size())
			return null;
		else
			return listPlayer.get(pos);
	}

	public int removePlayer(User user) {
		for (int i = 0; i < listPlayer.size(); i++) {
			if (listPlayer.get(i).getUser().getName().equalsIgnoreCase(user.getName())) {
				listPlayer.remove(i);
				return i;
			}
		}

		return -1;
	}

	public int removeGuest(User user) {
		for (int i = 0; i < listGuest.size(); i++) {
			if (listGuest.get(i).getUser().getName().equalsIgnoreCase(user.getName())) {
				listGuest.remove(i);
				return i;
			}
		}

		return -1;
	}

	public void leaveGame(PlayerInfo info) {
		removeInfo(info.getUser());
		listLeaveGame.add(info);
	}

	public void removeInfo(User user) {
		for (int i = 0; i < listPlayer.size(); i++) {
			if (listPlayer.get(i).getUser().getName().equalsIgnoreCase(user.getName())) {
				PlayerInfo info = listPlayer.remove(i);
				if (info.isBoss)
					findNewBoss();
			}
		}

		for (int i = 0; i < listGuest.size(); i++) {
			if (listGuest.get(i).getUser().getName().equalsIgnoreCase(user.getName())) {
				listGuest.remove(i);
			}
		}

		for (int i = 0; i < listQueue.size(); i++) {
			if (listQueue.get(i).getUser().getName().equalsIgnoreCase(user.getName())) {
				listQueue.remove(i);
			}
		}
	}

	public int findNewBoss() {
		for (int i = 0; i < listPlayer.size(); i++) {
			PlayerInfo pinfo = listPlayer.get(i);
			if (pinfo != null) {
				if (!pinfo.isDisconnected()) {
					pinfo.setBoss(true);
					boss = pinfo;
					return i;
				}
			}
		}

		return -1;
	}

	public ArrayList<PlayerInfo> getSpec2Player(int count, long minvalue) {
		int joincount = 0;
		ArrayList<PlayerInfo> listRes = new ArrayList<PlayerInfo>();
		for (int i = 0; i < listGuest.size(); i++) {
			if (joincount >= count)
				break;

			PlayerInfo pinfo = listGuest.get(i);
			if (pinfo.getBalance() > minvalue) {
				listRes.add(pinfo);
				joincount++;
			}
		}

		return listRes;
	}

	public void sendGameStatus2Zone(GameExtension ext, boolean status) {
		for (int i = 0; i < listPlayer.size(); i++) {
			PlayerInfo p = listPlayer.get(i);
			ext.getZonehandler().ntfUserBeginPlayingGameNtf(p.getUser(), status);
		}
	}

	public void player2Guest(PlayerInfo info) {
		removeInfo(info.getUser());
		listGuest.add(info);
	}

	public void guest2Player(PlayerInfo info) {
		removeInfo(info.getUser());
		listPlayer.add(info);
	}

	public void joinPlayer(PlayerInfo info) {
		listPlayer.add(info);
	}

	public void queuePlayer(PlayerInfo info) {
		for (int i = 0; i < listQueue.size(); i++) {
			if (listQueue.get(i).getUser().getName().equalsIgnoreCase(info.getUser().getName())) {
				listQueue.set(i, info);
				return;
			}
		}

		listQueue.add(info);
	}

	/**
	 * @return the boss
	 */
	public final PlayerInfo getBoss() {
		return boss;
	}

	public int countPlayer() {
		return listPlayer.size();
	}

	public int countGuest() {
		return listGuest.size();
	}

	public int countQueue() {
		return listQueue.size();
	}

	// neu tat ca cung up, se khong lat bai thang thang
	public int countNotPassPlayer() {
		int count = 0;
		for (int i = 0; i < listPlayer.size(); i++) {
			PlayerInfo pinfo = listPlayer.get(i);
			if (pinfo.isIspass() == false)
				count++;
		}

		return count;
	}

	/**
	 * @return the listPlayer
	 */
	public final ArrayList<PlayerInfo> getListPlayer() {
		return listPlayer;
	}

	public String toPlayerStringWithUser(String name) {
		ArrayList<PlayerInfo> listplaying = new ArrayList<PlayerInfo>();
		for (int i = 0; i < listPlayer.size(); i++)
			listplaying.add(listPlayer.get(i));

		String res = "";

		if (listplaying.size() == 0)
			return res;

		for (int i = 0; i < listplaying.size() - 1; i++) {
			PlayerInfo p = listplaying.get(i);

			if (p.getUser().getName().compareTo(name) == 0)
				res = res + p.toString(true) + ";";
			else
				res = res + p.toString(false) + ";";
		}

		PlayerInfo p = listplaying.get(listplaying.size() - 1);
		if (p.getUser().getName().compareTo(name) == 0)
			return res + p.toString(true);
		else
			return res + p.toString(false);
	}

	public String toPlayerString(String denim) {
		ArrayList<PlayerInfo> listplaying = new ArrayList<PlayerInfo>();
		for (int i = 0; i < listPlayer.size(); i++)
			listplaying.add(listPlayer.get(i));

		for (int i = 0; i < listLeaveGame.size(); i++)
			listplaying.add(listLeaveGame.get(i));

		return StringUtil.list2String(listplaying, denim);
	}

	public String toQueueString() {
		ArrayList<PlayerInfo> listq = new ArrayList<PlayerInfo>();
		for (int i = 0; i < listQueue.size(); i++) {
			if (isLeavingGame(listQueue.get(i).getUser()) == false)
				listq.add(listQueue.get(i));
		}

		return StringUtil.list2String(listq, "-");
	}

	public void checkReady() {
		readycount = 0;
		checkAllReady = true;
		needBossReady = true;
		for (int i = 0; i < listPlayer.size(); i++) { // lap kiem tra xem ready
														// het chua

			PlayerInfo p = listPlayer.get(i);
			if (!p.isReady()) {
				checkAllReady = false;

				if (p.isBoss() == false)
					needBossReady = false;
			} else
				readycount++;
		}

		if (listPlayer.size() <= 1)
			needBossReady = false;
	}

	public void resetRound() {
		for (int i = 0; i < listPlayer.size(); i++) {
			PlayerInfo p = listPlayer.get(i);
			if (p != null && p.getListcard().size() > 0)
				p.resetRound();
		}
	}

	public void resetGame(GameExtension extension, TableConfig config) {
		int i = 0;
		while (i < listPlayer.size()) {

			PlayerInfo p = listPlayer.get(i);
			p.getUserBalance(extension.getGame().getMoneytype());

			// Xoa bo player disconnect
			if (p.isDisconnected()) {
				extension.log(String.format("ResetGame: Clear disconnect player %s", p));
				leaveUser(extension, p.getUser());
			} else if (p.isLeave()) {
				extension.log(String.format("ResetGame: Clear register leave player %s", p));
				leaveUser(extension, p.getUser());
			}
			// Xoa bo player ko du tien	
			else if (p.getBalance() < config.getMinbalance()) {
				extension.log(String.format("ResetGame: player %s leave because not enough money", p));
				extension.log("Current balance user " + p.getUser().getName() + " = " + p.getBalance() + " minbet = " + config.getMinbalance());
				leaveUser(extension, p.getUser());

				SFSObject exntfObj = new SFSObject();
				exntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, p.getUser().getName());
				extension.sendGameEvent(ServerMsgID.EXT_EVENT_USER_EXPIRE_MONEY_NOTIF, exntfObj);
			}
			// Xoa bo player van truoc ko choi ma choi auto
			else if (p.getCountAuto() > extension.getConfig().getMaxAutoPlay()) {
				extension.log(String.format("ResetGame: player %s leave because play auto", p));
				leaveUser(extension, p.getUser());
			}
			//qua so lan cho phep thi kich ra khoi ban
			else if(((MaubinhPlayer)p).getCountPlay() >= PlayerInfo.MAX_COUNT_AUTO)
			{
				extension.log(String.format("ResetGame: player %s leave because play auto", p));
				leaveUser(extension, p.getUser());
			}
			else {
				p.resetGame();
				extension.log("Reset info " + p);
				extension.getZonehandler().ntfUserBeginPlayingGameNtf(p.getUser(), p.isPlaying());
				i++;
			}
		}

		// extension.notifyChangeRoomVariable(listPlayer);
		listLeaveGame.clear();
	}

	private void leaveUser(GameExtension extension, User user) {
		removeInfo(user);
		extension.getGame().removeFromChickenList(user);

		extension.getApi().leaveRoom(user, extension.getGameRoom(), true, false);
		extension.notifyLeave2Zone(user);

		// Set chip = 0
		UserVariable chipvar = user.getVariable(ServerFieldID.EXT_FIELD_GAME_CHIP);
		if (chipvar != null) {
			UserVariable val = new SFSUserVariable(ServerFieldID.EXT_FIELD_GAME_CHIP, 0);
			ArrayList<UserVariable> listval = new ArrayList<UserVariable>();
			listval.add(val);
			extension.getApi().setUserVariables(user, listval);
		}
	}

	/**
	 * @return the readycount
	 */
	public final int getReadycount() {
		return readycount;
	}

	/**
	 * @return the checkAllReady
	 */
	public final boolean isCheckAllReady() {
		return checkAllReady;
	}

	/**
	 * @return the needBossReady
	 */
	public final boolean isNeedBossReady() {
		return needBossReady;
	}

	/**
	 * @param boss
	 *            the boss to set
	 */
	public final void setBoss(PlayerInfo boss) {
		this.boss = boss;
	}

	public final ArrayList<PlayerInfo> getListQueue() {
		return listQueue;
	}

	public String getPlayerListString() {
		String lst = "";
		for (int i = 0; i < listPlayer.size(); i++)
			lst = lst + listPlayer.get(i).getUser().getName() + ",";

		return lst;
	}

	public ArrayList<PlayerInfo> getListLeaveGame() {
		return listLeaveGame;
	}

	public void setListLeaveGame(ArrayList<PlayerInfo> listLeaveGame) {
		this.listLeaveGame = listLeaveGame;
	}

	public void popBackListLeaveGame() {
		if (this.listLeaveGame.size() > 1)
			this.listLeaveGame.remove(this.listLeaveGame.size() - 1);
	}

}
