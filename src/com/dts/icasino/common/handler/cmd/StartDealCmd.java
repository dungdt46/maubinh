package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.entity.GameRoom;

public class StartDealCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new StartDealCmd();
	}

	@Override
	public void processEvent() {
		return;
	}

	@Override
	public void processRequest() {
		extension.log("Bot request start deal card");
		if (extension.getGame().getStatus() != GameRoom.GAME_TABLE_STATUS_PLAYING)
		{
			extension.log("Game room not start force to start game to deal card");
			extension.getGame().startForBotMode();
			//extension.sendInvalid(user, ServerMsgID.EXT_EVENT_DEAL_CARD_REQ);
		}
		else
			extension.getGame().startDealCard();
	}

}
