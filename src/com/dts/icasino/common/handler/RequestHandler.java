package com.dts.icasino.common.handler;

import java.util.concurrent.TimeoutException;

import com.dts.icasino.common.Config;
import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.encrypt.MessageEncoder;
import com.dts.icasino.common.handler.cmd.BaseCmd;
import com.dts.icasino.common.handler.cmd.CmdFactory;
import com.dts.icasino.common.util.StringUtil;
import com.smartfoxserver.v2.annotations.MultiHandler;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.SFSExtension;

@MultiHandler
public class RequestHandler extends BaseClientRequestHandler{
	
	private CmdFactory factory = CmdFactory.getInstance();
	
	private Config config = Config.getInstance();

	@Override
	public void handleClientRequest(User user, ISFSObject params) {
		GameExtension extension = (GameExtension) getParentExtension();
		String command = params.getUtfString(SFSExtension.MULTIHANDLER_REQUEST_ID);
		
		BaseCmd cmd = factory.getCmd(command);
		
		if (cmd == null)
		{
			extension.log("Can not find cmd of event " + command);
			return;
		}
		
		cmd.setUser(user);
		ISFSObject decryptObj = params;
		if (config.isUseEncrypt())
			decryptObj = MessageEncoder.getInstance().decrypt(user, params);
		
		cmd.setParams(decryptObj);
		//cmd.setExtension(extension);
		try {
			extension.getGamehandler().serviceMessage(GameEventHandler.ICASINO_TYPE_REQUEST, 0, cmd);
		} catch (TimeoutException e) {
			extension.log("Exception " + StringUtil.stackTraceToString(e));
		} catch (InterruptedException e) {
			extension.log("Exception " + StringUtil.stackTraceToString(e));
		}
		
		//extension.updateLastGameAction();
	}

}
