package com.dts.icasino.maubinh.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.handler.cmd.BaseCmd;
import com.dts.icasino.maubinh.MaubinhPlayer;
import com.dts.icasino.maubinh.MaubinhRoom;

public class ListCardLinkCmd extends BaseCmd {
	// xu ly event khi nguoi choi thuc hien 1 lan xep bai
	@Override
	public void processRequest() {
		if (((MaubinhRoom)extension.getGame()).isWaitLinkCard() == false) {
			extension.log("Can not list card link because invalid state");
			return;
		}

		String listcard = params.getUtfString(ServerFieldID.EXT_FIELD_LIST_CARD);
		if (listcard == null) {
			extension.log("List card id is null= ");
			return;
		}
		((MaubinhPlayer) extension.getGame().getPlayerman().getPlayerInfo(user)).setListSetCardFromUser(listcard);
		extension.updateLastGameAction();
//		extension.log("Sortcard = "
//				+ StringUtil.toCardString(
//						((MaubinhPlayer) extension.getGame().getPlayerman().getPlayerInfo(user)).getListsetcard())
//				+ "/user:" + user.getName());
		((MaubinhPlayer) extension.getGame().getPlayerman().getPlayerInfo(user)).setCountPlay(-1);
	}

	@Override
	public void processEvent() {
		extension.log("ListCardLinkCmd");
	}

	@Override
	public BaseCmd createCmd() {
		return new ListCardLinkCmd();
	}

}
