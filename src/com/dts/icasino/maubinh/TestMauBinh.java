package com.dts.icasino.maubinh;

import java.util.ArrayList;

import com.dts.icasino.common.entity.CardInfo;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.util.StringUtil;
import com.dts.icasino.xito.util.MaubinhUtil;
import com.smartfoxserver.bitswarm.sessions.Session;
import com.smartfoxserver.v2.entities.SFSUser;

public class TestMauBinh {
	ArrayList<PlayerInfo> listPlayer = new ArrayList<PlayerInfo>();
	
	public void addPlayer(MaubinhPlayer p){
		listPlayer.add(p);
	}
	
	public static void main(String[] args){
		TestMauBinh test = new TestMauBinh();
		
		MaubinhPlayer p1 = new MaubinhPlayer();
		p1.setUser(new SFSUser("dungdt", new Session()));
		p1.setListSetCardFromUser("41,53,45,49,57,30,26,38,34,22,51,47,55");
		test.addPlayer(p1);
		
		MaubinhPlayer p2 = new MaubinhPlayer();
		p2.setUser(new SFSUser("thanhtt", new Session()));
		p2.setListSetCardFromUser("50,54,42,46,58,31,27,23,39,35,52,56,48");
		test.addPlayer(p2);
		
		MaubinhPlayer p3 = new MaubinhPlayer();
		p3.setUser(new SFSUser("duongnv", new Session()));
		p3.setListSetCardFromUser("20,59,61,17,62,43,37,16,36,15,29,21,63");
		test.addPlayer(p3);
		
		/*MaubinhPlayer p4 = new MaubinhPlayer();
		p4.setUser(new SFSUser("hoangdd", new Session()));
		p4.setListSetCardFromUser("42,41,28,52,51,23,58,26,21,47,39,35,31");
		test.addPlayer(p4);*/
		
		test.victory();
	}
	
	private boolean isInWinList(String username, ArrayList<String> lstwin){
		if (lstwin == null || lstwin.size() == 0)
			return false;
		
		for (int i=0; i<lstwin.size(); i++)
		{
			if (lstwin.get(i).equalsIgnoreCase(username))
				return true;
		}
		
		return false;
	}
	
	private PlayerInfo getPlayer(String username){
		for (int i=0; i<listPlayer.size(); i++)
		{
			if (listPlayer.get(i).getUser().getName().equalsIgnoreCase(username))
				return listPlayer.get(i);
		}
		
		return null;
	}

	protected void victory() {
		System.out.println("GameResult handle");
		// check toi trang truoc
		int maxToiTrangType = 0;
		//int idxUser = -1;
		ArrayList<CardInfo> listToiTrangCard = null;
		ArrayList<String> lstMaxToiTrangUser = new ArrayList<String>();
		// extension.log("List size " + String.valueOf(listPlayer.size()));
		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer player = (MaubinhPlayer) listPlayer.get(i);
			player.calculateToiTrangType();
			int typeToiTrang = player.getTypeToiTrang();
			if (typeToiTrang > 0 && typeToiTrang != PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) {
				if (typeToiTrang > maxToiTrangType) {
					maxToiTrangType = typeToiTrang;
					listToiTrangCard = player.getListsetcard();
					lstMaxToiTrangUser.clear();
					lstMaxToiTrangUser.add(player.getUser().getName());
				} else if (typeToiTrang == maxToiTrangType && listToiTrangCard != null) {
					// neu bang nhau thi tim cai lon hon 3 - la hoa, 1 la hon, 2
					// la kem
					int compareVal = MaubinhUtil.compareSameType(player.getListsetcard(), listToiTrangCard,
							typeToiTrang);
					if (compareVal == 1) {
						maxToiTrangType = typeToiTrang;
						listToiTrangCard = player.getListsetcard();
						//idxUser = i;
						lstMaxToiTrangUser.clear();
						lstMaxToiTrangUser.add(player.getUser().getName());
					} else if (compareVal == 2) {
						// kem hon thi ke thoi, khong thay doi gi ca
					} else if (compareVal == 3) {
						// neu van bang nhau thi uu tien thang dung truoc trong
						// list ?
						lstMaxToiTrangUser.add(player.getUser().getName());
					}
				}
			}
			
			ArrayList<CardInfo> chi1 = player.getListSetCardByLine(1);
			ArrayList<CardInfo> chi2 = player.getListSetCardByLine(2);
			ArrayList<CardInfo> chi3 = player.getListSetCardByLine(3);
			int typeChi1 = MaubinhUtil.getMaxCardType(chi1);
			int typeChi2 = MaubinhUtil.getMaxCardType(chi2);
			int typeChi3 = MaubinhUtil.getMaxCardType(chi3);
			player.setMaxCardTypes(0, typeChi1);
			player.setMaxCardTypes(1, typeChi2);
			player.setMaxCardTypes(2, typeChi3);
			System.out.println("User:" + player.getUser().getName() + "/type chi1= "
					+ MaubinhUtil.getDescriptionToiTrangType(typeChi1) + "/type chi2= "
					+ MaubinhUtil.getDescriptionToiTrangType(typeChi2) + "/type chi3="
					+ MaubinhUtil.getDescriptionToiTrangType(typeChi3));
			
			//extension.log(player.getDump());
		}
		if (maxToiTrangType > 0 && lstMaxToiTrangUser.size() > 0 && maxToiTrangType != PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) {
			if (lstMaxToiTrangUser.size() == listPlayer.size()){
				System.out.println("Hoa me roi, gui ban tin hoa");
			}
			else
			{
				// so sanh voi cac thang con lai trong list
				int tongChiWin = MaubinhUtil.getChiWinByType(maxToiTrangType);
				System.out.println("User win: " + StringUtil.list2String(lstMaxToiTrangUser) + " toi trang type = "
						+ MaubinhUtil.getDescriptionToiTrangType(maxToiTrangType) + "/chi win = " + tongChiWin);

				for (int i = 0; i < listPlayer.size(); i++) {
					MaubinhPlayer player = (MaubinhPlayer)listPlayer.get(i);
					int winAdd = tongChiWin * (-1) * lstMaxToiTrangUser.size();
					if (isInWinList(player.getUser().getName(), lstMaxToiTrangUser)) {
						winAdd = tongChiWin * (listPlayer.size() - lstMaxToiTrangUser.size());
					}
					if (!isInWinList(player.getUser().getName(), lstMaxToiTrangUser)) {
						
						for (int j=0; j<lstMaxToiTrangUser.size(); j++)
						{
							MaubinhPlayer winPlayer = (MaubinhPlayer) getPlayer(lstMaxToiTrangUser.get(j));
							handleBalanceChange(winPlayer, listPlayer.get(i), tongChiWin, -2);
						}
					}

					// reset point sap ho, an ho
					((MaubinhPlayer) listPlayer.get(i)).setPointAnSapHo(0);
					((MaubinhPlayer) listPlayer.get(i)).setPointBiSapHo(0);

					System.out.println("add win tong 3 chi toi trang = " + winAdd + " to user = "
							+ listPlayer.get(i).getUser().getName());
					((MaubinhPlayer) listPlayer.get(i)).setWinTong3Chi(winAdd);
				}
				
				// rieng truong hop toi trang thi tru tien luon, khong so chi nua
				sendResultGame(listPlayer, lstMaxToiTrangUser);

				/*
				 * option toi trang so sanh tiep cac thang con lai trong ban
				 * 
				 * ArrayList<PlayerInfo> listNew = new
				 * ArrayList<PlayerInfo>(listPlayer);
				 * listNew.remove(idxUser);//remove thang toi trang
				 * this.compareResultMatch(listNew);
				 */
			}
			

		} else {
			// check tiep cac case, so sanh tung chi - compare each element
			// together
			compareResultMatch(listPlayer);
		}
	}

	private void compareResultMatch(ArrayList<PlayerInfo> listPlayer) {
		// ham xu ly so sanh cac chi cua cac user voi nhau
		for (int i = 0; i < listPlayer.size(); i++) {
			for (int k = i + 1; k < listPlayer.size(); k++) {
				MaubinhPlayer player1 = (MaubinhPlayer) listPlayer.get(i);
				MaubinhPlayer player2 = (MaubinhPlayer) listPlayer.get(k);
				if (player1.getUser().getName().equalsIgnoreCase(player2.getUser().getName()) == false) {
					// do compare here
					// check binh lung first
					if (MaubinhUtil.isBinhLung(player1.getListsetcard())) {
						if (!MaubinhUtil.isBinhLung(player2.getListsetcard())) {
							System.out.println(player1.getUser().getName() + " bi Binh lung");
							int pointWin1 = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(0));
							int pointWin2 = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(1));
							int pointWin3 = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(2));

							// tru them 6 chi do binh lung
							player1.setWinChi1(player1.getWinChi1() - pointWin1 - 2);
							player1.setWinChi2(player1.getWinChi2() - pointWin2 - 2);
							player1.setWinChi3(player1.getWinChi3() - pointWin3 - 2);

							// so tien duoc cong them do thang binh lung
							// player2.setWinChi1(player2.getWinChi1() +
							// pointWin1);
							// player2.setWinChi2(player2.getWinChi2() +
							// pointWin2);
							// player2.setWinChi3(player2.getWinChi3() +
							// pointWin3);
							int currPoint = player2.getPointBonus();
							player2.setPointBonus(
									currPoint + MaubinhUtil.getChiWinByType(PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) * 3
											+ pointWin1 + pointWin2 + pointWin3);

							player2.setPointBinhLung(player2.getPointBinhLung() + 6);
							player1.setPointBinhLung(player1.getPointBinhLung() - 6);

							// for sort list player
							int tongWin = pointWin1 + pointWin2 + pointWin3 + 6;
							handleBalanceChange(player2, player1, tongWin, -1);
						}

					} else if (MaubinhUtil.isBinhLung(player2.getListsetcard())) {
						if (!MaubinhUtil.isBinhLung(player1.getListsetcard())) {
							System.out.println(player2.getUser().getName() + " bi Binh lung");
							int pointWin1 = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(0));
							int pointWin2 = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(1));
							int pointWin3 = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(2));

							player2.setWinChi1(player2.getWinChi1() - pointWin1 - 2);
							player2.setWinChi2(player2.getWinChi2() - pointWin2 - 2);
							player2.setWinChi3(player2.getWinChi3() - pointWin3 - 2);

							// player1.setWinChi1(player1.getWinChi1() +
							// pointWin1);
							// player1.setWinChi2(player1.getWinChi2() +
							// pointWin2);
							// player1.setWinChi3(player1.getWinChi3() +
							// pointWin3);

							int currPoint = player1.getPointBonus();
							player1.setPointBonus(
									currPoint + MaubinhUtil.getChiWinByType(PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) * 3
											+ pointWin1 + pointWin2 + pointWin3);

							player2.setPointBinhLung(player2.getPointBinhLung() - 6);
							player1.setPointBinhLung(player1.getPointBinhLung() + 6);
							// for sort list player
							int tongWin = pointWin1 + pointWin2 + pointWin3 + 6;
							handleBalanceChange(player1, player2, tongWin, -1);
						}
					} else if (MaubinhUtil.isBinhLung(player1.getListsetcard())
							&& MaubinhUtil.isBinhLung(player2.getListsetcard())) {
						System.out.println(player1.getUser().getName() + "/" + player2.getUser().getName() + " bi Binh lung");
					} else {
						// so sanh binh thuong
						int demP1 = 0;// for check win count for p1 user
						int demP2 = 0;// for check win count for p2 user
						for (int t = 0; t < 3; t++) {
							// so tung chi
							int typeChiP1 = MaubinhUtil.convertMBSpecialToCompare(player1.getMaxCardTypes(t));
							int typeChiP2 = MaubinhUtil.convertMBSpecialToCompare(player2.getMaxCardTypes(t));
							int winChi = 1;
							if (typeChiP1 > typeChiP2) {
								winChi = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(t));
								player1.setWinChiByLine(t, winChi);
								player2.setWinChiByLine(t, -winChi);
								demP1++;
								handleBalanceChange(player1, player2, winChi, t);
							} else if (typeChiP1 < typeChiP2) {
								winChi = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(t));
								player1.setWinChiByLine(t, -winChi);
								player2.setWinChiByLine(t, winChi);
								demP2++;
								handleBalanceChange(player2, player1, winChi, t);
							} else if (typeChiP1 == typeChiP2) {
								// neu cung loai thi so sanh quan to nhat
								ArrayList<CardInfo> listp1 = player1.getListSetCardByLine(t + 1);
								ArrayList<CardInfo> listp2 = player2.getListSetCardByLine(t + 1);
								int resultCompare = MaubinhUtil.compareSameType(listp1, listp2, typeChiP1);
								if (resultCompare == 3) {
									// bang nhau so sanh tiep
									winChi = 0;
									player1.setWinChiByLine(t, winChi);
									player2.setWinChiByLine(t, winChi);
								} else if (resultCompare == 1) {
									winChi = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(t));
									player1.setWinChiByLine(t, winChi);
									player2.setWinChiByLine(t, -winChi);
									demP1++;
									handleBalanceChange(player1, player2, winChi, t);
								} else {
									winChi = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(t));
									player1.setWinChiByLine(t, -winChi);
									player2.setWinChiByLine(t, winChi);
									demP2++;
									handleBalanceChange(player2, player1, winChi, t);
								}
							}
							// truong hop binh thuong se so sanh den dau tru
							// tien den day
							if (t == 2) {
								System.out.println("Point p1 = " + demP1 + "/p2= " + demP2);
								if (demP1 == 3) // p1 an sap
								{
									player1.setPointAnSapHo(player1.getPointAnSapHo() + 1);
									player2.setPointBiSapHo(player2.getPointBiSapHo() + 1);
									handleBalanceChange(player1, player2, 6, t);
								} else if (demP2 == 3) {
									player2.setPointAnSapHo(player2.getPointAnSapHo() + 1);
									player1.setPointBiSapHo(player1.getPointBiSapHo() + 1);
									handleBalanceChange(player2, player1, 6, t);
								}
							}
						}
					}
				}else{
					continue;
				}
			}
		}
		this.sendResultGame(listPlayer, null);
	}

	public void sendResultGame(ArrayList<PlayerInfo> listPlayer, ArrayList<String> lstWin) {
		// send event to user
		String listResult = "";

		// format user|bai an trang| mau binh dac biet | bai chi1| bai chi2|
		// bai
		// chi3 | so chi cong tru chi 1
		// | so chi cong tru chi 2| so chi cong tru chi 3| tong so chi| tong
		// tien
		// them check sap lang
		// boolean isSapLang = false;
		// for (int k = 0; k < listPlayer.size(); k++) {
		// int poinAnSap = ((MaubinhPlayer)
		// listPlayer.get(k)).getPointAnSapHo();
		// if (poinAnSap == listPlayer.size() - 1)
		// isSapLang = true;
		// }

		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer player = (MaubinhPlayer) listPlayer.get(i);
			String userName = player.getUser().getName();
			int baiantrang = 0;

			int typeChi1 = player.getMaxCardTypes(0);
			int typeChi2 = player.getMaxCardTypes(1);
			int typeChi3 = player.getMaxCardTypes(2);
			int moneyBalance = (int) (player.getWinTong3ChiNormal() * 1000);

			int total3Chi = player.getWinTong3ChiNormal();
			int tongchi1 = player.getWinChi1();
			int tongchi2 = player.getWinChi2();
			int tongchi3 = player.getWinChi3();

			int typeSpecial1 = 0;
			int typeSpecial2 = 0;
			int typeSpecial3 = 0;

			int typeNormal1 = 0;
			int typeNormal2 = 0;
			int typeNormal3 = 0;
			int typeBinhLung = 0;
			int pointBinhLung = player.getPointBinhLung();
			if (player.getPointBinhLung() > 0)
				typeBinhLung = 2;
			else if (player.getPointBinhLung() < 0)
				typeBinhLung = 1;

			// check mau binh dac biet
			if (lstWin != null && lstWin.size() > 0) {
				moneyBalance = (int) (player.getWinTong3Chi() * 1000);
				total3Chi = player.getWinTong3Chi();
				typeSpecial1 = 0;
				typeSpecial2 = 0;
				typeSpecial3 = 0;
				typeNormal1 = 0;
				typeNormal2 = 0;
				typeNormal3 = 0;
			}
			if (isInWinList(userName, lstWin)) {
				baiantrang = MaubinhUtil
						.convertCardTypeToClient(((MaubinhPlayer) listPlayer.get(i)).getTypeToiTrang());
			} else {
				// check binh lung first
				if (MaubinhUtil.isBinhLung(player.getListsetcard())) {
					baiantrang = -1;
					typeSpecial1 = -1;
					typeSpecial2 = -1;
					typeSpecial3 = -1;
					typeNormal1 = -1;
					typeNormal2 = -1;
					typeNormal3 = -1;
				} else {
					if (typeChi1 == PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU) {
						typeSpecial1 = MaubinhUtil
								.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU);
						typeNormal1 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH);
					} else if (typeChi1 == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU) {
						typeSpecial1 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU);
						typeNormal1 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY);
					} else {
						typeSpecial1 = 0;
						typeNormal1 = MaubinhUtil.convertCardTypeToClient(typeChi1);
					}

					if (typeChi2 == PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA) {
						typeSpecial2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA);
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_CU_LU);
					} else if (typeChi2 == PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA) {
						typeSpecial2 = MaubinhUtil
								.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA);
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH);
					} else if (typeChi2 == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA) {
						typeSpecial2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA);
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY);
					} else {
						typeSpecial2 = 0;
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(typeChi2);
					}

					if (typeChi3 == PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI) {
						typeSpecial3 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI);
						typeNormal3 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_SAM_CO);
					} else {
						typeSpecial3 = 0;
						typeNormal3 = MaubinhUtil.convertCardTypeToClient(typeChi3);
					}
				}
			}
			// check sap ho va an sap lang
			int type_sapho = 0;
			int type_saplang = 0;
			int so_chi_sapho = 0;
			int so_chi_saplang = 0;
			System.out.println("User:" + player.getUser().getName() + "An sap ho: " + player.getPointAnSapHo()
					+ "/bi sap ho:" + player.getPointBiSapHo());
			int numPlay = listPlayer.size();
			if (player.getPointAnSapHo() == (numPlay - 1) && numPlay > 2) {
				type_saplang = 1;
				so_chi_saplang = player.getPointAnSapHo() * 6;
			} else if (player.getPointAnSapHo() > 0 && numPlay > 2) {
				type_sapho = 1;
				so_chi_sapho = player.getPointAnSapHo() * 6;
			} else if (player.getPointBiSapHo() == (numPlay - 1) && numPlay > 2) {
				type_saplang = 2;
				so_chi_saplang = player.getPointBiSapHo() * 6 * (-1);
			} else if (player.getPointBiSapHo() > 0 && numPlay > 2) {
				type_sapho = 2;
				so_chi_sapho = player.getPointBiSapHo() * 6 * (-1);
			}

			/*
			 * 0: không sập họ, 1: bắt được sập họ 2: bị sập họ 0: không sập
			 * làng 1:bắt sập làng 2: bị bắt sập làng
			 * 
			 */
			listResult += userName + "|" + String.valueOf(baiantrang) + "|" + String.valueOf(typeSpecial1) + ";"
					+ String.valueOf(typeSpecial2) + ";" + String.valueOf(typeSpecial3) + "|"
					+ String.valueOf(typeNormal1) + "|" + String.valueOf(typeNormal2) + "|"
					+ String.valueOf(typeNormal3) + "|" + String.valueOf(tongchi1) + ";" + String.valueOf(tongchi2)
					+ ";" + String.valueOf(tongchi3) + "|" + String.valueOf(total3Chi) + "|"
					+ String.valueOf(moneyBalance) + "|" + String.valueOf(type_sapho) + ";"
					+ String.valueOf(so_chi_sapho) + "|" + String.valueOf(type_saplang) + ";"
					+ String.valueOf(so_chi_saplang) + "|" + String.valueOf(typeBinhLung) + ";"
					+ String.valueOf(pointBinhLung) + "/";
			
			
		}

		System.out.println(listResult);
	}

	public void handleBalanceChange(PlayerInfo playerWin, PlayerInfo playerLoose, int winValue, int chiNum) {
		String chiDes = String.valueOf(chiNum);
		if (chiNum == -1)
			chiDes = "Binh lủng";
		else if (chiNum == -2)
			chiDes = "Tới trắng";
		else if (chiNum == -3) {
			chiDes = "Bỏ cuộc";
		} else
			chiDes = " so chi " + String.valueOf(chiNum);
		System.out.println("handleBalanceChange For Cam, des = " + chiDes) ;
	}

}
