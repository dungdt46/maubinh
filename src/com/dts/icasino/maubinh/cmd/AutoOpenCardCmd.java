package com.dts.icasino.maubinh.cmd;

import com.dts.icasino.common.handler.cmd.BaseCmd;
import com.dts.icasino.maubinh.MaubinhExtension;
import com.dts.icasino.maubinh.MaubinhRoom;

public class AutoOpenCardCmd extends BaseCmd {

	@Override
	public void processRequest() {
		// TODO Auto-generated method stub
	}

	@Override
	public void processEvent() {
		// TODO Auto-generated method stub
		extension.log("Open first card timer is up, default action");
		extension.cancelTimer();
		((MaubinhExtension)extension).setCurrentTimeSort(0);
        ((MaubinhRoom)extension.getGame()).turnupAllCard(true);
	}

	@Override
	public BaseCmd createCmd() {
		// TODO Auto-generated method stub
		return new AutoOpenCardCmd();
	}

}
