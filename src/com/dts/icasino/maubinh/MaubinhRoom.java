package com.dts.icasino.maubinh;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.am.BalanceResp;
import com.dts.icasino.common.am.MatchFinishResult;
import com.dts.icasino.common.entity.CardInfo;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.util.GenerateCard;
import com.dts.icasino.common.util.StringUtil;
import com.dts.icasino.xito.util.MaubinhUtil;
import com.elcom.utils.DateTimeUtil;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;

public class MaubinhRoom extends GameRoom {
	protected double leaveMoney = 0;
	private boolean isWaitLinkCard = false;
	private boolean isDraw = false;

	public MaubinhRoom(GameExtension ext) {
		super(ext);
		genCardLogic = new GenerateCard(3, 16);
	}

	@Override
	public void startDealCard() {
		// bat dau chia bai
		dealCard();
		((MaubinhExtension) extension).setDelay_finish_game(MaubinhExtension.TIME_SO_CHI);
		((MaubinhExtension) extension).setTimeResetGame(MaubinhExtension.TIME_RESET_SO_CHI);
		((MaubinhExtension) extension).startOpenCardTimer();
		isWaitLinkCard = true;
	}

	@Override
	protected void victory() {
		extension.log("GameResult handle");
		isWaitLinkCard = false;
		
		ArrayList<PlayerInfo> listPlayer = extension.getGame().getPlayerman().getListPlayer();
		for (int i = 0; i < listPlayer.size(); i++)
			((MaubinhPlayer) listPlayer.get(i)).calculateToiTrangType();
		
		((MaubinhExtension) extension).sendEndEvent();

		// check toi trang truoc
		int maxToiTrangType = 0;
		//int idxUser = -1;
		ArrayList<CardInfo> listToiTrangCard = null;
		ArrayList<String> lstMaxToiTrangUser = new ArrayList<String>();
		// extension.log("List size " + String.valueOf(listPlayer.size()));
		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer player = (MaubinhPlayer) listPlayer.get(i);
			int typeToiTrang = player.getTypeToiTrang();
			if (typeToiTrang > 0 && typeToiTrang != PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) {
				if (typeToiTrang > maxToiTrangType) {
					maxToiTrangType = typeToiTrang;
					listToiTrangCard = player.getListsetcard();
					lstMaxToiTrangUser.clear();
					lstMaxToiTrangUser.add(player.getUser().getName());
				} else if (typeToiTrang == maxToiTrangType && listToiTrangCard != null) {
					// neu bang nhau thi tim cai lon hon 3 - la hoa, 1 la hon, 2
					// la kem
					int compareVal = MaubinhUtil.compareSameType(player.getListsetcard(), listToiTrangCard,
							typeToiTrang);
					if (compareVal == 1) {
						maxToiTrangType = typeToiTrang;
						listToiTrangCard = player.getListsetcard();
						//idxUser = i;
						lstMaxToiTrangUser.clear();
						lstMaxToiTrangUser.add(player.getUser().getName());
					} else if (compareVal == 2) {
						// kem hon thi ke thoi, khong thay doi gi ca
					} else if (compareVal == 3) {
						// neu van bang nhau thi uu tien thang dung truoc trong
						// list ?
						lstMaxToiTrangUser.add(player.getUser().getName());
					}
				}
			}
			
			ArrayList<CardInfo> chi1 = player.getListSetCardByLine(1);
			ArrayList<CardInfo> chi2 = player.getListSetCardByLine(2);
			ArrayList<CardInfo> chi3 = player.getListSetCardByLine(3);
			int typeChi1 = MaubinhUtil.getMaxCardType(chi1);
			int typeChi2 = MaubinhUtil.getMaxCardType(chi2);
			int typeChi3 = MaubinhUtil.getMaxCardType(chi3);
			player.setMaxCardTypes(0, typeChi1);
			player.setMaxCardTypes(1, typeChi2);
			player.setMaxCardTypes(2, typeChi3);
			extension.log("User:" + player.getUser().getName() + "/type chi1= "
					+ MaubinhUtil.getDescriptionToiTrangType(typeChi1) + "/type chi2= "
					+ MaubinhUtil.getDescriptionToiTrangType(typeChi2) + "/type chi3="
					+ MaubinhUtil.getDescriptionToiTrangType(typeChi3));
			
			//extension.log(player.getDump());
		}
		if (maxToiTrangType > 0 && lstMaxToiTrangUser.size() > 0 && maxToiTrangType != PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) {
			//neu danh sach toi trang to nhat = so nguoi choi
			//hoa ca lang
			if (lstMaxToiTrangUser.size() == listPlayer.size())
			{
				extension.log("All user " + StringUtil.list2String(lstMaxToiTrangUser) + " win toi trang type = " 
						+ MaubinhUtil.getDescriptionToiTrangType(maxToiTrangType) + ", send draw notify");
				sendDrawNotify(listPlayer);
				isDraw = true;
				addDescription("Tất cả người chơi hòa, đều tới trắng " + MaubinhUtil.getDescriptionToiTrangType(maxToiTrangType));
			}
			else
			{
				// so sanh voi cac thang con lai trong list
				int tongChiWin = MaubinhUtil.getChiWinByType(maxToiTrangType);
				extension.log("User win: " + StringUtil.list2String(lstMaxToiTrangUser) + " toi trang type = "
						+ MaubinhUtil.getDescriptionToiTrangType(maxToiTrangType) + "/chi win = " + tongChiWin);

				for (int i = 0; i < listPlayer.size(); i++) {
					MaubinhPlayer player = (MaubinhPlayer)listPlayer.get(i);
					int winAdd = tongChiWin * (-1) * lstMaxToiTrangUser.size();
					if (isInWinList(player.getUser().getName(), lstMaxToiTrangUser)) {
						winAdd = tongChiWin * (listPlayer.size() - lstMaxToiTrangUser.size());
					}
					if (!isInWinList(player.getUser().getName(), lstMaxToiTrangUser)) {
						for (int j=0; j<lstMaxToiTrangUser.size(); j++)
						{
							int winpos = playerman.getPlayerPos(lstMaxToiTrangUser.get(j));
							if (winpos != -1)
							{
								MaubinhPlayer winPlayer = (MaubinhPlayer) playerman.getPlayerbyPos(winpos);
								handleBalanceChange(winPlayer, listPlayer.get(i), tongChiWin, -2);
							}
							else
								extension.log("Victory: can not get pos of toi trang user " + lstMaxToiTrangUser.get(j));
						}
					}

					// reset point sap ho, an ho
					((MaubinhPlayer) listPlayer.get(i)).setPointAnSapHo(0);
					((MaubinhPlayer) listPlayer.get(i)).setPointBiSapHo(0);

					extension.log("add win tong 3 chi toi trang = " + winAdd + " to user = "
							+ listPlayer.get(i).getUser().getName());
					((MaubinhPlayer) listPlayer.get(i)).setWinTong3Chi(winAdd);
				}
				// rieng truong hop toi trang thi tru tien luon, khong so chi nua
				this.sendResultGame(listPlayer, lstMaxToiTrangUser);

				/*
				 * option toi trang so sanh tiep cac thang con lai trong ban
				 * 
				 * ArrayList<PlayerInfo> listNew = new
				 * ArrayList<PlayerInfo>(listPlayer);
				 * listNew.remove(idxUser);//remove thang toi trang
				 * this.compareResultMatch(listNew);
				 */
			}
			
			((MaubinhExtension) extension).setDelay_finish_game(MaubinhExtension.TIME_KO_SO_CHI);
			((MaubinhExtension) extension).setTimeResetGame(MaubinhExtension.TIME_RESET_KO_SO_CHI);

		} else {
			// check tiep cac case, so sanh tung chi - compare each element
			// together
			this.compareResultMatch(listPlayer);
		}
		// ((MaubinhRoom)
		// extension.getGame()).setStatus(GameRoom.GAME_TABLE_STATUS_FINISH);
		((MaubinhExtension) extension).startFinishGameTimer(this, listPlayer);
		extension.victory();
	}

	private void compareResultMatch(ArrayList<PlayerInfo> listPlayer) {
		// ham xu ly so sanh cac chi cua cac user voi nhau
		for (int i = 0; i < listPlayer.size(); i++)
			((MaubinhPlayer)listPlayer.get(i)).checkBinhLung();
		
		int countbinhlung = 0;
		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer player1 = (MaubinhPlayer) listPlayer.get(i);	
			if (player1.isBinhLung())
				countbinhlung++;
			
			for (int k = i + 1; k < listPlayer.size(); k++) {
				MaubinhPlayer player2 = (MaubinhPlayer) listPlayer.get(k);
				//extension.log("player1: " + player1.getDump());
				//extension.log("player2: " + player2.getDump());
				if (player1.getUser().getName().equalsIgnoreCase(player2.getUser().getName()) == false) {
					// do compare here
					// check binh lung first
					if (player1.isBinhLung()) {
						if (!player2.isBinhLung()) {
							
							int pointWin1 = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(0));
							int pointWin2 = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(1));
							int pointWin3 = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(2));
							extension.log(player1.getUser().getName() + " bi Binh lung, PointWin1 " + pointWin1 + ", pointWin2 " + pointWin2 + ", pointWin3 " + pointWin3);
							
							// tru them 6 chi do binh lung
							player1.setWinChi1(player1.getWinChi1() - pointWin1 - 2);
							player1.setWinChi2(player1.getWinChi2() - pointWin2 - 2);
							player1.setWinChi3(player1.getWinChi3() - pointWin3 - 2);

							// so tien duoc cong them do thang binh lung
							// player2.setWinChi1(player2.getWinChi1() +
							// pointWin1);
							// player2.setWinChi2(player2.getWinChi2() +
							// pointWin2);
							// player2.setWinChi3(player2.getWinChi3() +
							// pointWin3);
							int currPoint = player2.getPointBonus();
							player2.setPointBonus(
									currPoint + MaubinhUtil.getChiWinByType(PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) * 3
											+ pointWin1 + pointWin2 + pointWin3);

							player2.setPointBinhLung(player2.getPointBinhLung() + 6);
							player1.setPointBinhLung(player1.getPointBinhLung() - 6);

							// for sort list player
							int tongWin = pointWin1 + pointWin2 + pointWin3 + 6;
							handleBalanceChange(player2, player1, tongWin, -1);
						}

					} else if (player2.isBinhLung()) {
						if (!player1.isBinhLung()) {
							int pointWin1 = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(0));
							int pointWin2 = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(1));
							int pointWin3 = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(2));
							extension.log(player2.getUser().getName() + " bi Binh lung, PointWin1 " + pointWin1 + ", pointWin2 " + pointWin2 + ", pointWin3 " + pointWin3);

							player2.setWinChi1(player2.getWinChi1() - pointWin1 - 2);
							player2.setWinChi2(player2.getWinChi2() - pointWin2 - 2);
							player2.setWinChi3(player2.getWinChi3() - pointWin3 - 2);

							// player1.setWinChi1(player1.getWinChi1() +
							// pointWin1);
							// player1.setWinChi2(player1.getWinChi2() +
							// pointWin2);
							// player1.setWinChi3(player1.getWinChi3() +
							// pointWin3);

							int currPoint = player1.getPointBonus();
							player1.setPointBonus(
									currPoint + MaubinhUtil.getChiWinByType(PlayerInfo.SPECIAL_CARD_TYPE_BINH_LUNG) * 3
											+ pointWin1 + pointWin2 + pointWin3);

							player2.setPointBinhLung(player2.getPointBinhLung() - 6);
							player1.setPointBinhLung(player1.getPointBinhLung() + 6);
							// for sort list player
							int tongWin = pointWin1 + pointWin2 + pointWin3 + 6;
							handleBalanceChange(player1, player2, tongWin, -1);
						}
					} else if (player1.isBinhLung()	&& player2.isBinhLung()) {
						extension.log(player1.getUser().getName() + "/" + player2.getUser().getName() + " bi Binh lung");
					} else {
						// so sanh binh thuong
						int demP1 = 0;// for check win count for p1 user
						int demP2 = 0;// for check win count for p2 user
						for (int t = 0; t < 3; t++) {
							// so tung chi
							int typeChiP1 = MaubinhUtil.convertMBSpecialToCompare(player1.getMaxCardTypes(t));
							int typeChiP2 = MaubinhUtil.convertMBSpecialToCompare(player2.getMaxCardTypes(t));
							int winChi = 1;
							if (typeChiP1 > typeChiP2) {
								winChi = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(t));
								player1.setWinChiByLine(t, winChi);
								player2.setWinChiByLine(t, -winChi);
								demP1++;
								handleBalanceChange(player1, player2, winChi, t);
							} else if (typeChiP1 < typeChiP2) {
								winChi = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(t));
								player1.setWinChiByLine(t, -winChi);
								player2.setWinChiByLine(t, winChi);
								demP2++;
								handleBalanceChange(player2, player1, winChi, t);
							} else if (typeChiP1 == typeChiP2) {
								// neu cung loai thi so sanh quan to nhat
								ArrayList<CardInfo> listp1 = player1.getListSetCardByLine(t + 1);
								ArrayList<CardInfo> listp2 = player2.getListSetCardByLine(t + 1);
								int resultCompare = MaubinhUtil.compareSameType(listp1, listp2, typeChiP1);
								if (resultCompare == 3) {
									// bang nhau so sanh tiep
									winChi = 0;
									player1.setWinChiByLine(t, winChi);
									player2.setWinChiByLine(t, winChi);
								} else if (resultCompare == 1) {
									winChi = MaubinhUtil.getChiWinByType(player1.getMaxCardTypes(t));
									player1.setWinChiByLine(t, winChi);
									player2.setWinChiByLine(t, -winChi);
									demP1++;
									handleBalanceChange(player1, player2, winChi, t);
								} else {
									winChi = MaubinhUtil.getChiWinByType(player2.getMaxCardTypes(t));
									player1.setWinChiByLine(t, -winChi);
									player2.setWinChiByLine(t, winChi);
									demP2++;
									handleBalanceChange(player2, player1, winChi, t);
								}
							}
							
							extension.log("T = " + t + " : Typechi1 " + typeChiP1 + "(" +StringUtil.list2String(player1.getListSetCardByLine(t+1))+ 
									"), typeChi2 " + typeChiP2 + "(" +StringUtil.list2String(player2.getListSetCardByLine(t+1))+ ") , winchi " + winChi);
							// truong hop binh thuong se so sanh den dau tru
							// tien den day
							if (t == 2) {
								extension.log("Point p1 = " + demP1 + "/p2= " + demP2);
								if (demP1 == 3) // p1 an sap
								{
									player1.setPointAnSapHo(player1.getPointAnSapHo() + 1);
									player2.setPointBiSapHo(player2.getPointBiSapHo() + 1);
									handleBalanceChange(player1, player2, 6, t);
								} else if (demP2 == 3) {
									player2.setPointAnSapHo(player2.getPointAnSapHo() + 1);
									player1.setPointBiSapHo(player1.getPointBiSapHo() + 1);
									handleBalanceChange(player2, player1, 6, t);
								}
							}
						}
					}
				}else{
					continue;
				}
			}
		}
		if (listPlayer.size() - countbinhlung <= 1)
		{
			((MaubinhExtension) extension).setDelay_finish_game(MaubinhExtension.TIME_KO_SO_CHI);
			((MaubinhExtension) extension).setTimeResetGame(MaubinhExtension.TIME_RESET_KO_SO_CHI);
		}
		
		this.sendResultGame(listPlayer, null);
	}

	public void sendResultGame(ArrayList<PlayerInfo> listPlayer, ArrayList<String> lstWin) {
		// send event to user
		String listResult = "";

		// format user|bai an trang| mau binh dac biet | bai chi1| bai chi2|
		// bai
		// chi3 | so chi cong tru chi 1
		// | so chi cong tru chi 2| so chi cong tru chi 3| tong so chi| tong
		// tien
		// them check sap lang
		// boolean isSapLang = false;
		// for (int k = 0; k < listPlayer.size(); k++) {
		// int poinAnSap = ((MaubinhPlayer)
		// listPlayer.get(k)).getPointAnSapHo();
		// if (poinAnSap == listPlayer.size() - 1)
		// isSapLang = true;
		// }

		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer player = (MaubinhPlayer) listPlayer.get(i);
			String userName = player.getUser().getName();
			int baiantrang = 0;

			int typeChi1 = player.getMaxCardTypes(0);
			int typeChi2 = player.getMaxCardTypes(1);
			int typeChi3 = player.getMaxCardTypes(2);
			int moneyBalance = (int) (player.getWinTong3ChiNormal() * extension.getGame().getConfig().getBetvalue());

			int total3Chi = player.getWinTong3ChiNormal();
			int tongchi1 = player.getWinChi1();
			int tongchi2 = player.getWinChi2();
			int tongchi3 = player.getWinChi3();

			int typeSpecial1 = 0;
			int typeSpecial2 = 0;
			int typeSpecial3 = 0;

			int typeNormal1 = 0;
			int typeNormal2 = 0;
			int typeNormal3 = 0;
			int typeBinhLung = 0;
			int pointBinhLung = player.getPointBinhLung();
			if (player.getPointBinhLung() > 0)
				typeBinhLung = 2;
			else if (player.getPointBinhLung() < 0)
				typeBinhLung = 1;

			// check mau binh dac biet
			if (lstWin != null && lstWin.size() > 0) {
				moneyBalance = (int) (player.getWinTong3Chi() * extension.getGame().getConfig().getBetvalue());
				total3Chi = player.getWinTong3Chi();
				typeSpecial1 = 0;
				typeSpecial2 = 0;
				typeSpecial3 = 0;
				typeNormal1 = 0;
				typeNormal2 = 0;
				typeNormal3 = 0;
			}
			if (isInWinList(userName, lstWin)) {
				baiantrang = MaubinhUtil
						.convertCardTypeToClient(((MaubinhPlayer) listPlayer.get(i)).getTypeToiTrang());
			} else {
				// check binh lung first
				if (MaubinhUtil.isBinhLung(player.getListsetcard())) {
					baiantrang = -1;
					typeSpecial1 = -1;
					typeSpecial2 = -1;
					typeSpecial3 = -1;
					typeNormal1 = -1;
					typeNormal2 = -1;
					typeNormal3 = -1;
				} else {
					if (typeChi1 == PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU) {
						typeSpecial1 = MaubinhUtil
								.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_DAU);
						typeNormal1 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH);
					} else if (typeChi1 == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU) {
						typeSpecial1 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_DAU);
						typeNormal1 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY);
					} else {
						typeSpecial1 = 0;
						typeNormal1 = MaubinhUtil.convertCardTypeToClient(typeChi1);
					}

					if (typeChi2 == PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA) {
						typeSpecial2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_CU_LU_CHI_GIUA);
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_CU_LU);
					} else if (typeChi2 == PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA) {
						typeSpecial2 = MaubinhUtil
								.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH_CHI_GIUA);
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_THUNG_PHA_SANH);
					} else if (typeChi2 == PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA) {
						typeSpecial2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY_CHI_GIUA);
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_TU_QUY);
					} else {
						typeSpecial2 = 0;
						typeNormal2 = MaubinhUtil.convertCardTypeToClient(typeChi2);
					}

					if (typeChi3 == PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI) {
						typeSpecial3 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_SAM_CHI_CUOI);
						typeNormal3 = MaubinhUtil.convertCardTypeToClient(PlayerInfo.GAME_CARD_TYPE_SAM_CO);
					} else {
						typeSpecial3 = 0;
						typeNormal3 = MaubinhUtil.convertCardTypeToClient(typeChi3);
					}
				}
			}
			// check sap ho va an sap lang
			int type_sapho = 0;
			int type_saplang = 0;
			int so_chi_sapho = 0;
			int so_chi_saplang = 0;
			extension.log("User:" + player.getUser().getName() + "An sap ho: " + player.getPointAnSapHo()
					+ "/bi sap ho:" + player.getPointBiSapHo());
			int numPlay = listPlayer.size();
			if (player.getPointAnSapHo() == (numPlay - 1) && numPlay > 2) {
				type_saplang = 1;
				so_chi_saplang = player.getPointAnSapHo() * 6;
			} else if (player.getPointAnSapHo() > 0 && numPlay >= 2) {
				type_sapho = 1;
				so_chi_sapho = player.getPointAnSapHo() * 6;
			} else if (player.getPointBiSapHo() == (numPlay - 1) && numPlay > 2) {
				type_saplang = 2;
				so_chi_saplang = player.getPointBiSapHo() * 6 * (-1);
			} else if (player.getPointBiSapHo() > 0 && numPlay >= 2) {
				type_sapho = 2;
				so_chi_sapho = player.getPointBiSapHo() * 6 * (-1);
			}

			/*
			 * 0: không sập họ, 1: bắt được sập họ 2: bị sập họ 0: không sập
			 * làng 1:bắt sập làng 2: bị bắt sập làng
			 * 
			 */
			listResult += userName + "|" + String.valueOf(baiantrang) + "|" + String.valueOf(typeSpecial1) + ";"
					+ String.valueOf(typeSpecial2) + ";" + String.valueOf(typeSpecial3) + "|"
					+ String.valueOf(typeNormal1) + "|" + String.valueOf(typeNormal2) + "|"
					+ String.valueOf(typeNormal3) + "|" + String.valueOf(tongchi1) + ";" + String.valueOf(tongchi2)
					+ ";" + String.valueOf(tongchi3) + "|" + String.valueOf(total3Chi) + "|"
					+ String.valueOf(moneyBalance) + "|" + String.valueOf(type_sapho) + ";"
					+ String.valueOf(so_chi_sapho) + "|" + String.valueOf(type_saplang) + ";"
					+ String.valueOf(so_chi_saplang) + "|" + String.valueOf(typeBinhLung) + ";"
					+ String.valueOf(pointBinhLung) + "/";
		}

		ISFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_GAME_RESULT, listResult);
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_GAME_RESULT, ntfObj);
	}
	
	private boolean isInWinList(String username, ArrayList<String> lstwin){
		if (lstwin == null || lstwin.size() == 0)
			return false;
		
		for (int i=0; i<lstwin.size(); i++)
		{
			if (lstwin.get(i).equalsIgnoreCase(username))
				return true;
		}
		
		return false;
	}

	public void handleBalanceChange(PlayerInfo playerWin, PlayerInfo playerLoose, int winValue, int chiNum) {
		String chiDes = String.valueOf(chiNum);
		int reason = PlayerInfo.RES_END_GAME;
		if (chiNum == -1)
			chiDes = "Binh lủng";
		else if (chiNum == -2)
			chiDes = "Tới trắng";
		else if (chiNum == -3) {
			chiDes = "Bỏ cuộc";
			reason = PlayerInfo.RES_BO_CUOC;
		} else
			chiDes = " so chi " + String.valueOf(chiNum);
		extension.log("handleBalanceChange For Cam");
		double realSub = 0;
		double subValue = winValue * extension.getGame().getConfig().getBetvalue();
		realSub = subPlayerBalanceForMB(playerLoose, "", subValue, reason, "Trừ tiền do " + chiDes);
		double addBuff = realSub;
		double realAdd = addPlayerBalanceForMB(playerWin, "", addBuff, reason, "Cong tien do " + chiDes);
		((MaubinhPlayer) playerLoose).setBalanceChange(realSub * (-1));
		((MaubinhPlayer) playerLoose).addWinVal(Math.round(0 - subValue));
		((MaubinhPlayer) playerWin).setBalanceChange(realAdd);
		((MaubinhPlayer) playerWin).addWinVal(Math.round(subValue));

	}

	@Override
	public double finishGame(PlayerInfo player, String relateuser, double value, int reason, String reasondes, int exp,
			int matchtype) {
		double amount = super.finishGame(player, relateuser, value, reason, reasondes, exp, matchtype);
		return amount;
	}

	public void addDescription(String des) {
		description.append(DateTimeUtil.formatDate(Calendar.getInstance().getTime()) + " - " + des);
		description.append("\r\n");
	}

	protected void addLossLeaveGame(PlayerInfo info, int exp) {
		for (int i = 0; i < this.getPlayerman().getListPlayer().size(); i++) {
			handleBalanceChange(this.getPlayerman().getListPlayer().get(i), info, 6, -3);
			// ((MaubinhPlayer) info).setWinChiByLine(1, -2);
			// ((MaubinhPlayer) info).setWinChiByLine(2, -2);
			// ((MaubinhPlayer) info).setWinChiByLine(3, -2);
		}
	}

	@Override
	public void leaveGame(PlayerInfo info) {
		int pos = playerman.getPlayerPos(info.getUser().getId());
		extension.log(String.format("LeaveGame: Player %s(%d) leave game in pos %d", info.getUser().getName(),
				info.getUser().getId(), pos));
		info.setIspass(true);
		playerman.leaveGame(info);
		setPlayerListVar();
		extension.getApi().leaveRoom(info.getUser(), extension.getGameRoom(), true, false);
		extension.notifyLeave2Zone(info.getUser());
		extension.notifyChangeRoomVariable(extension.getGame().getPlayerman().getListPlayer());
		this.sendLeaveNotify(info);

		// dem xem con bao nhieu thang chua up bo bai, neu = 1 thi ket thuc luon
		int countpplaying = 0;
		ArrayList<PlayerInfo> listPlayer = extension.getGame().getPlayerman().getListPlayer();
		for (int i = 0; i < listPlayer.size(); i++) {
			PlayerInfo p = listPlayer.get(i);
			if (p != null && p.isIspass() == false) {
				countpplaying++;
			}
		}
		// rieng mau binh fix cung 6 chi * muc cuoc * so nguoi trong ban
		double leaveval = subPlayerBalance(info, "", 6 * config.getBetvalue() * countpplaying, 14, "Xin ra");
		for (int i = 0; i < listPlayer.size(); i++) {
			double leaveBonus = leaveval/listPlayer.size();
			double realval = addPlayerBalance(listPlayer.get(i), info.getUser().getName(), leaveBonus,
					PlayerInfo.RES_BO_CUOC, "Cộng tiền do có người bỏ cuộc");
			((MaubinhPlayer) listPlayer.get(i)).setBalanceChange(realval);
		}
		addLoss(info.getUser(), 4);

		if (countpplaying > 1) {
			if (current_play == pos) {
				current_play--;
			} else if (current_play > pos)
				current_play--;
		} else {
			// set time to half
			((MaubinhExtension) extension).setDelay_finish_game(MaubinhExtension.TIME_KO_SO_CHI);
			((MaubinhExtension) extension).setTimeResetGame(MaubinhExtension.TIME_RESET_KO_SO_CHI);
			victory();
		}

	}

	public void sendLeaveNotify(PlayerInfo p) {
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, p.getUser().getName());
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, toPlayerString());
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_USER_LEAVE_NOTIF, ntfObj);
	}

	@Override
	public void defaultPlay() {
		// TODO Auto-generated method stub
		extension.log("defaultPlay");
		PlayerInfo info = playerman.getPlayerbyPos(current_play);
		if (info == null) {
			extension.log(String.format("DefaultBet: " + "Can not find current raise user info in position %d",
					current_play));
			return;
		}

		info.incCountAuto();
		// hanh dong auto cho user
	}

	@Override
	public String toString(String name) {
		// int uid = -1;
		// if (current_play > -1) {
		// User userRejoin = playerman.getPlayerbyPos(current_play).getUser();
		// if (userRejoin != null)
		// uid = userRejoin.getId();
		// }

		ArrayList<PlayerInfo> listPlayer = extension.getGame().getPlayerman().getListPlayer();
		String lstPlayers = "";
		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer p = (MaubinhPlayer) listPlayer.get(i);
			if (p.getUser().getName().equalsIgnoreCase(name))
				lstPlayers = lstPlayers + p.toString(true) + ";";
			else
				lstPlayers = lstPlayers + p.toString(false) + ";";
		}

		// return String.format("%d|%s", uid, lstPlayers);
		return lstPlayers;
	}

	@Override
	public PlayerInfo createNewPlayer() {
		// TODO Auto-generated method stub
		return new MaubinhPlayer();
	}

	@Override
	public boolean checkStart() {
		// TODO Auto-generated method stub
		// ham check truoc khi van moi bat dau
		ArrayList<User> lstWillRemove = new ArrayList<User>();
		ArrayList<PlayerInfo> listPlayer = extension.getGame().getPlayerman().getListPlayer();
		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer p = (MaubinhPlayer) listPlayer.get(i);
			if (p.getBalance() < config.getMinbalance())
				lstWillRemove.add(p.getUser());
		}
		if (lstWillRemove.size() > 0) {
			for (int i = 0; i < lstWillRemove.size(); i++) {
				User u = lstWillRemove.get(i);
				extension.log(String.format("ResetGame: player %s leave because not enough money", u.getName()));
				playerman.removeInfo(u);
				extension.getGame().removeFromChickenList(u);
				extension.getApi().leaveRoom(u, extension.getParentRoom());
				setPlayerListVar();
			}
			sendListUserUpdate();
		}
		if (playerman.countPlayer() <= 1)
			return false;
		else
			return true;

	}

	@Override
	public void sendListUserUpdate() {
		// gui event listuser update cho client
		SFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_USER, toPlayerString());
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_LIST_USER_UPDATE, ntfObj);
	}

	public String toPlayerString() {
		ArrayList<PlayerInfo> listPlayer = playerman.getListPlayer();
		ArrayList<PlayerInfo> listplaying = new ArrayList<PlayerInfo>();
		for (int i = 0; i < listPlayer.size(); i++) {
			PlayerInfo p = listPlayer.get(i);
			if (p.isPlayer()) {
				listplaying.add(p);
			}
		}

		return StringUtil.list2String(listplaying, "-");
	}

	// ham chia bai
	private void dealCard() {
		extension.log("Deal card for all player");
		ArrayList<PlayerInfo> listPlayer = extension.getGame().getPlayerman().getListPlayer();
		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer p = (MaubinhPlayer) listPlayer.get(i);
			if (p.isIspass() == false) {
				// moi nguoi choi se duoc 13 quan -- add card vao nguoi choi
				String listautoCard = extension.getGame().getListautocard(p.getUser().getName());
				ArrayList<CardInfo> listcard = genCardLogic.generateCard(13, true, listautoCard);
				ArrayList<CardInfo> newcard = MaubinhUtil.AutoSortCardPro(listcard);
				p.addCard(newcard);
				extension.log(String.format("DealCard: Deal player %s(%d) card %s", p.getUser().getName(),
						p.getUser().getId(), StringUtil.toCardString(newcard)));
				p.setListsetcard(newcard);
				addDescription("Chia bài " + StringUtil.toCardDescription(newcard) + " cho " + p.getUser().getName());
			}
			
			//extension.log("Dealcard to - " + p.getDump());
		}
		
		List<User> lstuser = extension.getParentRoom().getUserList();
		for (int i = 0; i < lstuser.size(); i++)
			sendDealCard(lstuser.get(i));
	}

	private void sendDealCard(User user) {
		// send event to user
		ISFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_LIST_CARD, toCardString(user.getName()));
		ntfObj.putInt(ServerFieldID.EXT_FIELD_DURATION, ((MaubinhExtension) extension).getOpen_auto_card_time());
		extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_DEAL_CARD_NOTIF, ntfObj, user);
	}

	public String toCardString(String username) {
		// ham chuyen card to string, chia bai cho tung user - cho nguoi choi va
		// khach
		ArrayList<PlayerInfo> listPlayer = extension.getGame().getPlayerman().getListPlayer();
		String strListCard = "";
		for (int i = 0; i < listPlayer.size(); i++) {
			MaubinhPlayer p = (MaubinhPlayer) listPlayer.get(i);
			if (p.isPlayer() && p.isIspass() == false) {
				if (p.getUser().getName().equalsIgnoreCase(username))
					strListCard = strListCard + p.getUser().getName() + "|" + p.toStateCardString(true) + ";";
				else
					strListCard = strListCard + p.getUser().getName() + "|" + p.toStateCardString(false) + ";";
			}
		}
		return strListCard;
	}

	public void turnupAllCard(boolean isTimeout) {
		// ham xu ly lat tat ca cac quan bai user len de so sanh - new timeout
		// thi khong can kiem tra so nguoi da xep xong
		if (isTimeout) {
			this.victory();
		} else {
			// kiem tra neu sap xep xong het thi cancel timer auto va tinh tien
			ArrayList<PlayerInfo> listPlayer = extension.getGame().getPlayerman().getListPlayer();
			int dem = 0;
			for (int i = 0; i < listPlayer.size(); i++) {
				if (((MaubinhPlayer) listPlayer.get(i)).isSortDone())
					dem++;
			}
			if (dem == listPlayer.size())// da sort xong het
			{
				extension.cancelTimer();
				this.victory();
			} 
		}
	}

	/// gui ban tin khi join lai ban
	public void sendGameChangeNotify(User user) {

		boolean isPlaying = extension.isPlaying();
		long currentTime = System.currentTimeMillis();
		extension.log("sendGameChangeNotify" + "/current Time = " + currentTime);

		int deltaTime = ((MaubinhExtension) extension).getOpen_auto_card_time()
				- (int) (currentTime - ((MaubinhExtension) extension).getCurrentTimeSort());
		if (deltaTime < 1000 || deltaTime > ((MaubinhExtension) extension).getOpen_auto_card_time())
			deltaTime = 0;
		SFSObject gameObj = new SFSObject();
		gameObj.putInt(ServerFieldID.EXT_FIELD_GAME_STATUS, isPlaying == true ? 1 : 0);
		gameObj.putInt(ServerFieldID.EXT_FIELD_TIME, deltaTime);
		if (this.getPlayerman().isLeavingGame(user) == true)
			gameObj.putNull(ServerFieldID.EXT_FIELD_GAME_INFO);
		else
			gameObj.putUtfString(ServerFieldID.EXT_FIELD_GAME_INFO, toString(user.getName()));
		extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_GAME_CHANGE_NOTIF, gameObj, user);
	}

	public void sendNotifySort(int isDone, String uid) {
		// ham notify all la co nguoi nao do da xep bai xong hay chua
		// send event to user
		ISFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, uid);
		ntfObj.putInt(ServerFieldID.EXT_FIELD_CARD_SORT, isDone);
		// notify all dung send game event
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_FINISH_CARD_LINK_NOTIFY, ntfObj);
	}

	public void sendVictoryNotify(String listparam, String listwin) {
		ISFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_GAME_INFO, listparam);
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_WIN_INFO, listwin);
		// notify all dung send game event
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_VICTORY_NOTIF, ntfObj);
	}

	public static void main(String[] args) {
		MaubinhRoom room = new MaubinhRoom(null);
		ArrayList<PlayerInfo> listPlayer = room.getPlayerman().getListPlayer();

		String listcard = "22,30,25,61,41,36,37,13,63,33,52,62,42";
		MaubinhPlayer p1 = new MaubinhPlayer();
		p1.setListSetCardFromUser(listcard);
		listPlayer.add(p1);

		listcard = "19,40,30,59,50,18,20,27,31,44,21,56,58";
		MaubinhPlayer p2 = new MaubinhPlayer();
		p2.setListSetCardFromUser(listcard);
		listPlayer.add(p2);

		double number = -36000000;
		System.out.println(String.format("%.0f", number));
	}

	@Override
	public void onResetGame() {
		this.leaveMoney = 0;
		this.isDraw = false;
	}

	private void sendDrawNotify(ArrayList<PlayerInfo> lstPlayer){
		String lststr = "";
		for (int i=0; i<lstPlayer.size(); i++){
			MaubinhPlayer player = (MaubinhPlayer)lstPlayer.get(i);
			lststr = lststr + player.getUser().getName() + ";" + MaubinhUtil.convertCardTypeToClient(player.getTypeToiTrang()) + ";" + StringUtil.list2String(player.getListsetcard(), ",") + "|";
		}
		
		ISFSObject ntfObj = new SFSObject();
		ntfObj.putUtfString(ServerFieldID.EXT_FIELD_GAME_RESULT, lststr);
		extension.sendGameEvent(ServerMsgID.EXT_EVENT_DRAW_NTF, ntfObj);
	}

	protected double subPlayerBalanceForMB(PlayerInfo player, String relateuser, double value, int reason,
			String reasondes) {
		double subvalue = player.getBalance() > value ? value  : player.getBalance();
		BalanceResp res = extension.getAccman().subBalance(extension.getPartnerid(), extension.getGameid(), matchid, config.getBetvalue(), extension.getGameRoom().getName(),
				player.getUser(), relateuser, subvalue, reasondes, moneytype);
		
		long realval = getRealChangeBalance(res);
		extension.log( String.format("subPlayerBalanceForMB: Player %s(%d) sub balance %d, reason %s result %d", 
    			player.getUser().getName(), player.getUser().getId(), realval, reasondes, res.getErrCode()));
		
		
		if (res.getErrCode() == 0)
		{
			addDescription(String.format("Trừ người chơi %s số tiền %d với lý do %s", player.getUser().getName(), realval, reasondes));
			
			if (reason == PlayerInfo.RES_BO_CUOC) {
				player.setBalance(getLastBalance(res));
				setPlayerBalance(player.getUser(), res.getLastBalance(), res.getLastSilver(), res.getLastExp(), res.getLastLevel());
			}
			//player.setBalance(getLastBalance(res));
			//player.addCurrentbet(realval);
			//setPlayerBalance(player.getUser(), res.getLastBalance(), res.getLastSilver(), res.getLastExp(), res.getLastLevel());
			
//			if (realval != 0)
//				sendChangeBalanceNotif(player, 0-realval, reason);        
	        
			return realval;
		}
		
		return 0;
	}

	protected double addPlayerBalanceForMB(PlayerInfo player, String relateuser, double value, int reason,
			String reasondes) {
		BalanceResp res = extension.getAccman().addBalance(extension.getPartnerid(), extension.getGameid(), matchid, config.getBetvalue(), extension.getGameRoom().getName(), 
				player.getUser(), relateuser, value, reasondes, moneytype);
		
		long realval = getRealChangeBalance(res);
		extension.log( String.format("addPlayerBalanceForMB: Player %s(%d) add balance %d, reason %s result %d", 
    			player.getUser().getName(), player.getUser().getId(), realval, reasondes, res.getErrCode()));
		
		if (res.getErrCode() == 0)
		{
			addDescription(String.format("Cộng người chơi %s số tiền %d với lý do %s", player.getUser().getName(), realval, reasondes));
			
			if (reason == PlayerInfo.RES_BO_CUOC) {
				player.setBalance(getLastBalance(res));
				setPlayerBalance(player.getUser(), res.getLastBalance(), res.getLastSilver(), res.getLastExp(), res.getLastLevel());
			}
			//player.setBalance(getLastBalance(res));
			//player.addCurrentbet(res.getAmount());
			//setPlayerBalance(player.getUser(), res.getLastBalance(), res.getLastSilver(), res.getLastExp(), res.getLastLevel());
			
			//sendChangeBalanceNotif(player, realval, reason);
			return realval;
		}
		
		return 0;
	}

	public void setUserInfoAfterGame(User user, MatchFinishResult res, PlayerInfo player) {
		extension.log(String.format("Update %s(%d) balance = %d, award = %d, level = %d, exp = %d", user.getName(),
				user.getId(), res.getLastGold(), res.getAwardGold(), res.getLastLevel(), res.getLastExp()));
		sendChangeBalanceNotif(player, ((MaubinhPlayer) player).getBalanceChange(), 0);
		UserVariable moneyval = new SFSUserVariable(ServerFieldID.EXT_VAL_AMOUNT_BALANCE,
				res.getLastGold() + res.getRealGold());
		UserVariable awardval = new SFSUserVariable(ServerFieldID.EXT_VAL_AWARD_MONEY, res.getAwardGold());
		UserVariable expval = new SFSUserVariable(ServerFieldID.EXT_VAL_EXP, res.getLastExp());
		UserVariable levelval = new SFSUserVariable(ServerFieldID.EXT_VAL_LEVEL, res.getLastLevel());

		ArrayList<UserVariable> listval = new ArrayList<UserVariable>();
		listval.add(moneyval);
		listval.add(awardval);
		listval.add(expval);
		listval.add(levelval);
		extension.getApi().setUserVariables(user, listval);
	}

	public void setStatus(int _status) {
		this.status = _status;
	}

	public final boolean isWaitLinkCard() {
		return isWaitLinkCard;
	}

	public final boolean isDraw() {
		return isDraw;
	}
}
