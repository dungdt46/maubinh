package com.dts.icasino.common;

public class GameResultCode {
	public static final int GAME_SUCCESS = 0;
	
	public static final int ACTION_NOT_ALLOW = 5;
	
	public static final int RESCODE_NOT_ENOUGH_MONEY = 29;
	
	public static final int GAME_IS_PLAYING = 10;

}
