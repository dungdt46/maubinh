package com.dts.icasino.common.handler;

import com.dts.icasino.common.GameExtension;
import com.dts.icasino.common.handler.cmd.BaseCmd;
import com.dts.icasino.common.util.StringUtil;
import com.elcom.utils.thread.ThreadWithQueue;

public class GameEventHandler extends ThreadWithQueue{
	
	public static final int ICASINO_TYPE_EVENT = 1;
	public static final int ICASINO_TYPE_REQUEST = 2;
	
	private GameExtension ext;

	public GameEventHandler(GameExtension ext) {
		super();
		this.ext = ext;
	}

	@Override
	protected boolean processMessage(int event, int subevent, Object data) {
		try{
			BaseCmd cmd = (BaseCmd)data;
			cmd.setExtension(ext);
			
			switch (event) {
			case ICASINO_TYPE_EVENT:
				cmd.processEvent();
				break;
			case ICASINO_TYPE_REQUEST:
				cmd.processRequest();
				break;
			default:
				logger.error("Type event is invalid");
				break;
			}
		
		}
		catch (Throwable e) {
			ext.log("Exception " + StringUtil.stackTraceToString(e));
		}
		
		return true;
	}

	
}
