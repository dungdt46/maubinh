package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;

public class ChargeCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new ChargeCmd();
	}

	@Override
	public void processEvent() {
		
	}

	@Override
	public void processRequest() {
		double amount = params.getDouble(ServerFieldID.RECHARGE_RESULT);
		extension.log( String.format("PlayerCharge: Player %s(%d) charge %f", user.getName(), user.getId(), amount));
		GameRoom room = extension.getGame();
		
		PlayerInfo info = room.getPlayerman().getInfo(user);
		info.addBalance(amount);
	}

}
