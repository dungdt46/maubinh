package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.GameExtension;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public abstract class BaseCmd {
	
	protected ISFSEvent event;
	
	protected ISFSObject params;
	
	protected User user;
	
	protected GameExtension extension;
	
	public abstract void processRequest();
	
	public abstract void processEvent();
	
	public abstract BaseCmd createCmd();

	/**
	 * @param event the event to set
	 */
	public final void setEvent(ISFSEvent event) {
		this.event = event;
	}

	/**
	 * @param params the params to set
	 */
	public final void setParams(ISFSObject params) {
		this.params = params;
	}

	/**
	 * @param user the user to set
	 */
	public final void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the extension
	 */
	public final GameExtension getExtension() {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public final void setExtension(GameExtension extension) {
		this.extension = extension;
	}
}
