package com.dts.icasino.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.dts.icasino.common.am.AMImpl;
import com.dts.icasino.common.am.IAccMan;
import com.dts.icasino.common.encrypt.MessageEncoder;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.entity.TableConfig;
import com.dts.icasino.common.handler.EventHandler;
import com.dts.icasino.common.handler.GameEventHandler;
import com.dts.icasino.common.handler.RequestHandler;
import com.dts.icasino.common.handler.ZoneEventHandler;
import com.dts.icasino.common.handler.cmd.CheckReadyCmd;
import com.dts.icasino.common.handler.cmd.DefaultPlayCmd;
import com.dts.icasino.common.handler.cmd.DelayVictoryCmd;
import com.dts.icasino.common.handler.cmd.ResetCmd;
import com.dts.icasino.common.util.StringUtil;
import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.ExtensionReloadMode;
import com.smartfoxserver.v2.extensions.SFSExtension;

public abstract class GameExtension extends SFSExtension {

	protected int gameid;

	protected String gamename;

	protected String partnerid = "";

	private volatile boolean gameStarted;

	private boolean isPlaying = false;

	// van dau tien khi khoi tao ban, boss dc quyen chia bai
	private boolean isFirst = true;

	private long startWaitReady;

	protected GameRoom game;

	protected Config config = Config.getInstance();

	protected ZoneEventHandler zonehandler = new ZoneEventHandler(this);

	protected GameEventHandler gamehandler = new GameEventHandler(this);

	protected Timer timer;

	protected SmartFoxServer sfs = SmartFoxServer.getInstance();

	protected int timeResetGame = 22 * 1000;

	private IAccMan accman;
	
	

	@Override
	public void init() {

		addRequestHandler(ServerMsgID.EXT_EVENT_READY_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_UNREADY_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_KICK_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_SET_BOT_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_START_DEAL_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_SET_CARD_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.NOTIFY_RECEIVED_MONEY_FROM_FRIEND, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_SET_BOT_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_SET_CARD_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_GAME_LEAVE_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_GAME_CHIKKEN_CONFIG_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_QUEUE_GAME_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_LEAVE_GAME_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_GET_GAME_INFO_REQ, RequestHandler.class);
		addRequestHandler(ServerMsgID.EXT_EVENT_USE_CHICKEN_REQ, RequestHandler.class);

		addEventHandler(SFSEventType.USER_DISCONNECT, EventHandler.class);
		addEventHandler(SFSEventType.USER_LEAVE_ROOM, EventHandler.class);
		addEventHandler(SFSEventType.USER_JOIN_ROOM, EventHandler.class);
		addEventHandler(SFSEventType.SPECTATOR_TO_PLAYER, EventHandler.class);
		addEventHandler(SFSEventType.PLAYER_TO_SPECTATOR, EventHandler.class);
		onInit();

		parseConfig();
		setChickenStatus();
		setRoomVaribale();
		updateLastGameAction();
		accman = AMImpl.getInstance();
		gamehandler.start();
		startWaitReadyTimer();
		trace("Game Extension - [ roomid:" + getGameRoom().getId() + " ] init");
	}

	protected void parseConfig() {
		int minbet = 1000;
		int feepercent = 0;
		boolean haveChicken = false;
		int minlevel = minbet;
		int moneytype = -1;

		RoomVariable partnerparams = getParentRoom().getVariable("accParId");
		if (partnerparams != null)
			partnerid = partnerparams.getStringValue().trim();
		else
			log("Partner null");
		
		RoomVariable roomtypeparams= getParentRoom().getVariable("roomType");
        if (roomtypeparams != null)
        	moneytype = roomtypeparams.getIntValue();
        else
        	log("Roomtype null");

		log("Room of partner " + partnerid);
		RoomVariable params = getParentRoom().getVariable("params");
		if (params != null) {
			String[] listp = params.getStringValue().split("@");
			log(String.format("Param is %s, split into %s", params.getStringValue(), Arrays.toString(listp)));

			minbet = Integer.parseInt(listp[0].trim());

			if (listp.length >= 4)
				haveChicken = Integer.parseInt(listp[3].trim()) == 1;

			if (listp.length >= 5)
				minlevel = Integer.parseInt(listp[4].trim());
		}

		TableConfig config = new TableConfig(minbet, feepercent, minlevel, haveChicken);
		game.setConfig(config);
		game.setMoneytype(moneytype);
	}

	public abstract void onInit();

	public abstract void onDestroy();

	@Override
	public void destroy() {
		super.destroy();
		log("Game - [ roomid:" + getGameRoom().getId() + " ] - destroyed!");
		onDestroy();
		cancelTimer();
		gamehandler.stop(true);
	}

	public boolean isGameStarted() {
		return gameStarted;
	}

	class RemindTask extends TimerTask {

		public void run() {
			log("Time's up! Call default action");
			DefaultPlayCmd cmd = new DefaultPlayCmd();

			try {
				gamehandler.serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				log("Exception : " + StringUtil.stackTraceToString(e));
			}
		}
	}

	class ReadyTask extends TimerTask {

		@Override
		public void run() {
			CheckReadyCmd cmd = new CheckReadyCmd();

			try {
				gamehandler.serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				log("Exception : " + StringUtil.stackTraceToString(e));
			}
		}

	}

	class DelayVictoryTask extends TimerTask {

		@Override
		public void run() {
			DelayVictoryCmd cmd = new DelayVictoryCmd();

			try {
				gamehandler.serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				log("Exception : " + StringUtil.stackTraceToString(e));
			}
		}

	}

	class ResetTask extends TimerTask {

		@Override
		public void run() {
			ResetCmd cmd = new ResetCmd();

			try {
				gamehandler.serviceMessage(GameEventHandler.ICASINO_TYPE_EVENT, 0, cmd);
			} catch (Exception e) {
				log("Exception : " + StringUtil.stackTraceToString(e));
			}
		}

	}

	public Room getGameRoom() {
		return this.getParentRoom();
	}

	public void victory() {
		log("End of game " + gamename);
		game.description2File();
		// sendEndEvent();
		this.gameStarted = false;
		this.isPlaying = false;
		this.isFirst = false;

		// game.resetGame();
		// cancelTimer();
		// if (this.getGame().getBotMode() == true) {
		// log("Set botmode true - wait deal command");
		// } else {
		startWaitResetGame();
		// }

	}

	public void startGame() {
		log("Start game");

		if (gameStarted) {
			throw new IllegalStateException("Game is already started!");
		}

		if (game.checkStart() == false) {
			log("Check start fail, continue wait ready");
			return;
		}

		cancelTimer();
		gameStarted = true;
		isPlaying = true;
		sendStartEvent();

		game.start();
		notifyChangeRoomVariable(game.getPlayerman().getListPlayer());
	}

	public void sendStartEvent() {
		sendGameEvent(ServerMsgID.EXT_EVENT_START_GAME_NOTIF);
	}

	public void sendEndEvent() {
		sendGameEvent(ServerMsgID.EXT_EVENT_END_GAME_NOTIF);
	}

	public void startPlayTimer(long duration) {
		timer = new Timer(getTimerName());
		timer.schedule(new RemindTask(), duration);
		log(String.format("Start play timer, duration = %d ", duration));
	}

	public void startDelayVictory(long duration) {
		this.isPlaying = false;
		timer = new Timer(getTimerName());
		timer.schedule(new DelayVictoryTask(), duration);
		log(String.format("Start delay victory timer, duration = %d ", duration));
	}

	public void cancelTimer() {
		log("Cancel timer");

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	

	public void startWaitReadyTimer() {
		startWaitReady = System.currentTimeMillis();
		timer = new Timer(getTimerName());
		timer.schedule(new ReadyTask(), 0, 1000);
		log(String.format("Start wait ready timer"));
	}

	public void startWaitResetGame() {
		startWaitReady = System.currentTimeMillis();
		timer = new Timer(getTimerName());
		timer.schedule(new ResetTask(), this.timeResetGame);
		log(String.format("Start wait reset timer"));
	}

	public int getTimeResetGame() {
		return timeResetGame;
	}

	public void setTimeResetGame(int timeResetGame) {
		this.timeResetGame = timeResetGame;
	}

	public void sendGameEvent(String msgid, ISFSObject event) {
		log(" sendGameEvent ( " + msgid + " )- [ roomid:" + getGameRoom().getId() + ", event " + event.toJson() + " ]");
		send(msgid, event, getParentRoom().getUserList());
	}

	public void sendGameEvent2User(String msgid, ISFSObject event, User user) {
		log(" sendGameEvent ( " + msgid + " )- [ roomid:" + getGameRoom().getId() + ", event " + event.toJson()
				+ " ] to user " + user.getName());
		send(msgid, event, user);
	}

	public void sendGameEvent(String msgid) {
		log(" sendGameEvent ( " + msgid + " )- [ roomid:" + getGameRoom().getId() + " ]");
		send(msgid, null, getParentRoom().getUserList());
	}

	public void sendGameEvent2User(String msgid, User user) {
		log(" sendGameEvent ( " + msgid + " )- [ roomid:" + getGameRoom().getId() + " ] to user " + user.getName());
		send(msgid, null, user);
	}

	public void sendGameEvent2Other(String msgid, ISFSObject event, int uid) {
		log(" sendGameEvent2Other ( " + msgid + " )- [ roomid:" + getGameRoom().getId() + ", event " + event.toJson()
				+ " ]");
		java.util.List<User> listuser = getParentRoom().getUserList();

		for (int i = 0; i < listuser.size(); i++) {
			if (listuser.get(i).getId() != uid)
				send(msgid, event, listuser.get(i));
		}
	}

	public void sendGameEvent2Other(String msgid, int uid) {
		log(" sendGameEvent2Other ( " + msgid + " )- [ roomid:" + getGameRoom().getId() + " ]");
		java.util.List<User> listuser = getParentRoom().getUserList();

		for (int i = 0; i < listuser.size(); i++) {
			if (listuser.get(i).getId() != uid)
				send(msgid, null, listuser.get(i));
		}
	}

	public void sendInvalid(User user, String msgid) {
		log(String.format("User %s(%d) send invalid action", user.getName(), user.getId()));
		SFSObject resObj = new SFSObject();
		resObj.putInt(ServerFieldID.EXT_FIELD_RESULT, GameResultCode.ACTION_NOT_ALLOW);
		resObj.putInt(ServerFieldID.EXT_FIELD_OLD_RESULT, GameResultCode.ACTION_NOT_ALLOW);

		sendGameEvent2User(msgid, resObj, user);
	}

	public void log(String log) {
		trace(gamename + getGameRoom().getName() + "(" + getGameRoom().getId() + ") - " + log);
	}

	public final GameRoom getGame() {
		return game;
	}

	public final IAccMan getAccman() {
		return accman;
	}

	public void notifyChangeRoomVariable(ArrayList<PlayerInfo> listplayers) {
		// Notify to Room Variable
		RoomVariable val = getParentRoom().getVariable("params");
		// Split: A@B@...
		String[] lstParams = val.getStringValue().split("@");
		// Join param: money@isPlaying@numOfPlayer@...

		int numOfPlayer = 0;
		for (int i = 0; i < listplayers.size(); i++) {
			if (listplayers.get(i).isPlayer() == true && !listplayers.get(i).isLeave())
				numOfPlayer++;
		}

		String sParam = lstParams[0] + "@" + (gameStarted ? 1 : 0) + "@" + numOfPlayer;
		for (int i = 3; i < lstParams.length; i++) {
			sParam += "@" + lstParams[i];
		}
		this.log("Param RoomVarible: " + sParam);
		//
		RoomVariable valNew = new SFSRoomVariable("params", sParam);
		List<RoomVariable> lst = new ArrayList<RoomVariable>();
		lst.add(valNew);
		getApi().setRoomVariables(null, getParentRoom(), lst);
	}

	public void notifyLeave2Zone(User user) {
		getParentZone().getExtension().handleInternalMessage(ServerMsgID.EXT_EVENT_USER_LEAVE_NOTIF,
				String.format("%s,%d,%d", user.getName(), getParentZone().getId(), getParentRoom().getId()));
	}

	public void updateLastGameAction() {
		Room room = getGameRoom();
		RoomVariable lastactionvar = new SFSRoomVariable("lastaction", System.currentTimeMillis());
		lastactionvar.setHidden(true);
		lastactionvar.setGlobal(true);

		try {
			room.setVariable(lastactionvar);
		} catch (SFSVariableException e) {
			log(StringUtil.stackTraceToString(e));
		}
	}

	public void setChickenStatus() {
		RoomVariable chickenstatus = new SFSRoomVariable("chicken", game.getConfig().isHaveChicken());
		// chickenstatus.setHidden(true);
		chickenstatus.setGlobal(true);
		List<RoomVariable> lst = new ArrayList<RoomVariable>();
		lst.add(chickenstatus);

		getApi().setRoomVariables(null, getGameRoom(), lst);
	}

	public final ZoneEventHandler getZonehandler() {
		return zonehandler;
	}

	public final GameEventHandler getGamehandler() {
		return gamehandler;
	}

	public final int getGameid() {
		return gameid;
	}

	public final String getPartnerid() {
		return partnerid;
	}

	@Override
	public void send(String cmdName, ISFSObject params, List<User> recipients) {
		if (recipients == null || recipients.size() == 0)
			return;

		if (config.isUseEncrypt()) {
			for (int i = 0; i < recipients.size(); i++) {
				ISFSObject encryptObj = MessageEncoder.getInstance().encrypt(recipients.get(i), params);
				super.send(cmdName, encryptObj, recipients.get(i));
			}
		} else
			super.send(cmdName, params, recipients);
	}

	@Override
	public void send(String cmdName, ISFSObject params, User recipient) {
		ISFSObject encryptObj = params;
		if (config.isUseEncrypt())
			encryptObj = MessageEncoder.getInstance().encrypt(recipient, params);

		// super.send(cmdName, encryptObj, recipient);
		super.send(cmdName, encryptObj, recipient);
	}

	public void destroyRoom() {
		cancelTimer();
		List<User> listplayer = getParentRoom().getPlayersList();
		if (listplayer != null) {
			for (int i = 0; i < listplayer.size(); i++)
				getApi().leaveRoom(listplayer.get(i), getGameRoom());
		}

		List<User> listguest = getParentRoom().getSpectatorsList();
		if (listguest != null) {
			for (int i = 0; i < listguest.size(); i++)
				getApi().leaveRoom(listguest.get(i), getGameRoom());
		}

		getApi().removeRoom(getParentRoom(), true, true);

	}

	public final Config getConfig() {
		return config;
	}

	public final boolean isFirst() {
		return isFirst;
	}

	public final long getStartWaitReady() {
		return startWaitReady;
	}

	public final void setStartWaitReady(long startWaitReady) {
		this.startWaitReady = startWaitReady;
	}

	public void setRoomVaribale() {
		RoomVariable playval = new SFSRoomVariable("playtime", config.getPlayWaitTime());
		playval.setGlobal(true);
		// RoomVariable endval = new SFSRoomVariable("endtime",
		// config.getGameWaitStart());
		RoomVariable endval = new SFSRoomVariable("endtime", config.getFirstGameWaitStart());
		endval.setGlobal(true);
//set phế
		RoomVariable pheVal = new SFSRoomVariable("fee", "0.05");
		pheVal.setGlobal(true);

		// RoomVariable firstreadywait = new SFSRoomVariable("firstwait",
		// config.getFirstGameWaitStart());
		// firstreadywait.setGlobal(true);

		List<RoomVariable> lst = new ArrayList<RoomVariable>();
		lst.add(playval);
		lst.add(endval);
		lst.add(pheVal);
		// lst.add(firstreadywait);

		// getApi().setRoomVariables(user, getGameRoom(), lst);
		getParentRoom().setVariables(lst);
	}

	public void setReadyWaitVariable(long duration) {
		RoomVariable endval = new SFSRoomVariable("endtime", duration);
		endval.setGlobal(true);

		List<RoomVariable> lst = new ArrayList<RoomVariable>();
		lst.add(endval);

		getApi().setRoomVariables(null, getGameRoom(), lst);
	}

	public void setUseChickenVar(User user, int isuse) {
		List<UserVariable> lst = new ArrayList<UserVariable>();
		lst.add(new SFSUserVariable(ServerFieldID.EXT_VAL_USE_CHICKEN, isuse));

		getApi().setUserVariables(user, lst);
	}

	@Override
	public Object handleInternalMessage(String cmdName, Object params) {
		zonehandler.handlerCommandFromZone(cmdName, params);
		return super.handleInternalMessage(cmdName, params);
	}

	@Override
	public ExtensionReloadMode getReloadMode() {
		return ExtensionReloadMode.NONE;
	}

	public final boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean isPlayed) {
		this.isPlaying = isPlayed;
	}

	public String getTimerName() {
		return String.format("%s-%s", gamename, getGameRoom().getName());
	}

	public final void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}
}
