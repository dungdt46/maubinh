package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.ServerFieldID;
import com.dts.icasino.common.ServerMsgID;
import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;
import com.dts.icasino.common.util.StringUtil;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class PlayRegisterCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new PlayRegisterCmd();
	}

	@Override
	public void processEvent() {
	}

	@Override
	public void processRequest() {
		extension.log( String.format("RegisterPlay: Player %s(%d) join", user.getName(), user.getId()));
		GameRoom room = extension.getGame();
		
		PlayerInfo playerInfo = room.getPlayerman().getInfo(user);
		if (playerInfo != null)
		{
			if (playerInfo.isPlayer())
			{
				extension.log( String.format("RegisterPlay: Player %s(%d) already player", user.getName(), user.getId()));
				extension.sendInvalid(user, ServerMsgID.EXT_EVENT_QUEUE_GAME_RES);
				return;
			}
			
			if (playerInfo.getBalance() < room.getConfig().getMinlevel())
			{
				extension.log( String.format("RegisterPlay: Player %s(%d) have balance %f not enough money register play", 
	        			user.getName(), user.getId(), playerInfo.getBalance()));
				
				SFSObject exntfObj = new SFSObject();
                exntfObj.putUtfString(ServerFieldID.EXT_FIELD_USERID, user.getName());
                extension.sendGameEvent2User(ServerMsgID.EXT_EVENT_USER_EXPIRE_MONEY_NOTIF, exntfObj, user);
				
                extension.sendInvalid(user, ServerMsgID.EXT_EVENT_QUEUE_GAME_RES);
                return;
			}
			
			if (room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING)
			{
				room.getPlayerman().queuePlayer(playerInfo);
				room.sendQueueUpdate();
			}
			else
			{
				try {
					extension.getApi().spectatorToPlayer(playerInfo.getUser(), extension.getGameRoom(), true, false);
					Spectator2PlayerCmd cmd = new Spectator2PlayerCmd();
					cmd.setUser(playerInfo.getUser());
					cmd.setExtension(extension);
					cmd.processEvent();
				} catch (Exception e) {
					extension.log(StringUtil.stackTraceToString(e));
				}
			}
		}
		else
			extension.log( String.format("RegisterPlay: Player %s(%d) not found", user.getName(), user.getId()));
	}

}
