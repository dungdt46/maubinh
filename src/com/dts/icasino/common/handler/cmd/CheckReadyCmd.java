package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.entity.PlayerInfo;

public class CheckReadyCmd extends BaseCmd{

	@Override
	public BaseCmd createCmd() {
		return new CheckReadyCmd();
	}

	@Override
	public void processEvent() {
		if (extension.getGame().getPlayerman().countPlayer() > 1)
		{
			if (extension.isFirst())
			{
				if (System.currentTimeMillis() - extension.getStartWaitReady() > extension.getConfig().getFirstGameWaitStart())
				{
					PlayerInfo boss = extension.getGame().getPlayerman().getBoss();
					extension.log( String.format("CheckReady: Boss player %s(%d) not ready, start game", 
							boss.getUser().getName(), boss.getUser().getId()));
						                
					UserReadyCmd cmd = new UserReadyCmd();
					cmd.setUser(boss.getUser());
					cmd.setExtension(extension);
					cmd.processRequest();
				}
			}
			else
				if (System.currentTimeMillis() - extension.getStartWaitReady() > extension.getConfig().getGameWaitStart())
					extension.startGame();
		}
		else
			extension.setStartWaitReady(System.currentTimeMillis());
	}

	@Override
	public void processRequest() {
		
	}

}
