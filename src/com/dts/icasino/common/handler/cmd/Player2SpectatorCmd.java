package com.dts.icasino.common.handler.cmd;

import com.dts.icasino.common.entity.GameRoom;
import com.dts.icasino.common.entity.PlayerInfo;

public class Player2SpectatorCmd extends BaseCmd {

	@Override
	public BaseCmd createCmd() {
		return new Player2SpectatorCmd();
	}

	@Override
	public void processEvent() {

		extension.log(String.format("Player2Spectator: Player %s(%d) change from player to " + "spectator",
				user.getName(), user.getId()));
		GameRoom room = extension.getGame();

		PlayerInfo playerInfo = room.getPlayerman().getInfo(user);

		if (playerInfo == null) { // not exist, create new
			extension.log(String.format("Player2Spectator: Player %s(%d) not exist, create info", user.getName(),
					user.getId()));

			playerInfo = room.newPlayer(user);

		} else {
			extension.log(String.format("Player2Spectator: Player %s(%d) existed, update info", user.getName(),
					user.getId()));

			if (playerInfo.isPlayer() == true && room.getStatus() == GameRoom.GAME_TABLE_STATUS_PLAYING) {
				extension.log("Player2Spectator: player can not 2 spec because playing");
				return;
			}

			extension.getGame().removeInfo(playerInfo);
			playerInfo.setUser(user);
			playerInfo.setPlayer(false);
			playerInfo.setBoss(false);
			playerInfo.setDisconnected(false);
		}

		room.getPlayerman().player2Guest(playerInfo);
		room.sendQueueUpdate2User(user);
		room.sendListUserUpdate();

		room.sendChickenUpdate2User(user);
	}

	@Override
	public void processRequest() {
		return;
	}

}
