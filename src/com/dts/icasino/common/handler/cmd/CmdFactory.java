package com.dts.icasino.common.handler.cmd;

import java.util.HashMap;

import com.dts.icasino.common.ServerMsgID;
import com.smartfoxserver.v2.core.SFSEventType;

public class CmdFactory {
	private HashMap<String, BaseCmd> lstCmd = new HashMap<String, BaseCmd>();

	private static CmdFactory instance;

	private CmdFactory() {
		lstCmd.put(SFSEventType.USER_DISCONNECT.name(), new DisconnectedUserCmd());
		lstCmd.put(SFSEventType.PLAYER_TO_SPECTATOR.name(), new Player2SpectatorCmd());
		lstCmd.put(SFSEventType.SPECTATOR_TO_PLAYER.name(), new Spectator2PlayerCmd());
		lstCmd.put(SFSEventType.USER_JOIN_ROOM.name(), new UserJoinRoomCmd());
		lstCmd.put(SFSEventType.USER_LEAVE_ROOM.name(), new UserLeaveRoomCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_READY_REQ, new UserReadyCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_UNREADY_REQ, new UserUnreadyCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_KICK_REQ, new KickUserCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_START_DEAL_REQ, new StartDealCmd());
		lstCmd.put(ServerMsgID.NOTIFY_RECEIVED_MONEY_FROM_FRIEND, new ChargeCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_SET_BOT_REQ, new SetBotCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_SET_CARD_REQ, new SetBotCardCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_GAME_LEAVE_REQ, new LeaveGameCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_GAME_CHIKKEN_CONFIG_REQ, new ConfigChickenCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_QUEUE_GAME_REQ, new PlayRegisterCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_LEAVE_GAME_REQ, new RegisterLeaveCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_GET_GAME_INFO_REQ, new GetGameInfoCmd());
		lstCmd.put(ServerMsgID.EXT_EVENT_USE_CHICKEN_REQ, new UseChickenCmd());
	}

	public static synchronized CmdFactory getInstance() {
		if (instance == null)
			instance = new CmdFactory();

		return instance;
	}
	
	public void addCommand(String key, BaseCmd cmd)
	{
		lstCmd.put(key, cmd);
	}

	public BaseCmd getCmd(String key) {
		BaseCmd cmd = lstCmd.get(key);
		
		if (cmd == null)
			return null;
		else
			return cmd.createCmd();
	}
}
